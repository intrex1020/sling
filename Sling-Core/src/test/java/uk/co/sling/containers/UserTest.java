package uk.co.sling.containers;

import uk.co.sling.containers.User.UserBuilder;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author sebastian wojtkowiak
 */
@RunWith(value = Parameterized.class)
public class UserTest extends AbstractProfileTest
{
    private User instance;
    private Contractor contractorInstance;

    public UserTest(AbstractProfile instance)
    {
        super(instance);
    }

    @Parameterized.Parameters
    public static Collection data()
    {
        return Arrays.asList(new User.UserBuilder().setUserName("Username").setCompanyName("CompanyName").setVatCode("1321-312-123").setTown("Town").setPostCode("postCode").setStreet("Street")
                .setBuildingNumber("12a").setPhoneNumber("123123123").setEmail("email@email.com").buildUser());
    }

    @Before
    @Override
    public void setUp()
    {
        super.setUp();
        instance = new UserBuilder().setUserName("Username").setCompanyName("CompanyName").setVatCode("1321-312-123")
                .setTown("Town").setPostCode("postCode").setStreet("Street").setBuildingNumber("12a")
                .setPhoneNumber("123123123").setEmail("email@email.com").buildUser();

        contractorInstance = new Contractor.ContractorBuilder().setCompanyName("CompanyName").setVatCode("1321-312-123")
                .setTown("Town").setPostCode("postCode").setStreet("Street").setBuildingNumber("12a")
                .setPhoneNumber("123123123").setEmail("email@email.com").buildContractor();
    }

    @After
    @Override
    public void tearDown()
    {
        super.tearDown();
        instance = null;
        contractorInstance = null;
    }

    @Test
    public void testGetUserName()
    {
        System.out.println("testGetUserName");
        String expResult = "Username";
        String result = instance.getUserName();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetUserName()
    {
        System.out.println("testSetUserName");
        String userName = "MyUserName";
        instance.setUserName(userName);
        assertEquals(userName, instance.getUserName());
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetUserNameNull()
    {
        System.out.println("testSetUserNameNull");
        String userName = null;
        instance.setUserName(userName);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetUserNameEmptyString()
    {
        System.out.println("testSetUserNameEmptyString");
        String userName = "";
        instance.setUserName(userName);
    }

    @Test
    public void testSetUserNameNotSetErrorMessage()
    {
        System.out.println("testSetUserNameNotSetErrorMessage");
        String userName = "";
        try
        {
            instance.setUserName(userName);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetUserName"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetUserNameNullErrorMessage()
    {
        System.out.println("testSetUserNameNotSetErrorMessage");
        String userName = null;
        try
        {
            instance.setUserName(userName);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetUserName"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testUserNameInvalidFormatErrorMessage()
    {
        System.out.println("testUserNameInvalidFormatErrorMessage");

        try
        {
            instance.setUserName("  ");
        }
        catch (InvalidValueException ive)
        {
            assertEquals(Localisation.getValuePL("invalidUserName"), ive.getMessage());

            return;
        }

        fail("Didn't throw an exception");
    }

    @Test(expected = InvalidValueException.class)
    public void getContractorFail()
    {
        System.out.println("getContractorFail");
        String companyName = "asdasda";
        instance.getItem(companyName);
    }

    @Test
    public void getContractorFailErrorMessage()
    {
        System.out.println("getContractorFailErrorMessage");
        String companyName = "asdasda";
        try
        {
            instance.getItem(companyName);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("notExistingContractor"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getContractorSuccess()
    {
        System.out.println("getContractorSuccess");
        String companyName = "CompanyName";
        instance.addItem(contractorInstance);
        assertEquals(companyName, instance.getItem(companyName).getCompanyName());
    }

    @Test
    public void getContractorSuccessIsCopy()
    {
        System.out.println("getContractorSuccessIsCopy");
        String companyName = "CompanyName";
        instance.addItem(contractorInstance);
        assertNotSame(instance, instance.getItem(companyName));
    }

    @Test
    public void addContractorCheckExistence()
    {
        System.out.println("addContractorCheckExistence");
        String companyName = "CompanyName";
        instance.addItem(contractorInstance);
        assertEquals(companyName, instance.getItem(companyName).getCompanyName());
    }

    @Test (expected = InvalidValueException.class)
    public void getContractorNullIndex()
    {
        System.out.println("getContractorNullIndex");

        instance.getItem(null);
    }

    @Test
    public void getContractorNullIndexErrorMessage()
    {
        System.out.println("getContractorNullIndex");

        try
        {
            instance.getItem(null);
        }
        catch (InvalidValueException ive)
        {
            assertEquals(Localisation.getValuePL("notExistingContractor"), ive.getMessage());
            return;
        }

        fail("Wrong exception message");
    }

    @Test
    public void getCopy()
    {
        System.out.println("getCopy");
        assertNotSame(instance, instance.copy());
    }

    @Test
    public void equals()
    {
       User u = new UserBuilder()
                .setUserName("Username")
                .setCompanyName("CompanyName")
                .setVatCode("1321-312-123")
                .setTown("Town")
                .setPostCode("postCode")
                .setStreet("Street")
                .setBuildingNumber("12a")
                .setPhoneNumber("123123123")
                .setEmail("email@email.com")
                .buildUser();

       assertTrue("Equals returned false", u.equals(instance));
    }

    @Test
    public void equalsTheSame()
    {
        assertTrue(instance.equals(instance));
    }

    @Test
    public void equalsNotInstaceOf()
    {
        assertFalse(instance.equals(new Object()));
    }

    @Test
    public void hashcode()
    {
        User u = new UserBuilder()
                .setUserName("Username")
                .setCompanyName("CompanyName")
                .setVatCode("1321-312-123")
                .setTown("Town")
                .setPostCode("postCode")
                .setStreet("Street")
                .setBuildingNumber("12a")
                .setPhoneNumber("123123123")
                .setEmail("email@email.com")
                .buildUser();

        assertEquals("hashcode is not equal", u.hashCode(), instance.hashCode());
    }
}
