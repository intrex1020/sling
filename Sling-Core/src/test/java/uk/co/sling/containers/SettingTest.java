package uk.co.sling.containers;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

import org.junit.*;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;

import javax.imageio.ImageIO;

import static org.junit.Assert.*;
/**
 *
 * @author sebastian wojtkowiak
 */
public class SettingTest
{
    private Setting instance;
    private static Image img;

    @BeforeClass
    public static void loadImage() throws Exception
    {
        img = ImageIO.read(new File(ClassLoader.getSystemResource("testImage.png").getFile()));
    }

    @Before
    public void setUp()
    {
        instance = new Setting.SettingBuilder()
                .setAccountNumber("123123123")
                .setBankName("BKO BP")
                .setWaterMark(img)
                .buildSetting();
    }

    @After
    public void tearDown()
    {
        instance = null;
    }

    @AfterClass
    public static void freeImage()
    {
        img = null;
    }

    @Test
    public void accountNumberNoSpaces()
    {
        System.out.println("accountNumberNoSpaces");

        String accountNumber = "123123123";
        assertEquals(accountNumber, instance.getAccountNumber());
    }

    @Test
    public void accountNumberSpaces()
    {
        System.out.println("accountNumberSpaces");

        String accountNumber = "123 123 123";
        instance.setAccountNumber(accountNumber);
        assertEquals(accountNumber, instance.getAccountNumber());
    }

    @Test(expected = InvalidValueException.class)
    public void accountNumberSpaceFirst()
    {
        System.out.println("accountNumberSpaceFirst");

        String accountNumber = " 123 123 123";
        instance.setAccountNumber(accountNumber);
    }

    @Test
    public void accountNumberLetters()
    {
        System.out.println("accountNumberLetters");

        String accountNumber = "asd";
        instance.setAccountNumber(accountNumber);

        assertEquals(accountNumber, instance.getAccountNumber());
    }

    @Test(expected = InvalidValueException.class)
    public void accountNumberSpaceLast()
    {
        System.out.println("accountNumberSpaceLast");

        String accountNumber = "123 123 123 ";
        instance.setAccountNumber(accountNumber);
    }

    @Test(expected = InvalidValueException.class)
    public void accountNumberNull()
    {
        System.out.println("accountNumberNull");

        instance.setAccountNumber(null);
    }

    @Test(expected = InvalidValueException.class)
    public void accountNumberEmpty()
    {
        System.out.println("accountNumberEmpty");

        instance.setAccountNumber("");
    }

    @Test(expected = InvalidValueException.class)
    public void accountNumberOnlySpaces()
    {
        System.out.println("accountNumberOnlySpaces");

        String accountNumber = "        ";
        instance.setBankName(accountNumber);
    }

    @Test
    public void getBankName()
    {
        System.out.println("getBankName");

        String bankName = "BKO BP";
        assertEquals(bankName, instance.getBankName());
    }

    @Test(expected = ValueNotSetException.class)
    public void bankNameNull()
    {
        System.out.println("bankNameNull");

        instance.setBankName(null);
    }

    @Test
    public void bankNameNullOrEmptyErrorMessage()
    {
        System.out.println("bankNameNullOrEmptyErrorMessage");

        try
        {
            instance.setBankName(null);
        }
        catch (ValueNotSetException vnse)
        {
            assertEquals(Localisation.getValuePL("notSetBankName"), vnse.getMessage());

            return;
        }

        fail("Didn't throw exception");
    }

    @Test(expected = ValueNotSetException.class)
    public void bankNameEmpty()
    {
        System.out.println("bankNameEmpty");

        instance.setBankName("");
    }

    @Test(expected = InvalidValueException.class)
    public void bankNameFieldNotValid()
    {
        System.out.println("bankNameFieldNotValid");

        String bankName = " aa";
        instance.setBankName(bankName);
    }

    @Test
    public void bankNameWithSpaces()
    {
        System.out.println("bankNameWithSpaces");

        String bankName = "BK PB";
        instance.setBankName(bankName);

        assertEquals(bankName, instance.getBankName());
    }

    @Test
    public void bankNameWhiteCharactersErrorMessage()
    {
        System.out.println("bankNameNullOrEmptyErrorMessage");

        try
        {
            instance.setBankName(" ");
        }
        catch (InvalidValueException ive)
        {
            assertEquals(Localisation.getValuePL("invalidBankName"), ive.getMessage());

            return;
        }

        fail("Didn't throw exception");
    }

    @Test
    public void testImageNotNull()
    {
        System.out.println("testImageNotNull");

        assertNotNull(instance.getWaterMark());
    }

    @Test
    public void testImageNull()
    {
        System.out.println("testImageNull");

        instance.setWaterMark(null);
        assertNull(instance.getWaterMark());
    }

    @Test (expected = InvalidValueException.class)
    public void testImageSizeTooBig()
    {
        System.out.println("testImageSizeTooBig");

        BufferedImage bi = new BufferedImage(4250, 4250, BufferedImage.TYPE_INT_ARGB);

        for (int i = 0; i < 4250; i++)
        {
            for (int j = 0; j < 4250; j++)
            {
                int a = (int) (Math.random() * 256); //alpha
                int r = (int) (Math.random() * 256); //red
                int g = (int) (Math.random() * 256); //green
                int b = (int) (Math.random() * 256); //blue

                int p = (a << 24) | (r << 16) | (g << 8) | b; //pixel

                bi.setRGB(i, j, p);
            }
        }

        instance.setWaterMark(bi);
    }

    @Test
    public void testImageSizeTooBigErrorMessage()
    {
        System.out.println("testImageSizeTooBig");

        BufferedImage bi = new BufferedImage(4250, 4250, BufferedImage.TYPE_INT_ARGB);

        for (int i = 0; i < 4250; i++)
        {
            for (int j = 0; j < 4250; j++)
            {
                int a = (int) (Math.random() * 256); //alpha
                int r = (int) (Math.random() * 256); //red
                int g = (int) (Math.random() * 256); //green
                int b = (int) (Math.random() * 256); //blue

                int p = (a << 24) | (r << 16) | (g << 8) | b; //pixel

                bi.setRGB(i, j, p);
            }
        }

        try
        {
                instance.setWaterMark(bi);
        }
        catch (InvalidValueException ive)
        {
            assertEquals(Localisation.getValuePL("imageTooBig"), ive.getMessage());

            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testAccountNumberAcceptsLetters()
    {
        System.out.println("testAccountNumberAcceptsLetters");

        instance.setBankName("BANK 123123 AS");
    }
}
