package uk.co.sling.containers;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.utils.PaymentType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import uk.co.sling.local_date.LocalDateTimeEUFormat;
import uk.co.sling.locale.Localisation;

/**
 * Tests Invoice container
 *
 * @author intrex
 */
public class InvoiceTest
{
    private Invoice instance;
    private InvoiceItem itemInstance;
    private Contractor contractorInstance;
    private User userInstance;
    private Setting settingInstance;

    @Before
    public void setUp()
    {
        userInstance = new User.UserBuilder()
                .setUserName("Username")
                .setCompanyName("CompanyName")
                .setVatCode("1321-312-123")
                .setTown("Town")
                .setPostCode("postCode")
                .setStreet("Street")
                .setBuildingNumber("12a")
                .setPhoneNumber("123123123")
                .setEmail("email@email.com")
                .buildUser();

        contractorInstance = new Contractor.ContractorBuilder()
                .setCompanyName("CompanyName")
                .setVatCode("1321-312-123")
                .setTown("Town")
                .setPostCode("postCode")
                .setStreet("Street")
                .setBuildingNumber("12a")
                .setPhoneNumber("123123123")
                .setEmail("email@email.com")
                .buildContractor();

        settingInstance = new Setting.SettingBuilder()
                .setAccountNumber("1235342")
                .setBankName("My bank")
                .buildSetting();

        instance = new Invoice.InvoiceBuilder()
                .setDaysToPay(2)
                .setInvoiceNumber(15)
                .setIssueDate(LocalDateTimeEUFormat.of(12, 7, 2017))
                .setIssuedBy(userInstance)
                .setIssuedTo(contractorInstance)
                .setIsPaid(false)
                .setPaymentType(PaymentType.BANK_TRANSFER)
                .setSetting(settingInstance)
                .buildInvoice();

        itemInstance = new InvoiceItem.InvoiceItemBuilder()
                .setItemIndex(1)
                .setItemDescription("ItemDesc")
                .setItemAmount(12)
                .setMeasurementUnit("kg")
                .setVatPercentage(23)
                .setPriceNetto(200)
                .buildInvoiceItem();
    }

    @After
    public void tearDown()
    {
        instance = null;
        userInstance = null;
        contractorInstance = null;
        itemInstance = null;
    }

    @Test
    public void getInvoiceUniqueCode()
    {
        System.out.println("getInvoiceUniqueCode");
        String code = "15-12-07-2017";
        assertEquals(code, instance.getInvoiceUniqueCode());
    }

    @Test(expected = ValueNotSetException.class)
    public void setInvoiceUniqueCodeNull()
    {
        System.out.println("setInvoiceUniqueCodeNull");
        instance.setInvoiceUniqueCode(null);
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeSpaceString()
    {
        System.out.println("setInvoiceUniqueCodeSpaceString");
        instance.setInvoiceUniqueCode(" ");
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeEmptyString()
    {
        System.out.println("setInvoiceUniqueCodeEmptyString");
        instance.setInvoiceUniqueCode("");
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeMissingDashes()
    {
        System.out.println("setInvoiceUniqueCodeMissingDashes");
        instance.setInvoiceUniqueCode("1512072017");
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeMissingDash1()
    {
        System.out.println("setInvoiceUniqueCodeMissingDash1");
        instance.setInvoiceUniqueCode("1512-07-2017");
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeMissingDash2()
    {
        System.out.println("setInvoiceUniqueCodeMissingDash2");
        instance.setInvoiceUniqueCode("15-1207-2017");
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeMissingDas3h()
    {
        System.out.println("setInvoiceUniqueCodeMissingDas3h");
        instance.setInvoiceUniqueCode("15-12-072017");
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeMissingInvoiceNumber()
    {
        System.out.println("setInvoiceUniqueCodeMissingInvoiceNumber");
        instance.setInvoiceUniqueCode("12-07-2017");
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeMissingDate()
    {
        System.out.println("setInvoiceUniqueCodeMissingDate");
        instance.setInvoiceUniqueCode("15");
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeLetters()
    {
        System.out.println("setInvoiceUniqueCodeLetters");
        instance.setInvoiceUniqueCode("aa-aa-aa-aaaa");
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeNotFullYear()
    {
        System.out.println("setInvoiceUniqueCodeNotFullYear");
        instance.setInvoiceUniqueCode("03-12-12-17");
    }

    @Test
    public void setInvoiceUniqueCodeSuccess()
    {
        System.out.println("setInvoiceUniqueCodeSuccess");
        String code = "03-12-12-2017";
        instance.setInvoiceUniqueCode(code);
        assertEquals(code, instance.getInvoiceUniqueCode());
    }

    @Test
    public void setInvoiceUniqueCodeSuccessGreaterThan100()
    {
        System.out.println("setInvoiceUniqueCodeSuccessGreaterThan100");
        String code = "103-12-12-2017";
        instance.setInvoiceUniqueCode(code);
        assertEquals(code, instance.getInvoiceUniqueCode());
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeSpaceFirst()
    {
        System.out.println("bankNameFieldNotValid");

        String code = " 55";
        instance.setInvoiceUniqueCode(code);
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeSpaceLast()
    {
        System.out.println("bankNameSpaceLast");

        String code = "55 ";
        instance.setInvoiceUniqueCode(code);
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeOnlySpaces()
    {
        System.out.println("bankNameOnlySpaces");

        String code = "        ";
        instance.setInvoiceUniqueCode(code);
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeCorrectLastSpace()
    {
        System.out.println("setInvoiceUniqueCodeCorrectLastSpace");

        String code = "03-12-12-2017 ";
        instance.setInvoiceUniqueCode(code);
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceUniqueCodeCorrectFrontSpace()
    {
        System.out.println("setInvoiceUniqueCodeCorrectFrontSpace");

        String code = " 03-12-12-2017";
        instance.setInvoiceUniqueCode(code);
    }

    @Test
    public void setInvoiceUniqueCodeErrorMessage()
    {
        System.out.println("");
        String code = "03-12-12-17";

        try
        {
            instance.setInvoiceUniqueCode(code);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;

        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidUniqueCode"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void setInvoiceUniqueCodeNullErrorMessage()
    {
        System.out.println("");
        String code = null;

        try
        {
            instance.setInvoiceUniqueCode(code);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetUniqueCode"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getInvoiceNumber()
    {
        int number = 15;
        assertEquals(number, instance.getInvoiceNumber());
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceNumberLessThan1()
    {
        System.out.println("setInvoiceNumberLessThan1");
        instance.setInvoiceNumber(0);
    }

    @Test(expected = InvalidValueException.class)
    public void setInvoiceNumberLessThanNegative()
    {
        System.out.println("setInvoiceNumberLessThanNegative");
        instance.setInvoiceNumber(-5);
    }

    @Test
    public void setInvoiceNumber()
    {
        System.out.println("setInvoiceNumber");
        int number = 12;
        instance.setInvoiceNumber(number);
        assertEquals(number, instance.getInvoiceNumber());
    }

    @Test
    public void getIssuedBy()
    {
        System.out.println("getIssuedBy");
        assertEquals(userInstance.getUserName(), instance.getIssuedBy().getUserName());
    }

    @Test
    public void getIssuedByIsCopy()
    {
        System.out.println("getIssuedByIsCopy");
        assertNotSame(userInstance, instance.getIssuedBy());
    }

    @Test
    public void setIssuedBy()
    {
        System.out.println("setIssuedBy");
        User u = instance.getIssuedBy();
        u.setUserName("Mahjong");
        instance.setIssuedBy(u);
        assertEquals(u.getUserName(), instance.getIssuedBy().getUserName());
    }

    @Test(expected = ValueNotSetException.class)
    public void setIssuedByNull()
    {
        System.out.println("setIssuedByNull");
        instance.setIssuedBy(null);
    }

    @Test
    public void setIssuedByNullErrorMessage()
    {
        System.out.println("setIssuedByNullErrorMessage");
        try
        {
            instance.setIssuedBy(null);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetUser"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getIssuedTo()
    {
        System.out.println("getIssuedTo");
        assertEquals(contractorInstance.getCompanyName(), instance.getIssuedTo().getCompanyName());
    }

    @Test
    public void getIssuedToIsCopy()
    {
        System.out.println("getIssuedToIsCopy");
        assertNotSame(contractorInstance, instance.getIssuedTo());
    }

    @Test
    public void setIssuedTo()
    {
        System.out.println("setIssuedTo");
        Contractor c = instance.getIssuedTo();
        c.setCompanyName("Mahjong");
        instance.setIssuedTo(c);
        assertEquals(c.getCompanyName(), instance.getIssuedTo().getCompanyName());
    }

    @Test(expected = ValueNotSetException.class)
    public void setIssuedToNull()
    {
        System.out.println("setIssuedToNull");
        instance.setIssuedTo(null);
    }

    @Test
    public void setIssuedToNullErrorMessage()
    {
        System.out.println("setIssuedToNullErrorMessage");
        try
        {
            instance.setIssuedTo(null);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetContractor"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getSettingsTo()
    {
        System.out.println("getSettingsTo");
        assertEquals(settingInstance.getAccountNumber(), instance.getSetting().getAccountNumber());
    }

    @Test
    public void getSettingsToIsCopy()
    {
        System.out.println("getSettingsToIsCopy");
        assertNotSame(settingInstance, instance.getSetting());
    }

    @Test
    public void setSettingsTo()
    {
        System.out.println("setSettingsTo");
        Setting s = instance.getSetting();
        s.setBankName("Mahjong");
        instance.setSetting(s);
        assertEquals(s.getBankName(), instance.getSetting().getBankName());
    }

    @Test(expected = ValueNotSetException.class)
    public void setSettingsToNull()
    {
        System.out.println("setSettingsToNull");
        instance.setSetting(null);
    }

    @Test
    public void setSettingsToNullErrorMessage()
    {
        System.out.println("setSettingsToNullErrorMessage");
        try
        {
            instance.setSetting(null);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetSettings"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getIssueDate()
    {
        System.out.println("getIssueDate");
        assertTrue(LocalDateTimeEUFormat.of(12, 7, 2017).isEqual(instance.getIssueDate()));
    }

    @Test(expected = ValueNotSetException.class)
    public void setIssueDateNull()
    {
        System.out.println("setIssueDateNull");
        instance.setIssueDate(null);
    }

    @Test
    public void setIssueDateNullErrorMessage()
    {
        System.out.println("setIssueDateNullErrorMessage");
        try
        {
            instance.setIssueDate(null);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetIssueDate"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void setIssueDateSuccess()
    {
        System.out.println("setIssueDateSuccess");
        LocalDateTimeEUFormat date = LocalDateTimeEUFormat.now();
        instance.setIssueDate(date);
        assertEquals(date, instance.getIssueDate());
    }

    @Test
    public void getPayUntil()
    {
        System.out.println("getPayUntil");
        assertTrue(LocalDateTimeEUFormat.of(12, 7, 2017).plusDays(2).isEqual(instance.getPayUntil()));
    }

    @Test
    public void getDaysToPay()
    {
        System.out.println("getDaysToPay");
        int days = 2;
        assertEquals(days, instance.getDaysToPay());
    }

    @Test(expected = InvalidValueException.class)
    public void setDaysToPayLessThan0()
    {
        System.out.println("setDaysToPayLessThan0");
        instance.setDaysToPay(-1);
    }

    @Test
    public void setDaysToPayLessThan0ErrorMessage()
    {
        System.out.println("setDaysToPayLessThan0ErrorMessage");
        try
        {
            instance.setDaysToPay(-1);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidDaysToPay"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void setDaysToPayTo0()
    {
        System.out.println("setDaysToPayTo0");
        instance.setDaysToPay(0);
        assertEquals(0, instance.getDaysToPay());
    }

    @Test
    public void getIsPaid()
    {
        System.out.println("getIsPaid");
        assertFalse(instance.getIsPaid());
    }

    @Test
    public void setIsPaid()
    {
        System.out.println("setIsPaid");
        instance.setIsPaid(true);
        assertTrue(instance.getIsPaid());
    }

    @Test
    public void getPaymentType()
    {
        System.out.println("getPaymentType");
        assertEquals(PaymentType.BANK_TRANSFER, instance.getPaymentType());
    }

    @Test(expected = ValueNotSetException.class)
    public void setPaymentTypeNull()
    {
        System.out.println("setPaymentTypeNull");
        instance.setPaymentType(null);
    }

    @Test
    public void setPaymentTypeNullErrorMessage()
    {
        System.out.println("setPaymentTypeNullErrorMessage");
        try
        {
            instance.setPaymentType(null);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetPaymentType"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test(expected = InvalidValueException.class)
    public void getInvoiceItemFail()
    {
        System.out.println("getInvoiceItemFail");
        String itemIndex = "5";
        instance.getItem(itemIndex);
    }

    @Test
    public void getInvoiceItemFailErrorMessage()
    {
        System.out.println("getInvoiceItemFailErrorMessage");
        String itemIndex = "5";
        try
        {
            instance.getItem(itemIndex);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("notExistingInvoiceItem"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test(expected = InvalidValueException.class)
    public void getInvoiceItemNonIntegerIndex()
    {
        System.out.println("getInvoiceItemNonIntegerIndex");
        String itemIndex = "asd";
        instance.getItem(itemIndex);
    }

    @Test
    public void getInvoiceItemNonIntegerIndexErrorMessage()
    {
        System.out.println("getInvoiceItemNonIntegerIndexErrorMessage");
        String itemIndex = "asd";
        try
        {
            instance.getItem(itemIndex);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("notExistingInvoiceItem"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test (expected = InvalidValueException.class)
    public void getInvoiceItemNullIndex()
    {
        System.out.println("getInvoiceItemNullIndex");

        instance.getItem(null);
    }

    @Test
    public void getInvoiceItemNullIndexErrorMessage()
    {
        System.out.println("getInvoiceItemNonIntegerIndexErrorMessage");;
        try
        {
            instance.getItem(null);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("notExistingInvoiceItem"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getInvoiceItemSuccess()
    {
        System.out.println("getInvoiceItemSuccess");
        String itemIndex = "1";
        instance.addItem(itemInstance);
        assertEquals(Integer.parseInt(itemIndex), instance.getItem(itemIndex).getItemIndex());
    }

    @Test
    public void getInvoiceItemSuccessIsCopy()
    {
        System.out.println("getInvoiceItemSuccessIsCopy");
        String itemIndex = "1";
        instance.addItem(itemInstance);
        assertNotSame(itemInstance, instance.getItem(itemIndex));
    }

    @Test
    public void addInvoiceItemCheckExistence()
    {
        System.out.println("addInvoiceItemCheckExistence");
        String itemIndex = "1";
        instance.addItem(itemInstance);
        assertEquals(Integer.parseInt(itemIndex), instance.getItem(itemIndex).getItemIndex());
    }

    @Test
    public void setUniqueCodeWithInvoiceNumberOf1Number()
    {
        System.out.println("setUniqueCodeWithInvoiceNumberOf1Number");

        instance.setInvoiceUniqueCode("1-15-12-2017");
    }

    @Test (expected = ValueNotSetException.class)
    public void setPayUntilNull()
    {
        System.out.println("setPayUntilNull");

        instance.setPayUntil(null);
    }

    @Test
    public void setPayUntilNullErrorMessage()
    {
        System.out.println("setPayUntilNullErrorMessage");

        try
        {
            instance.setPayUntil(null);
        }
        catch (ValueNotSetException vnse)
        {
            assertEquals(Localisation.getValuePL("notSetPayDate"), vnse.getMessage());

            return;
        }

        fail("No exception was thrown that day");
    }

    @Test
    public void setDaysToPayModifiesPayUntil() throws Exception
    {
        System.out.println("setDaysToPayModifiesPayUntil");

        instance.setDaysToPay(20);
        LocalDateTimeEUFormat expectedDate = instance.getIssueDate().plusDays(instance.getDaysToPay());

        assertEquals(expectedDate, instance.getPayUntil());
    }

    @Test
    public void setPayUntilModifiesIssueDate() throws Exception
    {
        System.out.println("setPayUntilModifiesIssueDate");

        LocalDateTimeEUFormat now = LocalDateTimeEUFormat.now();
        instance.setPayUntil(now);
        LocalDateTimeEUFormat expectedDate = now.minusDays(instance.getDaysToPay());

        assertEquals(expectedDate, instance.getIssueDate());
    }

    @Test
    public void setIssueDateModifiesPayUntilDate() throws Exception
    {
        System.out.println("setIssueDateModifiesPayUntilDate");

        LocalDateTimeEUFormat now = LocalDateTimeEUFormat.now();
        instance.setIssueDate(now);
        LocalDateTimeEUFormat expectedDate = instance.getIssueDate().plusDays(instance.getDaysToPay());

        assertEquals(expectedDate, instance.getPayUntil());
    }

    @Test
    public void setIssueDateModifiesUniqueCode() throws Exception
    {
        System.out.println("setIssueDateModifiesUniqueCode");

        LocalDateTimeEUFormat now = LocalDateTimeEUFormat.now();
        instance.setIssueDate(now);

        String expectedCode = instance.getInvoiceNumber() + "-" + now;

        assertEquals(expectedCode, instance.getInvoiceUniqueCode());
    }

    @Test
    public void setInvoiceNumberModifiesUniqueCode() throws Exception
    {
        System.out.println("setInvoiceNumberModifiesUniqueCode");

        instance.setInvoiceNumber(33);

        String expectedCode = 33 + "-" + instance.getIssueDate();

        assertEquals(expectedCode, instance.getInvoiceUniqueCode());
    }

    @Test
    public void setInvoiceUniqueCodeModifiesInvoiceNumber() throws Exception
    {
        System.out.println("setInvoiceUniqueCodeModifiesInvoiceNumber");

        instance.setInvoiceUniqueCode("67-12-12-2017");

        assertEquals(67, instance.getInvoiceNumber());
    }

    @Test
    public void setInvoiceUniqueCodeModifiesIssueDate() throws Exception
    {
        System.out.println("setInvoiceUniqueCodeModifiesInvoiceNumber");

        instance.setInvoiceUniqueCode("67-01-01-2017");
        LocalDateTimeEUFormat expected = LocalDateTimeEUFormat.of(1, 1, 2017);

        System.out.println(instance.getIssueDate());

        assertEquals(expected, instance.getIssueDate());
    }

    @Test
    public void getCopy()
    {
        System.out.println("getCopy");
        assertNotSame(instance, instance.copy());
    }

    @Test
    public void equals()
    {
        Invoice i = new Invoice.InvoiceBuilder()
                .setDaysToPay(2)
                .setInvoiceNumber(15)
                .setIssueDate(LocalDateTimeEUFormat.of(12, 7, 2017))
                .setIssuedBy(userInstance)
                .setIssuedTo(contractorInstance)
                .setIsPaid(false)
                .setSetting(settingInstance)
                .setPaymentType(PaymentType.BANK_TRANSFER)
                .buildInvoice();

        assertTrue("Equals returned false", i.equals(instance));
    }

    @Test
    public void equalsTheSame()
    {
        assertTrue(instance.equals(instance));
    }

    @Test
    public void equalsNotInstanceOf()
    {
        assertFalse(instance.equals(new Object()));
    }

    @Test
    public void hashcode()
    {
        Invoice i = new Invoice.InvoiceBuilder()
                .setDaysToPay(2)
                .setInvoiceNumber(15)
                .setIssueDate(LocalDateTimeEUFormat.of(12, 7, 2017))
                .setIssuedBy(userInstance)
                .setIssuedTo(contractorInstance)
                .setIsPaid(false)
                .setSetting(settingInstance)
                .setPaymentType(PaymentType.BANK_TRANSFER)
                .buildInvoice();

        assertEquals("hashcode is not euqal", i.hashCode(), instance.hashCode());
    }
}
