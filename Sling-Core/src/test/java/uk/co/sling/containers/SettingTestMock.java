package uk.co.sling.containers;

import org.junit.*;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.locale.Localisation;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 *
 * @author sebastian wojtkowiak
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ImageIO.class)
public class SettingTestMock
{
	private Setting instance;
	private static Image img;

	@BeforeClass
	public static void loadImage() throws Exception
	{
		img = ImageIO.read(new File(ClassLoader.getSystemResource("testImage.png").getFile()));
	}

	@Before
	public void setUp()
	{
		instance = new Setting.SettingBuilder()
				.setAccountNumber("123123123")
				.setBankName("BKO BP")
				.setWaterMark(img)
				.buildSetting();
	}

	@After
	public void tearDown()
	{
		instance = null;
	}

	@AfterClass
	public static void freeImage()
	{
		img = null;
	}

	@Test(expected = InvalidValueException.class)
	public void testImageSizeReadError() throws Exception
	{
		PowerMockito.mockStatic(ImageIO.class);
		when(ImageIO.write(any(RenderedImage.class), anyString(), any(OutputStream.class)))
				.thenThrow(new IOException());

		instance.setWaterMark(img);
	}

	@Test
	public void testImageSizeReadErrorErrorMessage() throws Exception
	{
		PowerMockito.mockStatic(ImageIO.class);
		when(ImageIO.write(any(RenderedImage.class), anyString(), any(OutputStream.class)))
				.thenThrow(new IOException());

		try
		{
			instance.setWaterMark(img);
		}
		catch(InvalidValueException ive)
		{
			assertEquals(Localisation.getValuePL("imageReadError"), ive.getMessage());
			return;
		}

		fail("Didn't throw an exception");
	}
}
