package uk.co.sling.containers;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.locale.Localisation;
import uk.co.sling.utils.PaymentType;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import uk.co.sling.local_date.LocalDateTimeEUFormat;

import static org.junit.Assert.*;

/**
 *
 * @author sebastian wojtkowiak
 */
@RunWith(value = Parameterized.class)
public class ContractorTest extends AbstractProfileTest
{
    private Contractor instance;
    private Invoice invoiceInstance;
    private User userInstance;
    private Setting settingInstance;

    public ContractorTest(AbstractProfile instance)
    {
        super(instance);
    }

    @Parameterized.Parameters
    public static Collection data()
    {
        return Arrays.asList(
            new Contractor.ContractorBuilder()
            .setCompanyName("CompanyName")
            .setVatCode("1321-312-123")
            .setTown("Town")
            .setPostCode("postCode")
            .setStreet("Street")
            .setBuildingNumber("12a")
            .setPhoneNumber("123123123")
            .setEmail("email@email.com")
            .buildContractor()
        );
    }

    @Before
    @Override
    public void setUp()
    {
        super.setUp();
        instance = new Contractor.ContractorBuilder()
                .setCompanyName("CompanyName")
                .setVatCode("1321-312-123")
                .setTown("Town")
                .setPostCode("postCode")
                .setStreet("Street")
                .setBuildingNumber("12a")
                .setPhoneNumber("123123123")
                .setEmail("email@email.com")
                .buildContractor();

        userInstance = new User.UserBuilder()
                .setUserName("Username")
                .setCompanyName("CompanyName")
                .setVatCode("1321-312-123")
                .setTown("Town")
                .setPostCode("postCode")
                .setStreet("Street")
                .setBuildingNumber("12a")
                .setPhoneNumber("123123123")
                .setEmail("email@email.com")
                .buildUser();

        settingInstance = new Setting.SettingBuilder()
                .setAccountNumber("1235342")
                .setBankName("My bank")
                .buildSetting();

        invoiceInstance = new Invoice.InvoiceBuilder()
                .setDaysToPay(2)
                .setInvoiceNumber(15)
                .setIssueDate(LocalDateTimeEUFormat.of(12, 7, 2017))
                .setIssuedBy(userInstance)
                .setIssuedTo(instance)
                .setPaymentType(PaymentType.BANK_TRANSFER)
                .setSetting(settingInstance)
                .buildInvoice();
    }

    @After
    @Override
    public void tearDown()
    {
        super.tearDown();
        instance = null;
    }

    @Test(expected = InvalidValueException.class)
    public void getContractorFail()
    {
        System.out.println("getContractorFail");
        String getInvoiceUniqueCode = "15-01-01-2017";
        instance.getItem(getInvoiceUniqueCode);
    }

    @Test
    public void getContractorFailErrorMessage()
    {
        System.out.println("getContractorFailErrorMessage");
        String getInvoiceUniqueCode = "15-01-01-2017";
        try
        {
            instance.getItem(getInvoiceUniqueCode);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("notExistingContractor"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getContractorSuccess()
    {
        System.out.println("getContractorSuccess");
        String getInvoiceUniqueCode = "15-12-07-2017";
        instance.addItem(invoiceInstance);
        assertEquals(getInvoiceUniqueCode, instance.getItem(getInvoiceUniqueCode).getInvoiceUniqueCode());
    }

    @Test
    public void getContractorSuccessIsCopy()
    {
        System.out.println("getContractorSuccessIsCopy");
        String getInvoiceUniqueCode = "15-12-07-2017";
        instance.addItem(invoiceInstance);
        assertNotSame(instance, instance.getItem(getInvoiceUniqueCode));
    }

    @Test
    public void addContractorCheckExistence()
    {
        System.out.println("addContractorCheckExistence");
        String getInvoiceUniqueCode = "15-12-07-2017";
        instance.addItem(invoiceInstance);
        assertEquals(getInvoiceUniqueCode, instance.getItem(getInvoiceUniqueCode).getInvoiceUniqueCode());
    }

    @Test (expected = InvalidValueException.class)
    public void getInvoiceNullIndex()
    {
        System.out.println("getInvoiceNullIndex");

        instance.getItem(null);
    }

    @Test
    public void getInvoiceNullIndexErrorMessage()
    {
        System.out.println("getInvoiceNullIndexErrorMessage");;
        try
        {
            instance.getItem(null);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("notExistingContractor"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getCopy()
    {
        System.out.println("getCopy");
        assertNotSame(instance, instance.copy());
    }

    @Test
    public void equals()
    {
        Contractor c = new Contractor.ContractorBuilder()
                .setCompanyName("CompanyName")
                .setVatCode("1321-312-123")
                .setTown("Town")
                .setPostCode("postCode")
                .setStreet("Street")
                .setBuildingNumber("12a")
                .setPhoneNumber("123123123")
                .setEmail("email@email.com")
                .buildContractor();

        assertTrue("Equals returned false", c.equals(instance));
    }

    @Test
    public void equalsSameObject()
    {
        assertTrue(instance.equals(instance));
    }

    @Test
    public void equalsNotInstaceOfContractor()
    {
        assertFalse(instance.equals(new Object()));
    }

    @Test
    public void hashcode()
    {
        Contractor c = new Contractor.ContractorBuilder()
                .setCompanyName("CompanyName")
                .setVatCode("1321-312-123")
                .setTown("Town")
                .setPostCode("postCode")
                .setStreet("Street")
                .setBuildingNumber("12a")
                .setPhoneNumber("123123123")
                .setEmail("email@email.com")
                .buildContractor();

        assertEquals("hashcode is not equal", c.hashCode(), instance.hashCode());
    }
}
