package uk.co.sling.containers;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * Common test of User and Contractor
 *
 * @author sebastian wojtkowiak
 */
class AbstractProfileTest
{
    private AbstractProfile instance;
    private AbstractProfile copy;

    AbstractProfileTest(AbstractProfile instance)
    {
        this.instance = instance;
        copy = instance.copy();
    }

    @Before
    public void setUp()
    {
        instance = copy.copy();
    }

    @After
    public void tearDown()
    {
        instance = null;
    }

    @Test
    public void testGetCompanyName()
    {
        System.out.println("testGetCompanyName");
        String expResult = "CompanyName";
        String result = instance.getCompanyName();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetCompanyName()
    {
        System.out.println("testSetCompanyName");
        String companyName = "MyCompanyName";
        instance.setCompanyName(companyName);
        assertEquals(companyName, instance.getCompanyName());
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetCompanyNameNull()
    {
        System.out.println("testSetCompanyNameNull");
        String companyName = null;
        instance.setCompanyName(companyName);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetCompanyNameEmptyString()
    {
        System.out.println("testSetCompanyNameEmptyString");
        String companyName = "";
        instance.setCompanyName(companyName);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetCompanyNameFieldNotValid()
    {
        System.out.println("testSetCompanyNameFieldNotValid");

        String companyName = " aa";
        instance.setCompanyName(companyName);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetCompanyNameSpaceLast()
    {
        System.out.println("testSetCompanyNameSpaceLast");

        String companyName = "asd ";
        instance.setCompanyName(companyName);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetCompanyNameOnlySpaces()
    {
        System.out.println("testSetCompanyNameOnlySpaces");

        String companyName = "        ";
        instance.setCompanyName(companyName);
    }

    @Test
    public void testSetCompanyInvalidNameErrorMessage()
    {
        System.out.println("testSetCompanyInvalidNameErrorMessage");
        String companyName = "  ";
        try
        {
            instance.setCompanyName(companyName);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidCompanyName"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetCompanyNameNotSetErrorMessage()
    {
        System.out.println("testSetCompanyNameNotSetErrorMessage");
        String companyName = "";
        try
        {
            instance.setCompanyName(companyName);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetCompanyName"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetCompanyNameNullErrorMessage()
    {
        System.out.println("testSetCompanyNameNullErrorMessage");
        String companyName = null;
        try
        {
            instance.setCompanyName(companyName);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetCompanyName"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testGetVatCode()
    {
        System.out.println("testGetVatCode");
        String expResult = "1321-312-123";
        String result = instance.getVatCode();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetVatCode()
    {
        System.out.println("testSetVatCode");
        String vatCode = "532-52-52";
        instance.setVatCode(vatCode);
        assertEquals(vatCode, instance.getVatCode());
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetVatCodeNull()
    {
        System.out.println("testSetVatCodeNull");
        String vatCode = null;
        instance.setVatCode(vatCode);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetVatCodeEmptyString()
    {
        System.out.println("testSetVatCodeEmptyString");
        String vatCode = "";
        instance.setVatCode(vatCode);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetVatCodeSpaceFirst()
    {
        System.out.println("testSetVatCodeSpaceFirst");

        String vatCode = " aa";
        instance.setVatCode(vatCode);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetVatCodeSpaceLast()
    {
        System.out.println("testSetVatCodeSpaceLast");

        String vatCode = "asd ";
        instance.setVatCode(vatCode);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetVatCodeOnlySpaces()
    {
        System.out.println("testSetVatCodeOnlySpaces");

        String vatCode = "        ";
        instance.setVatCode(vatCode);
    }

    @Test
    public void testSetVatInvalidCodeErrorMessage()
    {
        System.out.println("testSetVatInvalidCodeErrorMessage");
        String vatCode = "  ";
        try
        {
            instance.setVatCode(vatCode);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidVatCode"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetVatCodeNotSetErrorMessage()
    {
        System.out.println("testSetVatCodeNotSetErrorMessage");
        String vatCode = "";
        try
        {
            instance.setVatCode(vatCode);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetVatCode"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetVatCodeNullErrorMessage()
    {
        System.out.println("testSetVatCodeNullErrorMessage");
        String vatCode = null;
        try
        {
            instance.setVatCode(vatCode);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetVatCode"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testGetTown()
    {
        System.out.println("testGetTown");
        String expResult = "Town";
        String result = instance.getTown();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetTown()
    {
        System.out.println("testSetTown");
        String town = "MyTown";
        instance.setTown(town);
        assertEquals(town, instance.getTown());
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetTownNull()
    {
        System.out.println("testSetTownNull");
        String town = null;
        instance.setTown(town);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetTownEmptyString()
    {
        System.out.println("testSetTownEmptyString");
        String town = "";
        instance.setTown(town);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetTownSpaceFirst()
    {
        System.out.println("testSetTownSpaceFirst");

        String town = " aa";
        instance.setTown(town);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetTownSpaceLast()
    {
        System.out.println("testSetTownSpaceLast");

        String town = "asd ";
        instance.setTown(town);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetTownOnlySpaces()
    {
        System.out.println("testSetTownOnlySpaces");

        String town = "        ";
        instance.setTown(town);
    }

    @Test
    public void testSetInvalidTownErrorMessage()
    {
        System.out.println("testSetInvalidTownErrorMessage");
        String town = "  ";
        try
        {
            instance.setTown(town);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidTown"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetTownNotSetErrorMessage()
    {
        System.out.println("testSetTownNotSetErrorMessage");
        String town = "";
        try
        {
            instance.setTown(town);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetTown"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetTownNullErrorMessage()
    {
        System.out.println("testSetTownNotSetErrorMessage");
        String town = null;
        try
        {
            instance.setTown(town);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetTown"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testGetPostCode()
    {
        System.out.println("testGetPostCode");
        String expResult = "postCode";
        String result = instance.getPostCode();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetPostCode()
    {
        System.out.println("testSetPostCode");
        String postCode = "MyPostCode";
        instance.setPostCode(postCode);
        assertEquals(postCode, instance.getPostCode());
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetPostCodeNull()
    {
        System.out.println("testSetPostCodeNull");
        String postCode = null;
        instance.setPostCode(postCode);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetPostCodeEmptyString()
    {
        System.out.println("testSetPostCodeEmptyString");
        String postCode = "";
        instance.setPostCode(postCode);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPostCodeSpaceFirst()
    {
        System.out.println("testSetPostCodeSpaceFirst");

        String postCode = " aa";
        instance.setPostCode(postCode);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPostCodeSpaceLast()
    {
        System.out.println("testSetPostCodeSpaceLast");

        String postCode = "asd ";
        instance.setPostCode(postCode);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPostCodeOnlySpaces()
    {
        System.out.println("testSetPostCodeOnlySpaces");

        String postCode = "        ";
        instance.setPostCode(postCode);
    }

    @Test
    public void testSetInvalidPostCodeErrorMessage()
    {
        System.out.println("testSetInvalidPostCodeErrorMessage");
        String postCode = "  ";
        try
        {
            instance.setPostCode(postCode);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidPostCode"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetPostCodeNotSetErrorMessage()
    {
        System.out.println("testSetPostCodeNotSetErrorMessage");
        String postCode = "";
        try
        {
            instance.setPostCode(postCode);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetPostCode"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetPostCodeNullErrorMessage()
    {
        System.out.println("testSetPostCodeNullErrorMessage");
        String postCode = null;
        try
        {
            instance.setPostCode(postCode);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetPostCode"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testGetStreet()
    {
        System.out.println("testGetStreet");
        String expResult = "Street";
        String result = instance.getStreet();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetStreet()
    {
        System.out.println("testSetStreet");
        String street = "MyStreet";
        instance.setStreet(street);
        assertEquals(street, instance.getStreet());
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetStreetNull()
    {
        System.out.println("testSetStreetNull");
        String street = null;
        instance.setStreet(street);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetStreetEmptyString()
    {
        System.out.println("testSetStreetEmptyString");
        String street = "";
        instance.setStreet(street);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetStreetSpaceFirst()
    {
        System.out.println("testSetStreetSpaceFirst");

        String street = " aa";
        instance.setStreet(street);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetStreetSpaceLast()
    {
        System.out.println("testSetStreetSpaceLast");

        String street = "asd ";
        instance.setStreet(street);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetStreetOnlySpaces()
    {
        System.out.println("testSetStreetOnlySpaces");

        String street = "        ";
        instance.setStreet(street);
    }

    @Test
    public void testSetInvalidStreetErrorMessage()
    {
        System.out.println("testSetInvalidStreetErrorMessage");
        String street = "  ";
        try
        {
            instance.setStreet(street);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidStreet"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetStreetNotSetErrorMessage()
    {
        System.out.println("testSetStreetNotSetErrorMessage");
        String street = "";
        try
        {
            instance.setStreet(street);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetStreet"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetStreetNullErrorMessage()
    {
        System.out.println("testSetStreetNullErrorMessage");
        String street = null;
        try
        {
            instance.setStreet(street);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetStreet"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testGetBuildingNumber()
    {
        System.out.println("testGetBuildingNumber");
        String expResult = "12a";
        String result = instance.getBuildingNumber();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetBuildingNumber()
    {
        System.out.println("testSetBuildingNumber");
        String buildingNumber = "12c";
        instance.setBuildingNumber(buildingNumber);
        assertEquals(buildingNumber, instance.getBuildingNumber());
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetBuildingNumberNull()
    {
        System.out.println("testSetBuildingNumberNull");
        String buildingNumber = null;
        instance.setBuildingNumber(buildingNumber);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetBuildingNumberEmptyString()
    {
        System.out.println("testSetBuildingNumberEmptyString");
        String buildingNumber = "";
        instance.setBuildingNumber(buildingNumber);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetBuildingSpaceFirst()
    {
        System.out.println("testSetBuildingSpaceFirst");

        String buildingNumber = " aa";
        instance.setBuildingNumber(buildingNumber);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetBuildingSpaceLast()
    {
        System.out.println("testSetBuildingSpaceLast");

        String buildingNumber = "asd ";
        instance.setBuildingNumber(buildingNumber);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetBuildingOnlySpaces()
    {
        System.out.println("testSetBuildingOnlySpaces");

        String buildingNumber = "        ";
        instance.setBuildingNumber(buildingNumber);
    }

    @Test
    public void testSetInvalidBuildingErrorMessage()
    {
        System.out.println("testSetInvalidBuildingErrorMessage");
        String buildingNumber = "  ";
        try
        {
            instance.setBuildingNumber(buildingNumber);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidBuilding"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetBuildingNotSetErrorMessage()
    {
        System.out.println("testSetBuildingNotSetErrorMessage");
        String buildingNumber = "";
        try
        {
            instance.setBuildingNumber(buildingNumber);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetBuilding"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetBuildingNullErrorMessage()
    {
        System.out.println("testSetBuildingNullErrorMessage");
        String buildingNumber = null;
        try
        {
            instance.setBuildingNumber(buildingNumber);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetBuilding"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testGetPhone()
    {
        System.out.println("testGetPhone");
        String expResult = "123123123";
        String result = instance.getPhone();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetPhone()
    {
        System.out.println("testSetPhone");
        String phone = "543253";
        instance.setPhone(phone);
        assertEquals(phone, instance.getPhone());
    }

    @Test
    public void testSetPhoneWithCountry()
    {
        System.out.println("testSetPhoneWithCountry");
        String phone = "+48 543253";
        instance.setPhone(phone);
        assertEquals(phone, instance.getPhone());
    }

    @Test
    public void testSetPhoneWithCountry2AndSpace()
    {
        System.out.println("testSetPhoneWithCountry2AndSpace");
        String phone = "+48 543253";
        instance.setPhone(phone);
        assertEquals(phone, instance.getPhone());
    }

    @Test
    public void testSetPhoneWithCountry1AndSpace()
    {
        System.out.println("testSetPhoneWithCountry1AndSpace");
        String phone = "+4 543253";
        instance.setPhone(phone);
        assertEquals(phone, instance.getPhone());
    }

    @Test
    public void testSetPhoneWithCountry3AndSpace()
    {
        System.out.println("testSetPhoneWithCountry3AndSpace");
        String phone = "+443 543253";
        instance.setPhone(phone);
        assertEquals(phone, instance.getPhone());
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhoneWithoutPlus()
    {
        System.out.println("testSetPhoneWithoutPlus");
        String phone = "443 543253";
        instance.setPhone(phone);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhoneWithCountry4AndSpace()
    {
        System.out.println("testSetPhoneWithCountry4AndSpace");
        String phone = "+4435 543253";
        instance.setPhone(phone);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhonePlusAndSpace()
    {
        System.out.println("testSetPhonePlusAndSpace");
        String phone = "+ 543253";
        instance.setPhone(phone);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhonePlusAndNoSpace()
    {
        System.out.println("testSetPhonePlusAndNoSpace");
        String phone = "+543253";
        instance.setPhone(phone);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetPhoneNull()
    {
        System.out.println("testSetPhoneNull");
        String phone = null;
        instance.setPhone(phone);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetPhoneEmptyString()
    {
        System.out.println("testSetPhoneEmptyString");
        String phone = "";
        instance.setPhone(phone);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhoneLetters()
    {
        System.out.println("testSetPhoneLetters");
        String phone = "5432a53";
        instance.setPhone(phone);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhoneDash()
    {
        System.out.println("testSetPhoneDash");
        String phone = "543-25-3";
        instance.setPhone(phone);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhoneSpaces()
    {
        System.out.println("testSetPhoneSpaces");
        String phone = "543 25 3";
        instance.setPhone(phone);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhoneSpaceFirst()
    {
        System.out.println("testSetPhoneSpaceFirst");

        String phone = " 55";
        instance.setPhone(phone);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhoneSpaceLast()
    {
        System.out.println("testSetPhoneSpaceLast");

        String phone = "555 ";
        instance.setPhone(phone);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetPhoneOnlySpaces()
    {
        System.out.println("testSetPhoneOnlySpaces");

        String phone = "        ";
        instance.setPhone(phone);
    }

    @Test
    public void testSetInvalidPhoneErrorMessage()
    {
        System.out.println("testSetInvalidPhoneErrorMessage");
        String phone = "  ";
        try
        {
            instance.setPhone(phone);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("incorrectPhone"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetPhoneNotSetErrorMessage()
    {
        System.out.println("testSetPhoneNotSetErrorMessage");
        String phone = "";
        try
        {
            instance.setPhone(phone);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetPhone"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong error message");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetPhoneNullErrorMessage()
    {
        System.out.println("testSetPhoneNullErrorMessage");
        String phone = null;
        try
        {
            instance.setPhone(phone);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetPhone"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong error message");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetPhoneIncorrectErrorMessage()
    {
        System.out.println("testSetPhoneIncorrectErrorMessage");
        String phone = "asd";
        try
        {
            instance.setPhone(phone);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong error message");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("incorrectPhone"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testGetEmail()
    {
        System.out.println("testGetEmail");
        String expResult = "email@email.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetEmail()
    {
        System.out.println("testSetEmail");
        String email = "myemail@email.pl";
        instance.setEmail(email);
        assertEquals(email, instance.getEmail());
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetEmailNull()
    {
        System.out.println("testSetEmailNull");
        String email = null;
        instance.setEmail(email);
    }

    @Test(expected = ValueNotSetException.class)
    public void testSetEmailEmptyString()
    {
        System.out.println("testSetEmailEmptyString");
        String email = "";
        instance.setEmail(email);
    }

    @Test
    public void testSetEmailAcademic()
    {
        System.out.println("testSetEmailAcademic");
        String email = "myemail@email.ac.uk";
        instance.setEmail(email);
        assertEquals(email, instance.getEmail());
    }

    @Test(expected = InvalidValueException.class)
    public void testSetEmailNoAt()
    {
        System.out.println("testSetEmailNoAt");
        String email = "myemailemail.pl";
        instance.setEmail(email);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetEmailSpaceFirst()
    {
        System.out.println("testSetEmailSpaceFirst");

        String email = " aa";
        instance.setEmail(email);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetEmailSpaceLast()
    {
        System.out.println("testSetEmailSpaceLast");

        String email = "aaa ";
        instance.setEmail(email);
    }

    @Test(expected = InvalidValueException.class)
    public void testSetEmailOnlySpaces()
    {
        System.out.println("testSetEmailOnlySpaces");

        String email = "        ";
        instance.setEmail(email);
    }

    @Test
    public void testSetInvalidEmailErrorMessage()
    {
        System.out.println("testSetInvalidEmailErrorMessage");
        String email = "  ";
        try
        {
            instance.setEmail(email);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("incorrectEmail"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetEmailInvalidErrorMessage()
    {
        System.out.println("testSetEmailInvalidErrorMessage");
        String email = "myemailemail.pl";
        try
        {
            instance.setEmail(email);
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong error message");
            return;
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("incorrectEmail"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetEmailEmptyStringErrorMessage()
    {
        System.out.println("testSetEmailEmptyStringErrorMessage");
        String email = "";
        try
        {
            instance.setEmail(email);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetEmail"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong error message");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void testSetEmailNullErrorMessage()
    {
        System.out.println("testSetEmailNoAt");
        String email = null;
        try
        {
            instance.setEmail(email);
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetEmail"), ex.getMessage());
            return;
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong error message");
            return;
        }

        fail("Didn't throw an exception");
    }
}
