package uk.co.sling.containers;

import org.junit.After;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;

/**
 *
 * @author Sebastian Wojtkowiak
 */
public class InvoiceItemTest
{
    private InvoiceItem instance;

    @Before
    public void setUp()
    {
        instance = new InvoiceItem.InvoiceItemBuilder()
                .setItemIndex(1)
                .setItemDescription("ItemDesc")
                .setItemAmount(12)
                .setMeasurementUnit("kg")
                .setVatPercentage(23)
                .setPriceNetto(200)
                .buildInvoiceItem();
    }

    @After
    public void tearDown()
    {
        instance = null;
    }

    @Test
    public void getItemIndex()
    {
        System.out.println("getItemIndex");
        int index = 1;
        assertEquals(index, instance.getItemIndex());
    }

    @Test(expected = InvalidValueException.class)
    public void setItemIndexLessThan1()
    {
        System.out.println("setItemIndexLessThan1");

        instance.setItemIndex(0);
    }

    @Test
    public void setItemIndexLessThan1ErrorMessage()
    {
        System.out.println("setItemIndexLessThan1ErrorMessage");
        try
        {
            instance.setItemIndex(0);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidItemIndex"), ex.getMessage());
            return;
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getItemDescription()
    {
        System.out.println("getItemDescription");
        String desc = "ItemDesc";
        assertEquals(desc, instance.getItemDescription());
    }

    @Test(expected = InvalidValueException.class)
    public void setItemDescriptionSpaceFirst()
    {
        System.out.println("setItemDescriptionSpaceFirst");

        String desc = " aa";

        instance.setItemDescription(desc);
    }

    @Test(expected = InvalidValueException.class)
    public void setItemDescriptionSpaceLast()
    {
        System.out.println("setItemDescriptionSpaceLast");

        String desc = "asd ";

        instance.setItemDescription(desc);
    }

    @Test(expected = InvalidValueException.class)
    public void setItemDescriptionOnlySpaces()
    {
        System.out.println("setItemDescriptionOnlySpaces");

        String desc = "        ";

        instance.setItemDescription(desc);
    }

    @Test
    public void setItemInvalidDescriptionErrorMessage()
    {
        System.out.println("setItemInvalidDescriptionErrorMessage");

        String desc = "        ";

        try
        {
            instance.setItemDescription(desc);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidItemDescription"), ex.getMessage());
            return;
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test(expected = ValueNotSetException.class)
    public void setItemDescriptionNull()
    {
        System.out.println("setItemDescriptionNull");

        instance.setItemDescription(null);
    }

    @Test
    public void setItemDescriptionNullErrorMessage()
    {
        System.out.println("setItemDescriptionNullErrorMessage");
        try
        {
            instance.setItemDescription(null);
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetItemDescription"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test(expected = ValueNotSetException.class)
    public void setItemDescriptionEmptyString()
    {
        System.out.println("setItemDescriptionEmptyString");

        instance.setItemDescription("");
    }

    @Test
    public void setItemDescriptionEmptyStringErrorMessage()
    {
        System.out.println("setItemDescriptionEmptyStringErrorMessage");
        try
        {
            instance.setItemDescription("");
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetItemDescription"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getItemAmount()
    {
        System.out.println("getItemAmount");
        double amount = 12;
        assertEquals(amount, instance.getItemAmount(), 0);
    }

    @Test(expected = InvalidValueException.class)
    public void setItemAmount0()
    {
        System.out.println("setItemAmount0");

        instance.setItemAmount(0);
    }

    @Test(expected = InvalidValueException.class)
    public void setItemAmountNegative()
    {
        System.out.println("setItemAmountNegative");

        instance.setItemAmount(-0.1);
    }

    @Test
    public void voidSetItemAmountFailErrorMessage()
    {
        System.out.println("voidSetItemAmountFailErrorMessage");
        try
        {
            instance.setItemAmount(-0.1);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidItemAmount"), ex.getMessage());
            return;
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void setItemAmount()
    {
        System.out.println("setItemAmount");

        instance.setItemAmount(0.1);

        assertEquals(0.1, instance.getItemAmount(), 0);
    }

    @Test
    public void setItemAmountExactToMin()
    {
        System.out.println("setItemAmountExactToMin");

        instance.setItemAmount(0.001);

        assertEquals(0.001, instance.getItemAmount(), 0);
    }

    @Test
    public void getMeasurementUnit()
    {
        System.out.println("getMeasurementUnit");
        String unit = "kg";
        assertEquals(unit, instance.getMeasurementUnit());
    }

    @Test(expected = InvalidValueException.class)
    public void setMeasurementUnitSpaceFirst()
    {
        System.out.println("setMeasurementUnitSpaceFirst");

        String unit = " aa";

        instance.setMeasurementUnit(unit);
    }

    @Test(expected = InvalidValueException.class)
    public void setMeasurementUnitSpaceLast()
    {
        System.out.println("setMeasurementUnitSpaceLast");

        String unit = "asd ";

        instance.setMeasurementUnit(unit);
    }

    @Test(expected = InvalidValueException.class)
    public void setMeasurementUnitOnlySpaces()
    {
        System.out.println("setMeasurementUnitOnlySpaces");

        String unit = "        ";

        instance.setMeasurementUnit(unit);
    }

    @Test
    public void setMeasurementUnitErrorMessage()
    {
        System.out.println("setMeasurementUnitErrorMessage");

        String unit = "        ";

        try
        {
            instance.setMeasurementUnit(unit);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidMeasurementUnit"), ex.getMessage());
            return;
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test(expected = ValueNotSetException.class)
    public void setMeasurementUnitNull()
    {
        System.out.println("setMeasurementUnitNull");

        instance.setMeasurementUnit(null);
    }

    @Test
    public void setMeasurementUnitNullErrorMessage()
    {
        System.out.println("setMeasurementUnitNullErrorMessage");

        try
        {
            instance.setMeasurementUnit(null);
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetMeasurementUnit"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test(expected = ValueNotSetException.class)
    public void setMeasurementUnitEmptyString()
    {
        System.out.println("setMeasurementUnitEmptyString");

        instance.setMeasurementUnit("");
    }

    @Test
    public void setMeasurementUnitEmptyStringErrorMessage()
    {
        System.out.println("setMeasurementUnitEmptyStringErrorMessage");
        try
        {
            instance.setMeasurementUnit("");
        }
        catch (InvalidValueException ex)
        {
            fail("Wrong exception");
            return;
        }
        catch (ValueNotSetException ex)
        {
            assertEquals(Localisation.getValuePL("notSetMeasurementUnit"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getItemVatPercentage()
    {
        System.out.println("getItemVatPercentage");
        int percentage = 23;
        assertEquals(percentage, instance.getVatPercentage(), 0);
    }

    @Test(expected = InvalidValueException.class)
    public void setItemVatPercentageGreaterThan100()
    {
        System.out.println("setItemVatPercentageGreaterThan100");

        instance.setVatPercentage(100.1);
    }

    @Test
    public void setItemVatPercentageGreaterThan100ErrorMessage()
    {
        System.out.println("setItemVatPercentageGreaterThan100ErrorMessage");

        try
        {
            instance.setVatPercentage(100.1);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidVatPercentage"), ex.getMessage());
            return;
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test(expected = InvalidValueException.class)
    public void setItemVatPercentageLessThan0()
    {
        System.out.println("setItemVatPercentageLessThan0");

        instance.setVatPercentage(-1);
    }

    @Test
    public void setItemVatPercentageErrorMessage()
    {
        System.out.println("setItemVatPercentageErrorMessage");
        try
        {
            instance.setVatPercentage(-1);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidVatPercentage"), ex.getMessage());
            return;
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void getPriceNetto()
    {
        System.out.println("getPriceNetto");
        double price = 200;
        assertEquals(price, instance.getPriceNetto(), 0);
    }

    @Test(expected = InvalidValueException.class)
    public void setItemPriceNettoLessThan0()
    {
        System.out.println("setItemPriceNettoLessThan0");

        instance.setPriceNetto(-1);
    }

    @Test
    public void setItemPriceNettoLessThan0ErrorMessage()
    {
        System.out.println("setItemPriceNettoLessThan0ErrorMessage");
        try
        {
            instance.setPriceNetto(-1);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("invalidPriceNetto"), ex.getMessage());
            return;
        }
        catch (ValueNotSetException ex)
        {
            fail("Wrong exception");
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void equals()
    {
        InvoiceItem it = new InvoiceItem.InvoiceItemBuilder()
                .setItemIndex(1)
                .setItemDescription("ItemDesc")
                .setItemAmount(12)
                .setMeasurementUnit("kg")
                .setVatPercentage(23)
                .setPriceNetto(200)
                .buildInvoiceItem();

        assertTrue("Equals returned false", it.equals(instance));
    }

    @Test
    public void equalsTheSame()
    {
        assertTrue(instance.equals(instance));
    }

    @Test
    public void equalsNotInstanceOf()
    {
        assertFalse(instance.equals(new Object()));
    }

    @Test
    public void hashcode()
    {
        InvoiceItem it = new InvoiceItem.InvoiceItemBuilder()
                .setItemIndex(1)
                .setItemDescription("ItemDesc")
                .setItemAmount(12)
                .setMeasurementUnit("kg")
                .setVatPercentage(23)
                .setPriceNetto(200)
                .buildInvoiceItem();

        assertEquals("hashcode is not equal", it.hashCode(), instance.hashCode());
    }
}
