package uk.co.sling.local_date;

import java.time.Month;
import org.junit.After;
import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.Test;
import sun.security.util.Cache;

/**
 *
 * @author intrex
 */
public class LocalDateTimeEUFormatTest
{
    private LocalDateTimeEUFormat instance;

    @Before
    public void setUp()
    {
        instance = LocalDateTimeEUFormat.of(3, Month.MARCH, 2017);
    }

    @After
    public void tearDown()
    {
        instance = null;
    }

    @Test
    public void toStringTestNoTime()
    {
        System.out.println("testToStringNoTime");
        String date = "00-00-00-03-03-2017";
        assertEquals(date, instance.toStringWithTime());
    }

    @Test
    public void toStringTestWithTimeLessThan10()
    {
        System.out.println("toStringTestWithTimeLessThan10");
        String date = "05-03-03-03-03-2017";
        instance = instance.plusHours(5).plusMinutes(3).plusSeconds(3);
        assertEquals(date, instance.toStringWithTime());
    }

    @Test
    public void toStringTestWithTimeGreaterThan10()
    {
        System.out.println("toStringTestWithTimeGreaterThan10");
        String date = "15-13-13-03-03-2017";
        instance = instance.plusHours(15).plusMinutes(13).plusSeconds(13);
        assertEquals(date, instance.toStringWithTime());
    }

    @Test
    public void toStringTestMonthLessThan10()
    {
        System.out.println("toStringTestMonthLessThan10");
        String date = "03-03-2017";
        assertEquals(date, instance.toString());
    }

    @Test
    public void toStringTestDaysLessThan10()
    {
        System.out.println("toStringTestDaysLessThan10");
        String date = "03-03-2017";
        assertEquals(date, instance.toString());
    }

    @Test
    public void toStringTestMonthGreaterThan10()
    {
        System.out.println("toStringTestMonthGreaterThan10");
        String date = "03-12-2017";
        assertEquals(date, instance.plusMonths(9).toString());
    }

    @Test
    public void toStringTestDaysGreaterThan10()
    {
        System.out.println("toStringTestDaysGreaterThan10");
        String date = "13-03-2017";
        assertEquals(date, instance.plusDays(10).toString());
    }

    @Test
    public void toStringTestUSFormat()
    {
        System.out.println("toStringTestUSFormat");
        String date = "2017-03-13";
        assertEquals(date, instance.plusDays(10).toStringUSFormat());
    }
}
