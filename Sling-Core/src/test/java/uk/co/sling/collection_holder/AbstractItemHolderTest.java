package uk.co.sling.collection_holder;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.locale.Localisation;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests implementation of methods implemented in AbstractItemHolder The
 * abstract method getItem is not tested as the implementation is not provided
 *
 *
 * @author Sebastian Wojtkowiak
 */
public class AbstractItemHolderTest
{
    private AbstractItemHolderImpl<MyContainer> instance;

    @Before
    public void setUp()
    {
        instance = new AbstractItemHolderImpl<>();
    }

    @After
    public void tearDown()
    {
        instance = null;
    }

    @Test
    public void getAllItemsSuccess()
    {
        System.out.println("getAllItemsSuccess");
        MyContainer o = new MyContainer(5, 5);
        instance.addItem(o);
        assertTrue(instance.getAllItems().contains(o));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getAllItemsAddToCollection()
    {
        System.out.println("getAllItemsAddToCollection");
        MyContainer o = new MyContainer(5, 5);
        instance.addItem(o);
        instance.getAllItems().add(new MyContainer(5, 5));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getAllItemsRemoveFromCollection()
    {
        System.out.println("getAllItemsRemoveFromCollection");
        MyContainer o = new MyContainer(5, 5);
        instance.addItem(o);
        instance.getAllItems().remove(o);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getAllItemsAddCollectionToCollection()
    {
        System.out.println("getAllItemsAddCollectionToCollection");
        MyContainer o = new MyContainer(5, 5);
        instance.addItem(o);
        instance.getAllItems().addAll(Arrays.asList(new MyContainer(5, 5)));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getAllItemsRemoveCollectionToCollection()
    {
        System.out.println("getAllItemsRemoveCollectionToCollection");
        MyContainer o = new MyContainer(5, 5);
        instance.addItem(o);
        Collection<MyContainer> col = Arrays.asList(o);
        instance.getAllItems().removeAll(col);
    }

    @Test
    public void getAllItemsNotNull()
    {
        System.out.println("getAllItemsNotNull");

        assertNotNull(instance.getAllItems());
    }

    @Test
    public void getAllItemsEmpty()
    {
        System.out.println("getAllItemsEmpty");
        MyContainer o = new MyContainer(5, 5);
        instance.addItem(o);
        instance.removeItem(o);

        assertTrue(instance.getAllItems().isEmpty());
    }

    @Test(expected = InvalidValueException.class)
    public void addItemNull()
    {
        System.out.println("addItemNull");
        instance.addItem(null);
    }

    @Test
    public void addItemNullErrorMessage()
    {
        System.out.println("addItemNullErrorMessage");
        try
        {
            instance.addItem(null);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("errorAddingItem"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void addItemSuccess()
    {
        System.out.println("addItemSuccess");
        MyContainer o = new MyContainer(5, 5);
        assertTrue(instance.addItem(o));
    }

    @Test
    public void addItemTheSameTwice()
    {
        System.out.println("addItemTheSameTwice");
        MyContainer o = new MyContainer(5, 5);
        instance.addItem(o);

        assertTrue("Didn't return true while adding the same item again", instance.addItem(o));
    }

    @Test
    public void addItemWithTheSameValuesTwice()
    {
        System.out.println("addItemWithTheSameValuesTwice");

        MyContainer mc = new MyContainer(5, 5);
        MyContainer mc2 = new MyContainer(5, 5);

        instance.addItem(mc);

        assertEquals(mc2.getValue2(), instance.getAllItems().iterator().next().getValue2());
    }

    @Test(expected = InvalidValueException.class)
    public void removeItemNull()
    {
        System.out.println("removeItemNull");
        instance.removeItem(null);
    }

    @Test
    public void removeItemNullErrorMessage()
    {
        try
        {
            System.out.println("removeItemNullErrorMessage");
            instance.removeItem(null);
        }
        catch (InvalidValueException ex)
        {
            assertEquals(Localisation.getValuePL("nonExistingItem"), ex.getMessage());
            return;
        }

        fail("Didn't throw an exception");
    }

    @Test
    public void removeItemNotInCollection()
    {
        System.out.println("removeItemNotInCollection");
        MyContainer o1 = new MyContainer(5, 5);
        MyContainer o2 = new MyContainer(3, 5);

        instance.addItem(o1);

        assertFalse("Remove non-existing didn't return false", instance.removeItem(o2));
    }

    @Test
    public void removeItemSuccess()
    {
        System.out.println("removeItemSuccess");
        MyContainer o = new MyContainer(5, 5);
        instance.addItem(o);
        assertTrue(instance.removeItem(o));
    }

    @Test
    public void removeAllNotEmpty()
    {
        System.out.println("removeAllNotEmpty");
        AbstractItemHolderTest.MyContainer o = new AbstractItemHolderTest.MyContainer(5, 5);
        instance.addItem(o);
        assertTrue(instance.removeAll());
    }

    @Test
    public void removeAllNotEmptyCheckSize()
    {
        System.out.println("removeAllNotEmpty");
        AbstractItemHolderTest.MyContainer o = new AbstractItemHolderTest.MyContainer(5, 5);
        instance.addItem(o);
        instance.removeAll();
        assertEquals(0, instance.getAllItems().size());
    }

    @Test
    public void removeAllEmpty()
    {
        System.out.println("removeAllEmpty");
        assertFalse(instance.removeAll());
    }

    @Test
    public void reAdd()
    {
        System.out.println("reAdd");
        AbstractItemHolderTest.MyContainer o = new AbstractItemHolderTest.MyContainer(5, 5);
        int newValue2 = 11;
        instance.addItem(o);
        instance.addItem(new AbstractItemHolderTest.MyContainer(5, newValue2));

        assertEquals(newValue2, instance.getAllItems().iterator().next().getValue2());
    }

    private class AbstractItemHolderImpl<T extends AbstractItem> extends AbstractItemHolder<T>
    {
        @Override
        public T getItem(String uniqueKey) throws InvalidValueException
        {
            return null;
        }
    }

    private class MyContainer extends AbstractItem
    {
        private final int value1;
        private final int value2;

        public MyContainer(int value1, int value2)
        {
            this.value1 = value1;
            this.value2 = value2;
        }

        public int getValue1()
        {
            return value1;
        }

        public int getValue2()
        {
            return value2;
        }


        @Override
        public boolean equals(Object item)
        {
            if (this == item)
                return true;

            if (!(item instanceof MyContainer))
                return false;

            MyContainer mc = (MyContainer)item;

            return value1 == mc.getValue1();

        }

        @Override
        public int hashCode()
        {
            int prime = 31;
            int prime2 = 97;

            return prime * prime2 + value1;
        }
    }
}
