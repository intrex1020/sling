package uk.co.sling.errorloging;

import java.io.*;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 *
 * @author Sebastian Wojtkowiak
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(PrintWriter.class)
public class WriteErrorLogTest
{
    @After
    public void deleteFiles()
    {
        File f = new File("errorLogs");

        if (f != null)
        {

            try
            {
                for (File ff : f.listFiles())
                    if (ff.isFile())
                        ff.delete();

                f.delete();
            }
            catch (Exception e) {}
        }
    }

    @Test
    public void testIsDirCreated()
    {
        System.out.println("testIsDirCreated");

        try
        {
            throw new RuntimeException();
        }
        catch (RuntimeException ex)
        {
            WriteErrorLog.writeErrorLog(ex);

            assertTrue(new File("errorLogs").exists());
        }
    }

    @Test
    public void testIsLogCreated()
    {
        System.out.println("testIsLogCreated");

        try
        {
            throw new RuntimeException();
        }
        catch (RuntimeException ex)
        {
            WriteErrorLog.writeErrorLog(ex);

            String[] files = new File("errorLogs").list();

            assertNotNull(files);
            assertTrue(files.length > 0);
        }
    }

    @Test
    public void testReturnsTrue()
    {
        System.out.println("testReturnsTrue");

        try
        {
            throw new RuntimeException();
        }
        catch (RuntimeException ex)
        {
            assertTrue(WriteErrorLog.writeErrorLog(ex));
        }
    }

    @Test
    public void testWriteLogFileDoesntExist() throws Exception
    {
        File mockFile = mock(File.class);
        when(mockFile.exists()).thenReturn(false);
        when(mockFile.getParentFile()).thenReturn(mockFile);
        when(mockFile.getParentFile().mkdirs()).thenReturn(true);
        when(mockFile.createNewFile()).thenReturn(true);
        Whitebox.setInternalState(WriteErrorLog.class, "LOG_FILE", mockFile);

        assertFalse(WriteErrorLog.writeErrorLog(new Exception()));
    }

    @Test
    public void testWriteLogIOException() throws Exception
    {
        File mockFile = mock(File.class);
        when(mockFile.exists()).thenReturn(false);
        when(mockFile.getParentFile()).thenReturn(mockFile);
        when(mockFile.getParentFile().mkdirs()).thenReturn(true);
        when(mockFile.createNewFile()).thenThrow(new IOException());
        Whitebox.setInternalState(WriteErrorLog.class, "LOG_FILE", mockFile);

        assertFalse(WriteErrorLog.writeErrorLog(new Exception()));
    }
}
