package uk.co.sling.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.co.sling.containers.*;
import uk.co.sling.local_date.LocalDateTimeEUFormat;
import uk.co.sling.utils.PaymentType;

import javax.imageio.ImageIO;
import java.io.File;

import static org.junit.Assert.assertEquals;
import static uk.co.sling.database.InvoiceItemDB.*;

public class InvoiceItemDBTest
{
	private static File db = new File("data/data.db");
	private Contractor contractorInstance;
	private User userInstance;
	private Invoice invoiceInstance;
	private Setting settingInstance;
	private InvoiceItem itemInstance;
	private ConnectionManager cm;

	@Before
	public void setUp() throws Exception
	{
		userInstance = new User.UserBuilder()
				.setUserName("Username")
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildUser();

		UserDB.insert(userInstance);

		contractorInstance = new Contractor.ContractorBuilder()
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildContractor();

		ContractorDB.insert(userInstance, contractorInstance);

		settingInstance = new Setting.SettingBuilder()
				.setAccountNumber("1235342")
				.setBankName("My bank")
				.setWaterMark(ImageIO.read(new File(ClassLoader.getSystemResource("testImage.png").getFile())))
				.buildSetting();

		invoiceInstance = new Invoice.InvoiceBuilder()
				.setDaysToPay(2)
				.setInvoiceNumber(15)
				.setIssueDate(LocalDateTimeEUFormat.of(12, 7, 2017))
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.BANK_TRANSFER)
				.setSetting(settingInstance)
				.buildInvoice();

		InvoiceDB.insert(invoiceInstance);

		itemInstance = new InvoiceItem.InvoiceItemBuilder()
				.setItemIndex(1)
				.setItemDescription("ItemDesc")
				.setItemAmount(12)
				.setMeasurementUnit("kg")
				.setVatPercentage(23)
				.setPriceNetto(200)
				.buildInvoiceItem();

		cm = ConnectionManager.getConnectionInstance();
	}

	@After
	public void tearDown() throws Exception
	{
		//manager = null;
		userInstance = null;
		contractorInstance = null;
		settingInstance = null;
		invoiceInstance = null;
		cm.close();
		db.delete();
	}

	@Test
	public void testInsert() throws Exception
	{
		System.out.println("testInsert");
		invoiceInstance.addItem(itemInstance);
		insert(cm, invoiceInstance);
		invoiceInstance.removeAll();

		getItems(cm, invoiceInstance);
		assertEquals(itemInstance, invoiceInstance.getAllItems().iterator().next());
	}

	@Test
	public void testInsertManagerNull() throws Exception
	{
		System.out.println("testInsertManagerNull");
		insert(null, invoiceInstance);
	}

	@Test
	public void testInsertInvoiceNull() throws Exception
	{
		System.out.println("testInsertInvoiceNull");
		insert(cm, null);
	}

	@Test
	public void testInsertMultipleElements() throws Exception
	{
		System.out.println("testInsertMultipleElements");
		invoiceInstance.addItem(itemInstance);
		InvoiceItem it = itemInstance.copy();
		it.setItemIndex(2);
		invoiceInstance.addItem(it);
		insert(cm, invoiceInstance);
		invoiceInstance.removeAll();

		getItems(cm, invoiceInstance);
		assertEquals(2, invoiceInstance.getAllItems().size());
	}

	@Test
	public void testUpdate() throws Exception
	{
		System.out.println("testUpdate");
		invoiceInstance.addItem(itemInstance);
		insert(cm, invoiceInstance);
		String newDescription = "New Description";
		itemInstance.setItemDescription(newDescription);
		invoiceInstance.removeAll();
		invoiceInstance.addItem(itemInstance);
		update(cm, invoiceInstance);
		invoiceInstance.removeAll();

		getItems(cm, invoiceInstance);
		assertEquals(newDescription, invoiceInstance.getAllItems().iterator().next().getItemDescription());
	}

	@Test
	public void testUpdateManagerNull() throws Exception
	{
		System.out.println("testUpdateManagerNull");
		update(null, invoiceInstance);
	}

	@Test
	public void testUpdateInvoiceNull() throws Exception
	{
		System.out.println("testUpdateInvoiceNull");
		update(cm, null);
	}

	@Test
	public void testUpdateMultiple() throws Exception
	{
		System.out.println("testUpdateMultiple");
		invoiceInstance.addItem(itemInstance);
		InvoiceItem it = itemInstance.copy();
		it.setItemIndex(2);
		invoiceInstance.addItem(it);
		insert(cm, invoiceInstance);
		String desc1= "Description 1";
		String desc2 = "Description 2";
		itemInstance = invoiceInstance.getItem("1");
		itemInstance.setItemDescription(desc1);
		it = invoiceInstance.getItem("2");
		it.setItemDescription(desc2);
		invoiceInstance.removeAll();
		invoiceInstance.addItem(it);
		invoiceInstance.addItem(itemInstance);
		update(cm, invoiceInstance);
		invoiceInstance.removeAll();

		getItems(cm, invoiceInstance);
		assertEquals(2, invoiceInstance.getAllItems().size());
		assertEquals(desc1, invoiceInstance.getItem("1").getItemDescription());
		assertEquals(desc2, invoiceInstance.getItem("2").getItemDescription());
	}

	@Test
	public void testUpdateNonExistingTable() throws Exception
	{
		System.out.println("testUpdateNonExistingTable");
		update(cm, invoiceInstance);
	}

	@Test
	public void testUpdateEmptyTable() throws Exception
	{
		System.out.println("testUpdateEmptyTable");
		cm.getStatement(CREATE_TABLE).execute();

		update(cm, invoiceInstance);
	}
}