package uk.co.sling.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.co.sling.containers.*;
import uk.co.sling.exceptions.SQLFetchException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.local_date.LocalDateTimeEUFormat;
import uk.co.sling.utils.PaymentType;

import javax.imageio.ImageIO;
import java.io.File;
import java.sql.SQLException;

import static org.junit.Assert.*;
import static uk.co.sling.database.InvoiceSettingDB.*;

public class InvoiceSettingDBTest
{
	private static File db = new File("data/data.db");
	private User userInstance;
	private Invoice invoiceInstance;
	private Setting settingInstance;
	private Contractor contractorInstance;
	private ConnectionManager cm;

	@Before
	public void setUp() throws Exception
	{
		userInstance = new User.UserBuilder()
				.setUserName("Username")
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildUser();

		contractorInstance = new Contractor.ContractorBuilder()
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildContractor();

		settingInstance = new Setting.SettingBuilder()
				.setAccountNumber("1235342")
				.setBankName("My bank")
				.setWaterMark(ImageIO.read(new File(ClassLoader.getSystemResource("testImage.png").getFile())))
				.buildSetting();

		invoiceInstance = new Invoice.InvoiceBuilder()
				.setDaysToPay(2)
				.setInvoiceNumber(15)
				.setIssueDate(LocalDateTimeEUFormat.of(12, 7, 2017))
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.BANK_TRANSFER)
				.setSetting(settingInstance)
				.buildInvoice();

		UserDB.insert(userInstance);
		ContractorDB.insert(userInstance, contractorInstance);
		InvoiceDB.insert(invoiceInstance);

		cm = ConnectionManager.getConnectionInstance();
	}

	@After
	public void tearDown() throws Exception
	{
		cm.close();
		cm = null;
		userInstance = null;
		invoiceInstance = null;
		settingInstance = null;
		contractorInstance = null;
		db.delete();
	}

	@Test
	public void testInsertToDB() throws Exception
	{
		System.out.println("testInsertToDB");
		cm.getStatement("DELETE FROM " + TABLE_NAME).execute();
		insert(cm, invoiceInstance);

		assertNotNull("DB doesn't have the invoice setting", getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()));
	}

	@Test (expected = SQLException.class)
	public void testInsertSameItemTwice() throws Exception
	{
		System.out.println("testInsertSameItemTwice");
		insert(cm, invoiceInstance);
		insert(cm, invoiceInstance);
	}

	@Test
	public void testInsertNullManager() throws Exception
	{
		System.out.println("testInsertNullManager");
		insert(null, invoiceInstance);
	}

	@Test
	public void testInsertNullInvoice() throws Exception
	{
		System.out.println("testInsertNullInvoice");
		insert(cm, null);
	}

	@Test
	public void testInsertNullWaterMark() throws Exception
	{
		System.out.println("testInsertNullWaterMark");
		cm.getStatement("DELETE FROM " + TABLE_NAME).execute();
		settingInstance.setWaterMark(null);
		invoiceInstance.setSetting(settingInstance);
		insert(cm, invoiceInstance);
	}

	@Test
	public void testUpdateNullManager() throws Exception
	{
		System.out.println("testUpdateNullManager");
		update(null, invoiceInstance);
	}

	@Test
	public void testUpdateNullInvoice() throws Exception
	{
		System.out.println("testUpdateNullInvoice");
		update(cm, null);
	}

	@Test
	public void testUpdateTableDoesntExistsButNoEntry() throws Exception
	{
		System.out.println("testUpdateTableDoesntExistsButNoEntry");

		update(cm, invoiceInstance);
	}

	@Test
	public void testUpdateTableExistsButEmpty() throws Exception
	{
		System.out.println("testUpdateTableDoesntExistsButNoEntry");
		cm.getStatement(CREATE_TABLE).execute();

		update(cm, invoiceInstance);
	}

	@Test
	public void testUpdateAccountNumberDidntChange() throws Exception
	{
		System.out.println("testUpdateAccountNumberDidntChange");
		update(cm, invoiceInstance);

		assertEquals("Account number name has changed", invoiceInstance.getSetting().getAccountNumber(),
				getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getAccountNumber());
	}

	@Test
	public void testUpdateBankNameDidntChange() throws Exception
	{
		System.out.println("testUpdateBankNameDidntChange");
		update(cm, invoiceInstance);

		assertEquals("Bank name has changed", invoiceInstance.getSetting().getBankName(),
				getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getBankName());
	}

	@Test
	public void testUpdateWaterMarkDidntChange() throws Exception
	{
		System.out.println("testUpdateWaterMarkDidntChange");
		update(cm, invoiceInstance);

		assertEquals("Water mark name has changed", invoiceInstance.getSetting().getWaterMark(),
				getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getWaterMark());
	}

	@Test
	public void testUpdateWaterMarkDidntChangeNull() throws Exception
	{
		System.out.println("testUpdateWaterMarkDidntChangeNull");
		settingInstance.setWaterMark(null);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertEquals("Water mark name has changed", invoiceInstance.getSetting().getWaterMark(),
				getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getWaterMark());
	}

	@Test
	public void testUpdateAccountNumberChanged() throws Exception
	{
		System.out.println("testUpdateAccountNumberChanged");
		cm.getStatement(CREATE_TABLE).execute();

		String newNumber = "656673345";
		settingInstance.setAccountNumber(newNumber);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertEquals("Account number name hasn't changed", newNumber, getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getAccountNumber());
	}

	@Test
	public void testUpdateBankNameChanged() throws Exception
	{
		System.out.println("testUpdateBankNameChanged");

		String newBankName = "My super pro bank";
		settingInstance.setBankName(newBankName);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertEquals("Bank name hasn't changed", newBankName, getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getBankName());
	}

	@Test
	public void testWaterMarkUserChanged() throws Exception
	{
		System.out.println("testWaterMarkUserChanged");

		settingInstance.setWaterMark(null);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertNull(getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getWaterMark());
	}

	@Test (expected = SQLException.class)
	public void testGetSettingEmptyTable() throws Exception
	{
		System.out.println("testGetSettingEmptyTable");
		cm.getStatement("DELETE FROM " + TABLE_NAME).execute();

		getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode());
	}

	@Test (expected = SQLFetchException.class)
	public void testGetSettingWhileTableDoesntExist() throws Exception
	{
		System.out.println("testGetSettingWhileTableDoesntExist");
		cm.getStatement("DROP TABLE " + TABLE_NAME).execute();

		assertNull(getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()));
	}

	@Test
	public void testGetSetting() throws Exception
	{
		System.out.println("testGetSetting");

		assertNotNull("Setting not fetched", getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()));
	}

	@Test (expected = ValueNotSetException.class)
	public void testGetSettingManagerNull() throws Exception
	{
		System.out.println("testGetSettingManagerNull");
		getSetting(null, userInstance, invoiceInstance.getInvoiceUniqueCode());
	}

	@Test (expected = ValueNotSetException.class)
	public void testGetSettingUserNull() throws Exception
	{
		System.out.println("testGetSettingUserNull");
		getSetting(cm, null, invoiceInstance.getInvoiceUniqueCode());
	}

	@Test (expected = ValueNotSetException.class)
	public void testGetSettingInvoiceNull() throws Exception
	{
		System.out.println("testGetSettingInvoiceNull");
		getSetting(cm, userInstance, null);
	}

	@Test
	public void testInsertLeftSlash() throws Exception
	{
		System.out.println("testInsertLeftSlash");
		String bankName = "\\";

		settingInstance.setBankName(bankName);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getBankName());
	}

	@Test
	public void testInsertRightSlash() throws Exception
	{
		System.out.println("testInsertRightSlash");
		String bankName = "//";

		settingInstance.setBankName(bankName);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getBankName());
	}

	@Test
	public void testInsertPeriod() throws Exception
	{
		System.out.println("testInsertPeriod");
		String bankName = ".";

		settingInstance.setBankName(bankName);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getBankName());
	}

	@Test
	public void testInsertSemicolon() throws Exception
	{
		System.out.println("testInsertPeriod");
		String bankName = ";";

		settingInstance.setBankName(bankName);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getBankName());
	}

	@Test
	public void testInsertTylda() throws Exception
	{
		System.out.println("testInsertTylda");
		String bankName = "~";

		settingInstance.setBankName(bankName);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getBankName());
	}

	@Test
	public void testInsertWeirdCharacter() throws Exception
	{
		System.out.println("testInsertWeirdCharacter");
		String bankName = "¬";

		settingInstance.setBankName(bankName);
		invoiceInstance.setSetting(settingInstance);
		update(cm, invoiceInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(cm, userInstance, invoiceInstance.getInvoiceUniqueCode()).getBankName());
	}
}