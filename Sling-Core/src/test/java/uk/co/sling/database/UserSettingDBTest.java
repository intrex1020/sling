package uk.co.sling.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.co.sling.containers.Setting;
import uk.co.sling.containers.User;
import uk.co.sling.exceptions.SQLFetchException;
import uk.co.sling.exceptions.ValueNotSetException;

import javax.imageio.ImageIO;
import java.io.File;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static uk.co.sling.database.UserSettingDB.*;

public class UserSettingDBTest
{
	private static File db = new File("data/data.db");
	private User userInstance;
	private Setting settingInstance;

	@Before
	public void setUp() throws Exception
	{
		userInstance = new User.UserBuilder()
				.setUserName("Username")
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildUser();

		settingInstance = new Setting.SettingBuilder()
				.setAccountNumber("1235342")
				.setBankName("My bank")
				.setWaterMark(ImageIO.read(new File(ClassLoader.getSystemResource("testImage.png").getFile())))
				.buildSetting();

		UserDB.insert(userInstance);
	}

	@After
	public void tearDown() throws Exception
	{
		userInstance = null;
		settingInstance = null;
		db.delete();
	}

	@Test
	public void testInsertToDB() throws Exception
	{
		System.out.println("testInsertToDB");
		insert(userInstance, settingInstance);

		assertNotNull("DB doesn't have the setting", getSetting(userInstance));
	}

	@Test
	public void testInsertSameItemTwice() throws Exception
	{
		System.out.println("testInsertSameItemTwice");
		insert(userInstance, settingInstance);
		insert(userInstance, settingInstance);
	}

	@Test
	public void testInsertNullUser() throws Exception
	{
		System.out.println("testInsertNullUser");
		insert(null, settingInstance);
	}

	@Test
	public void testInsertNullSetting() throws Exception
	{
		System.out.println("testInsertNullSetting");
		insert(null, settingInstance);
	}

	@Test
	public void testInsertNullWaterMark() throws Exception
	{
		System.out.println("testInsertNullWaterMark");
		settingInstance.setWaterMark(null);
		insert(userInstance, settingInstance);
	}

	@Test (expected = SQLException.class)
	public void testGetSettingEmptyTable() throws Exception
	{
		System.out.println("testGetSettingEmptyTable");
		ConnectionManager.getConnectionInstance().getStatement(CREATE_TABLE).execute();

		getSetting(userInstance);
	}

	@Test (expected = SQLFetchException.class)
	public void testGetSettingWhileTableDoesntExist() throws Exception
	{
		System.out.println("testGetSettingWhileTableDoesntExist");

		assertNull(getSetting(userInstance));
	}

	@Test
	public void testGetSetting() throws Exception
	{
		System.out.println("testGetSetting");
		insert(userInstance, settingInstance);

		assertNotNull("Setting not fetched", getSetting(userInstance));
	}

	@Test (expected = ValueNotSetException.class)
	public void testGetSettingUserNull() throws Exception
	{
		System.out.println("testGetSettingUserNull");
		getSetting(null);
	}

	@Test
	public void testGetWaterMarkNotNull() throws Exception
	{
		System.out.println("testGetWaterMarkNotNull");
		insert(userInstance, settingInstance);

		assertNotNull("Fetched blob is null", getSetting(userInstance).getWaterMark());
	}

	@Test
	public void testInsertLeftSlash() throws Exception
	{
		System.out.println("testInsertLeftSlash");
		String bankName = "\\";

		settingInstance.setBankName(bankName);
		insert(userInstance, settingInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(userInstance).getBankName());
	}

	@Test
	public void testInsertRightSlash() throws Exception
	{
		System.out.println("testInsertRightSlash");
		String bankName = "//";

		settingInstance.setBankName(bankName);
		insert(userInstance, settingInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(userInstance).getBankName());
	}

	@Test
	public void testInsertPeriod() throws Exception
	{
		System.out.println("testInsertPeriod");
		String bankName = ".";

		settingInstance.setBankName(bankName);
		insert(userInstance, settingInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(userInstance).getBankName());
	}

	@Test
	public void testInsertSemicolon() throws Exception
	{
		System.out.println("testInsertPeriod");
		String bankName = ";";

		settingInstance.setBankName(bankName);
		insert(userInstance, settingInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(userInstance).getBankName());
	}

	@Test
	public void testInsertTylda() throws Exception
	{
		System.out.println("testInsertTylda");
		String bankName = "~";

		settingInstance.setBankName(bankName);
		insert(userInstance, settingInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(userInstance).getBankName());
	}

	@Test
	public void testInsertWeirdCharacter() throws Exception
	{
		System.out.println("testInsertWeirdCharacter");
		String bankName = "¬";

		settingInstance.setBankName(bankName);
		insert(userInstance, settingInstance);

		assertEquals("Doesn't contain slash", bankName, getSetting(userInstance).getBankName());
	}
}