package uk.co.sling.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * Tests if the database is initialised properly and database file is created
 *
 * @author
 */
public class ConnectionManagerTest
{
	private ConnectionManager manager;
	private File db = new File("data/data.db");

	@Before
	public void setUp() throws Exception
	{
		manager = ConnectionManager.getConnectionInstance();
	}

	@After
	public void tearDown() throws Exception
	{
		manager.close();

		manager = null;

		db.delete();
	}

	@Test
	public void isDatabaseFileCreated()
	{
		assertTrue("Database file not created", db.exists());
	}
}