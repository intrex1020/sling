package uk.co.sling.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.co.sling.containers.User;
import uk.co.sling.exceptions.SQLFetchException;
import uk.co.sling.exceptions.ValueNotSetException;

import java.io.File;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static uk.co.sling.database.UserDB.*;

public class UserDBTest
{
	private static File db = new File("data/data.db");
	private User userInstance;

	@Before
	public void setUp() throws Exception
	{
		userInstance = new User.UserBuilder()
				.setUserName("Username")
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildUser();
	}

	@After
	public void tearDown() throws Exception
	{
		//manager = null;
		userInstance = null;
		db.delete();
	}

	@Test
	public void testInsertToDB() throws Exception
	{
		System.out.println("testInsertToDB");
		insert(userInstance);

		assertTrue("DB doesn't have the user", UserDB.getUsers().contains(userInstance));
	}

	@Test (expected = SQLException.class)
	public void testInsertSameItemTwice() throws Exception
	{
		System.out.println("testInsertSameItemTwice");
		insert(userInstance);
		insert(userInstance);
	}

	@Test
	public void testRemoveFromDB() throws Exception
	{
		System.out.println("testRemoveFromDB");
		insert(userInstance);
		remove(userInstance);
	}

	@Test
	public void testRemoveNonExistingFromDB() throws Exception
	{
		System.out.println("testRemoveNonExistingFromDB");
		insert(userInstance);
		remove(userInstance);
		remove(userInstance);

		assertEquals(0, getUsers().size());
	}

	@Test
	public void testFetchUsersIfTableDoesntExist() throws Exception
	{
		System.out.println("testFetchInvoiceSettingIfTableDoesntExist");

		assertEquals(0, getUsers().size());
	}

	@Test
	public void testRemoveWhileTableNotCreated() throws Exception
	{
		System.out.println("testRemoveWhileTableNotCreated");
		remove(userInstance);
	}

	@Test
	public void testInsertNull() throws Exception
	{
		System.out.println("testInsertNull");
		insert(null);
	}

	@Test
	public void testRemoveNull() throws Exception
	{
		System.out.println("testRemoveNull");
		remove(null);
	}

	@Test
	public void testUpdateNull() throws Exception
	{
		System.out.println("testUpdateNull");
		update(null, null);
	}

	@Test
	public void testUpdateUserNullNewUserNotNull() throws Exception
	{
		System.out.println("testUpdateUserNullContractorsNotNull");
		update(null, userInstance);
	}

	@Test
	public void testUpdateUserNotNullNewUserNull() throws Exception
	{
		System.out.println("testUpdateUserNotNullContractorsNull");
		insert(userInstance);
		update(userInstance, null);

		assertTrue("Doesn't contain user", getUsers().contains(userInstance));
	}

	@Test
	public void testUpdateUserTableExistsButNoEntry() throws Exception
	{
		System.out.println("testUpdateTableDoesntExistsButNoEntry");
		insert(userInstance);
		remove(userInstance);

		update(userInstance, null);
	}

	@Test
	public void testUpdateUserDidntChangeUserName() throws Exception
	{
		System.out.println("testUpdateUserDidntChangerUserName");
		insert(userInstance);
		update(userInstance, userInstance);

		assertEquals("User name has changed", userInstance.getUserName(), getUsers().get(0).getUserName());
	}

	@Test
	public void testUpdateUserDidntChangeCompanyName() throws Exception
	{
		System.out.println("testUpdateBankNameDidntChange");
		insert(userInstance);
		update(userInstance, userInstance);

		assertEquals("Company name has changed", userInstance.getCompanyName(), getUsers().get(0).getCompanyName());
	}

	@Test
	public void testUpdateUserDidntChangeVatCode() throws Exception
	{
		System.out.println("testUpdateWaterMarkDidntChange");
		insert(userInstance);
		update(userInstance, userInstance);

		assertEquals("Vat code has changed", userInstance.getVatCode(), getUsers().get(0).getVatCode());
	}

	@Test
	public void testUpdateUserDidntChangeTown() throws Exception
	{
		System.out.println("testUpdateUserDidntChangeTown");
		insert(userInstance);
		update(userInstance, userInstance);

		assertEquals("Town has changed", userInstance.getTown(), getUsers().get(0).getTown());
	}

	@Test
	public void testUpdateUserDidntChangePostCode() throws Exception
	{
		System.out.println("testUpdateUserDidntChangePostCode");
		insert(userInstance);
		update(userInstance, userInstance);

		assertEquals("Post code has changed", userInstance.getPostCode(), getUsers().get(0).getPostCode());
	}

	@Test
	public void testUpdateUserDidntChangeStreetName() throws Exception
	{
		System.out.println("testUpdateUserDidntChangeStreetName");
		insert(userInstance);
		update(userInstance, userInstance);

		assertEquals("Street name has changed", userInstance.getStreet(), getUsers().get(0).getStreet());
	}

	@Test
	public void testUpdateUserDidntChangeBuildingNumber() throws Exception
	{
		System.out.println("testUpdateUserDidntChangeBuildingNumber");
		insert(userInstance);
		update(userInstance, userInstance);

		assertEquals("Building number has changed", userInstance.getBuildingNumber(), getUsers().get(0).getBuildingNumber());
	}

	@Test
	public void testUpdateUserDidntChangePhone() throws Exception
	{
		System.out.println("testUpdateUserDidntChangePhone");
		insert(userInstance);
		update(userInstance, userInstance);

		assertEquals("Phone has changed", userInstance.getPhone(), getUsers().get(0).getPhone());
	}

	@Test
	public void testUpdateUserDidntChangeEmail() throws Exception
	{
		System.out.println("testUpdateUserDidntChangeEmail");
		insert(userInstance);
		update(userInstance, userInstance);

		assertEquals("Email has changed", userInstance.getEmail(), getUsers().get(0).getEmail());
	}

	@Test
	public void testUpdateUserChangedUserName() throws Exception
	{
		System.out.println("testUpdateUserChangedUserName");
		insert(userInstance);

		User newUsername = new User.UserBuilder()
				.setUserName("User 2")
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildUser();

		update(userInstance, newUsername);

		assertEquals("User name hasn't changed", newUsername.getUserName(), getUsers().get(0).getUserName());
	}

	@Test
	public void testUpdateUserChangedCompanyName() throws Exception
	{
		System.out.println("testUpdateUserChangedCompanyName");
		insert(userInstance);
		userInstance.setCompanyName("My Super Company");
		update(userInstance, userInstance);

		assertEquals("Company name hasn't changed", userInstance.getCompanyName(), getUsers().get(0).getCompanyName());
	}

	@Test
	public void testUpdateUserChangedVatCode() throws Exception
	{
		System.out.println("testUpdateUserChangedVatCode");
		insert(userInstance);
		userInstance.setVatCode("53432");
		update(userInstance, userInstance);

		assertEquals("Vat code hasn't changed", userInstance.getVatCode(), getUsers().get(0).getVatCode());
	}

	@Test
	public void testUpdateUserChangedTown() throws Exception
	{
		System.out.println("testUpdateUserChangedTown");
		insert(userInstance);
		userInstance.setTown("New City");
		update(userInstance, userInstance);

		assertEquals("Town hasn't changed", userInstance.getTown(), getUsers().get(0).getTown());
	}

	@Test
	public void testUpdateUserChangedPostCode() throws Exception
	{
		System.out.println("testUpdateUserChangedPostCode");
		insert(userInstance);
		userInstance.setPhone("1337");
		update(userInstance, userInstance);

		assertEquals("Post code hasn't changed", userInstance.getPostCode(), getUsers().get(0).getPostCode());
	}

	@Test
	public void testUpdateUserChangedStreetName() throws Exception
	{
		System.out.println("testUpdateUserChangedStreetName");
		insert(userInstance);
		userInstance.setStreet("This Street");
		update(userInstance, userInstance);

		assertEquals("Street name hasn't changed", userInstance.getStreet(), getUsers().get(0).getStreet());
	}

	@Test
	public void testUpdateUserChangedBuildingNumber() throws Exception
	{
		System.out.println("testUpdateUserChangedBuildingNumber");
		insert(userInstance);
		userInstance.setBuildingNumber("13b");
		update(userInstance, userInstance);

		assertEquals("Building number hasn't changed", userInstance.getBuildingNumber(), getUsers().get(0).getBuildingNumber());
	}

	@Test
	public void testUpdateUserChangedPhone() throws Exception
	{
		System.out.println("testUpdateUserChangedPhone");
		insert(userInstance);
		userInstance.setPhone("+48 123123123");
		update(userInstance, userInstance);

		assertEquals("Phone hasn't changed", userInstance.getPhone(), getUsers().get(0).getPhone());
	}

	@Test
	public void testUpdateUserChangedEmail() throws Exception
	{
		System.out.println("testUpdateUserChangedEmail");
		insert(userInstance);
		userInstance.setEmail("myemail@email.com");
		update(userInstance, userInstance);

		assertEquals("Email hasn't changed", userInstance.getEmail(), getUsers().get(0).getEmail());
	}

	@Test
	public void testGetAllUsersEmptyTable() throws Exception
	{
		System.out.println("testGetAllUsersEmptyTable");
		ConnectionManager.getConnectionInstance().getStatement(CREATE_TABLE).execute();
		ConnectionManager.getConnectionInstance().close();
		getUsers();
	}

	@Test
	public void testGetAllUsersWhileTableDoesntExist() throws Exception
	{
		System.out.println("testGetAllContractorsWhileTableDoesntExist");
		getUsers();
	}

	@Test
	public void testGetAllUsers() throws Exception
	{
		System.out.println("testGetAllContractors");
		insert(userInstance);

		assertTrue("Returned list size is not 1", getUsers().size() == 1);
	}

	@Test
	public void testInsertLeftSlash() throws Exception
	{
		System.out.println("testInsertLeftSlash");
		String userName = "\\";

		userInstance.setUserName(userName);
		insert(userInstance);

		assertEquals("Doesn't contain slash", userName, getUsers().get(0).getUserName());
	}

	@Test
	public void testInsertRightSlash() throws Exception
	{
		System.out.println("testInsertRightSlash");
		String userName = "//";

		userInstance.setUserName(userName);
		insert(userInstance);

		assertEquals("Doesn't contain slash", userName, getUsers().get(0).getUserName());
	}

	@Test
	public void testInsertPeriod() throws Exception
	{
		System.out.println("testInsertPeriod");
		String userName = ".";

		userInstance.setUserName(userName);
		insert(userInstance);

		assertEquals("Doesn't contain period", userName, getUsers().get(0).getUserName());
	}

	@Test
	public void testInsertSemicolon() throws Exception
	{
		System.out.println("testInsertPeriod");
		String userName = ";";

		userInstance.setUserName(userName);
		insert(userInstance);

		assertEquals("Doesn't contain semicolon", userName, getUsers().get(0).getUserName());
	}

	@Test
	public void testInsertTylda() throws Exception
	{
		System.out.println("testInsertTylda");
		String userName = "~";

		userInstance.setUserName(userName);
		insert(userInstance);

		assertEquals("Doesn't contain ~", userName, getUsers().get(0).getUserName());
	}

	@Test
	public void testInsertWeirdCharacter() throws Exception
	{
		System.out.println("testInsertWeirdCharacter");
		String userName = "¬";

		userInstance.setUserName(userName);
		insert(userInstance);

		assertEquals("Doesn't contain ¬", userName, getUsers().get(0).getUserName());
	}
}