package uk.co.sling.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.co.sling.containers.*;
import uk.co.sling.local_date.LocalDateTimeEUFormat;
import uk.co.sling.utils.PaymentType;

import javax.imageio.ImageIO;
import java.io.File;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static uk.co.sling.database.InvoiceDB.*;

public class InvoiceDBTest
{
	private static File db = new File("data/data.db");
	private Contractor contractorInstance;
	private User userInstance;
	private Invoice invoiceInstance;
	private Setting settingInstance;
	private InvoiceItem itemInstance;
	private int year = LocalDate.now().getYear();

	@Before
	public void setUp() throws Exception
	{
		userInstance = new User.UserBuilder()
				.setUserName("Username")
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildUser();

		UserDB.insert(userInstance);

		contractorInstance = new Contractor.ContractorBuilder()
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildContractor();

		ContractorDB.insert(userInstance, contractorInstance);

		settingInstance = new Setting.SettingBuilder()
				.setAccountNumber("1235342")
				.setBankName("My bank")
				.setWaterMark(ImageIO.read(new File(ClassLoader.getSystemResource("testImage.png").getFile())))
				.buildSetting();

		invoiceInstance = new Invoice.InvoiceBuilder()
				.setDaysToPay(2)
				.setInvoiceNumber(15)
				.setIssueDate(LocalDateTimeEUFormat.of(12, 7, LocalDateTime.now().getYear()))
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.BANK_TRANSFER)
				.setSetting(settingInstance)
				.buildInvoice();

		itemInstance = new InvoiceItem.InvoiceItemBuilder()
				.setItemIndex(1)
				.setItemDescription("ItemDesc")
				.setItemAmount(12)
				.setMeasurementUnit("kg")
				.setVatPercentage(23)
				.setPriceNetto(200)
				.buildInvoiceItem();
	}

	@After
	public void tearDown() throws Exception
	{
		//manager = null;
		userInstance = null;
		contractorInstance = null;
		db.delete();
	}

	@Test
	public void testInsertNull() throws Exception
	{
		System.out.println("testInsertNull");
		insert(null);
	}

	@Test (expected = SQLException.class)
	public void testInsertSameItemTwice() throws Exception
	{
		System.out.println("testInsertSameItemTwice");
		insert(invoiceInstance);
		insert(invoiceInstance);
	}

	@Test
	public void testInvoiceItemAdded() throws Exception
	{
		System.out.println("testInvoiceItemAdded");
		invoiceInstance.addItem(itemInstance);
		insert(invoiceInstance);
		invoiceInstance.removeAll();
		getInvoices(userInstance, contractorInstance);

		assertNotNull("Did not save invoice items",
				contractorInstance.getAllItems().iterator().next()
						.getAllItems().iterator().next());
	}

	@Test
	public void testInvoiceSettingAdded() throws Exception
	{
		System.out.println("testInvoiceSettingAdded");
		insert(invoiceInstance);
		getInvoices(userInstance, contractorInstance);

		assertNotNull("Did not save invoice setting",
				contractorInstance.getAllItems().iterator().next()
						.getSetting());
	}

	@Test
	public void  testUpdateInvoiceNull() throws Exception
	{
		System.out.println("testUpdateInvoiceNull");
		insert(invoiceInstance);
		update(null, invoiceInstance);
	}

	@Test
	public void testUpdateNewInvoiceNull() throws Exception
	{
		System.out.println("testUpdateNewInvoiceNull");
		insert(invoiceInstance);
		update(invoiceInstance, null);
	}

	@Test
	public void testUpdateBothNull() throws Exception
	{
		System.out.println("testUpdateBothNull");
		insert(invoiceInstance);
		update(null, null);
	}

	@Test
	public void testUpdateNonExisting() throws Exception
	{
		System.out.println("testUpdateNonExisting");
		insert(invoiceInstance);
		remove(invoiceInstance);
		update(invoiceInstance, invoiceInstance);
	}

	@Test
	public void testUpdateTableDoesntExist() throws Exception
	{
		System.out.println("testUpdateTableDoesntExist");
		update(invoiceInstance, invoiceInstance);
	}

	@Test
	public void testUpdateSettingUpdated() throws Exception
	{
		System.out.println("testUpdateSettingUpdated");
		insert(invoiceInstance);
		String newBank = "Bank New";
		settingInstance.setBankName(newBank);
		invoiceInstance.setSetting(settingInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		//invoice was never added to invoice so this test should be valid one
		assertEquals(newBank, contractorInstance.getAllItems().iterator().next()
				.getSetting().getBankName());
	}

	@Test
	public void testUpdateItemUpdated() throws Exception
	{
		System.out.println("testUpdateSettingUpdated");
		insert(invoiceInstance);
		invoiceInstance.addItem(itemInstance);
		update(invoiceInstance, invoiceInstance);
		//check if that item is fetched
		invoiceInstance.removeItem(itemInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(itemInstance, contractorInstance.getAllItems().iterator().next()
				.getAllItems().iterator().next());
	}

	@Test
	public void testUpdateUniqueCodeDidntChange() throws Exception
	{
		System.out.println("testUpdateUniqueCodeDidntChange");
		insert(invoiceInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(invoiceInstance.getInvoiceUniqueCode(),
				contractorInstance.getAllItems().iterator().next()
		.getInvoiceUniqueCode());
	}

	@Test
	public void testUpdateInvoiceNumberDidntChange() throws Exception
	{
		System.out.println("testUpdateInvoiceNumberDidntChange");
		insert(invoiceInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(invoiceInstance.getInvoiceNumber(),
				contractorInstance.getAllItems().iterator().next()
						.getInvoiceNumber());
	}

	@Test
	public void testUpdateIssuedByDidntChange() throws Exception
	{
		System.out.println("testUpdateIssuedByDidntChange");
		insert(invoiceInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(invoiceInstance.getIssuedBy(),
				contractorInstance.getAllItems().iterator().next()
						.getIssuedBy());
	}

	@Test
	public void testUpdateIssuedToDidntChange() throws Exception
	{
		System.out.println("testUpdateIssuedToDidntChange");
		insert(invoiceInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(invoiceInstance.getIssuedTo(),
				contractorInstance.getAllItems().iterator().next()
						.getIssuedTo());
	}

	@Test
	public void testSettingDidntChange() throws Exception
	{
		System.out.println("testSettingDidntChange");
		insert(invoiceInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(invoiceInstance.getSetting().getBankName(),
				contractorInstance.getAllItems().iterator().next()
						.getSetting().getBankName());
	}

	@Test
	public void testItemDidntChange() throws Exception
	{
		System.out.println("testItemDidntChange");
		insert(invoiceInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertTrue(contractorInstance.getAllItems().contains(invoiceInstance));
	}

	@Test
	public void testIssueDateDidntChange() throws Exception
	{
		System.out.println("testIssueDateDidntChange");
		insert(invoiceInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(invoiceInstance.getIssueDate(),
				contractorInstance.getAllItems().iterator().next()
						.getIssueDate());
	}

	@Test
	public void testDaysToPayDidntChange() throws Exception
	{
		System.out.println("testDaysToPayDidntChange");
		insert(invoiceInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(invoiceInstance.getDaysToPay(),
				contractorInstance.getAllItems().iterator().next()
						.getDaysToPay());
	}

	@Test
	public void testPaymentTypeDidntChange() throws Exception
	{
		System.out.println("testPaymentTypeDidntChange");
		insert(invoiceInstance);
		update(invoiceInstance, invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(invoiceInstance.getPaymentType(),
				contractorInstance.getAllItems().iterator().next()
						.getPaymentType());
	}

	@Test
	public void testUpdateUniqueCodeDidChange1() throws Exception
	{
		System.out.println("testUpdateUniqueCodeDidChange1");
		insert(invoiceInstance);

		Invoice tmp = new Invoice.InvoiceBuilder()
				.setDaysToPay(2)
				.setInvoiceNumber(33)
				.setIssueDate(LocalDateTimeEUFormat.of(12, 12, 2006))
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.BANK_TRANSFER)
				.setSetting(settingInstance)
				.buildInvoice();

		update(invoiceInstance, tmp);

		getInvoices(userInstance, contractorInstance);

		assertEquals(tmp.getInvoiceUniqueCode(),
				contractorInstance.getAllItems().iterator()
						.next().getInvoiceUniqueCode());
	}

	@Test
	public void testUpdateInvoiceNumberDidChange1() throws Exception
	{
		System.out.println("testUpdateInvoiceNumberDidChange1");
		insert(invoiceInstance);

		Invoice tmp = new Invoice.InvoiceBuilder()
				.setDaysToPay(2)
				.setInvoiceNumber(9)
				.setIssueDate(LocalDateTimeEUFormat.of(12, 7, 2017))
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.BANK_TRANSFER)
				.setSetting(settingInstance)
				.buildInvoice();

		update(invoiceInstance, tmp);

		getInvoices(userInstance, contractorInstance);

		assertEquals(tmp.getInvoiceNumber(), contractorInstance.getAllItems()
				.iterator().next().getInvoiceNumber());
	}

	@Test
	public void testSettingDidChange1() throws Exception
	{
		System.out.println("testSettingDidChange1");
		insert(invoiceInstance);

		settingInstance.setBankName("New Bank Name");

		Invoice tmp = new Invoice.InvoiceBuilder()
				.setDaysToPay(2)
				.setInvoiceNumber(invoiceInstance.getInvoiceNumber())
				.setIssueDate(invoiceInstance.getIssueDate())
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.BANK_TRANSFER)
				.setSetting(settingInstance)
				.buildInvoice();

		update(invoiceInstance, tmp);

		getInvoices(userInstance, contractorInstance);

		assertEquals(settingInstance.getBankName(), contractorInstance.getAllItems()
				.iterator().next().getSetting().getBankName());
	}

	@Test
	public void testSettingDidChange2() throws Exception
	{
		System.out.println("testSettingDidChange2");
		insert(invoiceInstance);

		settingInstance.setBankName("New Bank Name");
		update(invoiceInstance, null);

		getInvoices(userInstance, contractorInstance);

		assertEquals(settingInstance.getBankName(), contractorInstance.getAllItems()
				.iterator().next().getSetting().getBankName());
	}

	@Test
	public void testItemDidChange1() throws Exception
	{
		System.out.println("testItemDidChange1");
		insert(invoiceInstance);
		invoiceInstance.removeItem(itemInstance);

		itemInstance = new InvoiceItem.InvoiceItemBuilder()
				.setItemIndex(3)
				.setItemDescription("ItemDesc")
				.setItemAmount(12)
				.setMeasurementUnit("kg")
				.setVatPercentage(23)
				.setPriceNetto(200)
				.buildInvoiceItem();

		Invoice tmp = new Invoice.InvoiceBuilder()
				.setDaysToPay(2)
				.setInvoiceNumber(invoiceInstance.getInvoiceNumber())
				.setIssueDate(invoiceInstance.getIssueDate())
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.BANK_TRANSFER)
				.setSetting(settingInstance)
				.buildInvoice();

		tmp.addItem(itemInstance);
		update(invoiceInstance, tmp);
		tmp.removeItem(itemInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(itemInstance, contractorInstance.getAllItems().iterator().next()
				.getAllItems().iterator().next());
	}

	@Test
	public void testItemDidChange2() throws Exception
	{
		System.out.println("testItemDidChange2");
		insert(invoiceInstance);
		invoiceInstance.removeItem(itemInstance);

		itemInstance = new InvoiceItem.InvoiceItemBuilder()
				.setItemIndex(3)
				.setItemDescription("ItemDesc")
				.setItemAmount(12)
				.setMeasurementUnit("kg")
				.setVatPercentage(23)
				.setPriceNetto(200)
				.buildInvoiceItem();

		invoiceInstance.addItem(itemInstance);
		update(invoiceInstance, invoiceInstance);
		invoiceInstance.removeItem(itemInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(itemInstance, contractorInstance.getAllItems().iterator().next()
				.getAllItems().iterator().next());
	}

	@Test
	public void testIssueDateDidChange2() throws Exception
	{
		System.out.println("testIssueDateDidChange2");
		insert(invoiceInstance);

		LocalDateTimeEUFormat newDate = LocalDateTimeEUFormat.of(12, 12, 2007);

		Invoice tmp = new Invoice.InvoiceBuilder()
				.setDaysToPay(2)
				.setInvoiceNumber(11)
				.setIssueDate(newDate)
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.BANK_TRANSFER)
				.setSetting(settingInstance)
				.buildInvoice();

		update(invoiceInstance, tmp);

		getInvoices(userInstance, contractorInstance);

		assertEquals(newDate, contractorInstance.getAllItems().iterator().next()
				.getIssueDate());
	}

	@Test
	public void testDaysToPayDidChange1() throws Exception
	{
		System.out.println("testDaysToPayDidChange1");
		insert(invoiceInstance);

		int daysToPay = 44;
		Invoice tmp = new Invoice.InvoiceBuilder()
				.setDaysToPay(daysToPay)
				.setInvoiceNumber(11)
				.setIssueDate(LocalDateTimeEUFormat.of(12, 12, 2007))
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.BANK_TRANSFER)
				.setSetting(settingInstance)
				.buildInvoice();
		update(invoiceInstance, tmp);

		getInvoices(userInstance, contractorInstance);

		assertEquals(daysToPay, contractorInstance.getAllItems().iterator().next()
				.getDaysToPay());
	}

	@Test
	public void testDaysToPayDidChange2() throws Exception
	{
		System.out.println("testDaysToPayDidChange2");
		insert(invoiceInstance);

		int daysToPay = 44;
		invoiceInstance.setDaysToPay(daysToPay);
		update(invoiceInstance, null);

		getInvoices(userInstance, contractorInstance);

		assertEquals(daysToPay, contractorInstance.getAllItems().iterator().next()
				.getDaysToPay());
	}

	@Test
	public void testPaymentTypeDidChange1() throws Exception
	{
		System.out.println("testPaymentTypeDidChange1");
		insert(invoiceInstance);

		Invoice tmp = new Invoice.InvoiceBuilder()
				.setDaysToPay(3)
				.setInvoiceNumber(11)
				.setIssueDate(LocalDateTimeEUFormat.of(12, 12, 2007))
				.setIssuedBy(userInstance)
				.setIssuedTo(contractorInstance)
				.setIsPaid(false)
				.setPaymentType(PaymentType.CASH)
				.setSetting(settingInstance)
				.buildInvoice();
		update(invoiceInstance, tmp);

		getInvoices(userInstance, contractorInstance);

		assertEquals(PaymentType.CASH, contractorInstance.getAllItems().iterator().next()
				.getPaymentType());
	}

	@Test
	public void testPaymentTypeDidChange2() throws Exception
	{
		System.out.println("testPaymentTypeDidChange2");
		insert(invoiceInstance);

		invoiceInstance.setPaymentType(PaymentType.CASH);
		update(invoiceInstance, null);

		getInvoices(userInstance, contractorInstance);

		assertEquals(PaymentType.CASH, contractorInstance.getAllItems().iterator().next()
				.getPaymentType());
	}

	@Test
	public void testInvoiceRemoved() throws Exception
	{
		System.out.println("testInvoiceRemoved");
		insert(invoiceInstance);
		remove(invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(0, contractorInstance.getAllItems().size());
	}

	@Test
	public void testRemoveNull() throws Exception
	{
		System.out.println("testRemoveNull");
		remove(null);
	}

	@Test
	public void testRemoveNonExisting() throws Exception
	{
		System.out.println("testRemoveNonExisting");
		insert(invoiceInstance);
		remove(invoiceInstance);
		remove(invoiceInstance);
	}

	@Test
	public void testRemoveNonExistingTable() throws Exception
	{
		System.out.println("testRemoveNonExistingTable");
		remove(invoiceInstance);
	}

	@Test
	public void testGetMaxNumber() throws Exception
	{
		System.out.println("testGetMaxNumber");
		insert(invoiceInstance);

		assertEquals(15, getLatestInvoiceNumberForYear(userInstance, year));
	}

	@Test
	public void testGetMaxNumberUserNull() throws Exception
	{
		System.out.println("testGetMaxNumberUserNull");
		insert(invoiceInstance);

		assertEquals(1, getLatestInvoiceNumberForYear(null, year));
	}

	@Test
	public void testGetMaxNumberTableDoesntExist() throws Exception
	{
		System.out.println("testGetMaxNumberTableDoesntExist");

		assertEquals(1, getLatestInvoiceNumberForYear(null, year));
	}

	@Test
	public void testGetMaxNumberTableEmpty() throws Exception
	{
		System.out.println("testGetMaxNumberUserNull");
		insert(invoiceInstance);
		remove(invoiceInstance);

		assertEquals(1, getLatestInvoiceNumberForYear(null, year));
	}

	@Test
	public void testGetInvoicesUserNull() throws Exception
	{
		System.out.println("testGetInvoicesUserNull");
		getInvoices(null, contractorInstance);
	}

	@Test
	public void testGetInvoicesContractorNull() throws Exception
	{
		System.out.println("testGetInvoicesContractorNull");
		getInvoices(userInstance, null);
	}

	@Test
	public void testGetInvoicesContractorPopulated() throws Exception
	{
		System.out.println("testGetInvoicesContractorPopulated");
		insert(invoiceInstance);

		getInvoices(userInstance, contractorInstance);

		assertEquals(1, contractorInstance.getAllItems().size());
	}

	@Test
	public void testGetInvoicesNonExistingTable() throws Exception
	{
		System.out.println("testGetInvoicesNonExistingTable");
		getInvoices(userInstance, contractorInstance);
	}

	@Test
	public void testGetInvoicesEmptyTable() throws Exception
	{
		System.out.println("testGetInvoicesEmptyTable");
		insert(invoiceInstance);
		remove(invoiceInstance);

		getInvoices(userInstance, contractorInstance);
	}

	@Test
	public void test1ReturendIfNoInvoicesInTheGivenYear() throws Exception
	{
		System.out.println("test1ReturendIfNoInvoicesInTheGivenYear");
		insert(invoiceInstance);

		assertEquals(1, getLatestInvoiceNumberForYear(userInstance, 5));
	}
}