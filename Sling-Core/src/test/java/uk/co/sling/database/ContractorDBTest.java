package uk.co.sling.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;
import uk.co.sling.containers.Contractor;
import uk.co.sling.containers.User;
import uk.co.sling.exceptions.SQLFetchException;
import uk.co.sling.exceptions.ValueNotSetException;

import java.io.File;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static uk.co.sling.database.ContractorDB.insert;
import static uk.co.sling.database.ContractorDB.remove;
import static uk.co.sling.database.ContractorDB.update;
import static uk.co.sling.database.ContractorDB.getContractors;
import static uk.co.sling.database.ContractorDB.CREATE_TABLE;
import static uk.co.sling.database.UserDB.insert;

public class ContractorDBTest
{
	private static File db = new File("data/data.db");
	private Contractor contractorInstance;
	private User userInstance;

	@Before
	public void setUp() throws Exception
	{
		userInstance = new User.UserBuilder()
				.setUserName("Username")
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildUser();

		insert(userInstance);

		contractorInstance = new Contractor.ContractorBuilder()
				.setCompanyName("CompanyName")
				.setVatCode("1321-312-123")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildContractor();
	}

	@After
	public void tearDown() throws Exception
	{
		//manager = null;
		userInstance = null;
		contractorInstance = null;
		db.delete();
	}

	@Test
	public void testInsertToDB() throws Exception
	{
		System.out.println("testInsertToDB");
		insert(userInstance, contractorInstance);

		assertTrue("DB doesn't have the contractor", UserDB.getUsers().contains(userInstance));
	}

	@Test (expected = SQLException.class)
	public void testInsertSameItemTwice() throws Exception
	{
		System.out.println("testInsertSameItemTwice");
		insert(userInstance, contractorInstance);
		insert(userInstance, contractorInstance);
	}

	@Test
	public void testRemoveFromDB() throws Exception
	{
		System.out.println("testRemoveFromDB");
		insert(userInstance, contractorInstance);
		remove(userInstance, contractorInstance);
	}

	@Test
	public void testRemoveNonExistingFromDB() throws Exception
	{
		System.out.println("testRemoveNonExistingFromDB");
		insert(userInstance, contractorInstance);
		remove(userInstance, contractorInstance);
		remove(userInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals(0, userInstance.getAllItems().size());
	}

	@Test
	public void testRemoveWhileTableNotCreated() throws Exception
	{
		System.out.println("testRemoveWhileTableNotCreated");
		remove(userInstance, contractorInstance);
	}

	@Test
	public void testInsertNull() throws Exception
	{
		System.out.println("testInsertNull");
		insert(null, null);
	}

	@Test
	public void testInsertUserNull() throws Exception
	{
		System.out.println("testInsertUserNull");
		insert(null, contractorInstance);
	}

	@Test
	public void testInsertContractorNull() throws Exception
	{
		System.out.println("testInsertContractorNull");
		insert(userInstance, null);
	}

	@Test
	public void testRemoveNull() throws Exception
	{
		System.out.println("testRemoveNull");
		remove(null, null);
	}

	@Test
	public void testRemoveUserNull() throws Exception
	{
		System.out.println("testRemoveUserNull");
		remove(null, contractorInstance);
	}

	@Test
	public void testRemoveContractorNull() throws Exception
	{
		System.out.println("testRemoveContractorNull");
		remove(userInstance, null);
	}

	@Test
	public void testUpdateNull() throws Exception
	{
		System.out.println("testUpdateNull");
		update(null, null, null);
	}

	@Test
	public void testUpdateUserNullContractorsNotNull() throws Exception
	{
		System.out.println("testUpdateUserNullContractorsNotNull");
		update(null, contractorInstance, contractorInstance);
	}

	@Test
	public void testUpdateUserNotNullContractorsNull() throws Exception
	{
		System.out.println("testUpdateUserNotNullContractorsNull");
		insert(userInstance, contractorInstance);
		update(userInstance, null, null);
	}

	@Test
	public void testUpdateUserNotNullNewContractorNotNullContractorNull() throws Exception
	{
		System.out.println("testUpdateUserNotNullNewContractorNotNullContractorNull");
		insert(userInstance, contractorInstance);
		update(userInstance, null, contractorInstance);
	}

	@Test
	public void testUpdateUserNotNullContractorNotNullNewContractorNull() throws Exception
	{
		System.out.println("testUpdateUserNotNullContractorNotNullNewContractorNull");
		insert(userInstance, contractorInstance);
		update(userInstance, contractorInstance, null);
		getContractors(userInstance);

		assertTrue("Doesn't contain user", userInstance.getAllItems().contains(contractorInstance));
	}

	@Test
	public void testUpdateContractorDidntChangeCompanyName() throws Exception
	{
		System.out.println("testUpdateContractorDidntChangeCompanyName");
		insert(userInstance, contractorInstance);
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Company name has changed", contractorInstance.getCompanyName(), userInstance.getAllItems().iterator().next().getCompanyName());
	}

	@Test
	public void testUpdateContractorDidntChangeVatCode() throws Exception
	{
		System.out.println("testUpdateContractorDidntChangeVatCode");
		insert(userInstance, contractorInstance);
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Vat code has changed", contractorInstance.getVatCode(), userInstance.getAllItems().iterator().next().getVatCode());
	}

	@Test
	public void testUpdateContractorDidntChangeTown() throws Exception
	{
		System.out.println("testUpdateContractorDidntChangeTown");
		insert(userInstance, contractorInstance);
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Town has changed", contractorInstance.getTown(),  userInstance.getAllItems().iterator().next().getTown());
	}

	@Test
	public void testUpdateContractorDidntChangePostCode() throws Exception
	{
		System.out.println("testUpdateContractorDidntChangePostCode");
		insert(userInstance, contractorInstance);
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Post code has changed", contractorInstance.getPostCode(),  userInstance.getAllItems().iterator().next().getPostCode());
	}

	@Test
	public void testUpdateContractorDidntChangeStreetName() throws Exception
	{
		System.out.println("testUpdateContractorDidntChangeStreetName");
		insert(userInstance, contractorInstance);
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Street name has changed", contractorInstance.getStreet(),  userInstance.getAllItems().iterator().next().getStreet());
	}

	@Test
	public void testUpdateContractorDidntChangeBuildingNumber() throws Exception
	{
		System.out.println("testUpdateContractorDidntChangeBuildingNumber");
		insert(userInstance, contractorInstance);
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Building number has changed", contractorInstance.getBuildingNumber(),  userInstance.getAllItems().iterator().next().getBuildingNumber());
	}

	@Test
	public void testUpdateContractorDidntChangePhone() throws Exception
	{
		System.out.println("testUpdateContractorDidntChangePhone");
		insert(userInstance, contractorInstance);
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Phone has changed", contractorInstance.getPhone(),  userInstance.getAllItems().iterator().next().getPhone());
	}

	@Test
	public void testUpdateContractorDidntChangeEmail() throws Exception
	{
		System.out.println("testUpdateContractorDidntChangeEmail");
		insert(userInstance, contractorInstance);
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Email has changed", contractorInstance.getEmail(),  userInstance.getAllItems().iterator().next().getEmail());
	}

	@Test
	public void testUpdateContractorChangedCompanyName() throws Exception
	{
		System.out.println("testUpdateContractorChangedCompanyName");
		insert(userInstance, contractorInstance);
		contractorInstance.setCompanyName("My Super Company");
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Company name hasn't changed", contractorInstance.getCompanyName(),  userInstance.getAllItems().iterator().next().getCompanyName());
	}

	@Test
	public void testUpdateContractorChangedVatCode() throws Exception
	{
		System.out.println("testUpdateContractorChangedVatCode");
		insert(userInstance, contractorInstance);
		Contractor newContractor = new Contractor.ContractorBuilder()
				.setCompanyName("CompanyName")
				.setVatCode("53432")
				.setTown("Town")
				.setPostCode("postCode")
				.setStreet("Street")
				.setBuildingNumber("12a")
				.setPhoneNumber("123123123")
				.setEmail("email@email.com")
				.buildContractor();
		update(userInstance, contractorInstance, newContractor);
		getContractors(userInstance);

		assertEquals("Vat code hasn't changed", newContractor.getVatCode(),  userInstance.getAllItems().iterator().next().getVatCode());
	}

	@Test
	public void testUpdateContractorChangedTown() throws Exception
	{
		System.out.println("testUpdateContractorChangedTown");
		insert(userInstance, contractorInstance);
		contractorInstance.setTown("New City");
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Town hasn't changed", contractorInstance.getTown(),  userInstance.getAllItems().iterator().next().getTown());
	}

	@Test
	public void testUpdateContractorChangedPostCode() throws Exception
	{
		System.out.println("testUpdateContractorChangedPostCode");
		insert(userInstance, contractorInstance);
		contractorInstance.setPhone("1337");
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Post code hasn't changed", contractorInstance.getPostCode(),  userInstance.getAllItems().iterator().next().getPostCode());
	}

	@Test
	public void testUpdateContractorChangedStreetName() throws Exception
	{
		System.out.println("testUpdateContractorChangedStreetName");
		insert(userInstance, contractorInstance);
		contractorInstance.setStreet("This Street");
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Street name hasn't changed", contractorInstance.getStreet(),  userInstance.getAllItems().iterator().next().getStreet());
	}

	@Test
	public void testUpdateContractorChangedBuildingNumber() throws Exception
	{
		System.out.println("testUpdateContractorChangedBuildingNumber");
		insert(userInstance, contractorInstance);
		contractorInstance.setBuildingNumber("13b");
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Building number hasn't changed", contractorInstance.getBuildingNumber(),  userInstance.getAllItems().iterator().next().getBuildingNumber());
	}

	@Test
	public void testUpdateContractorChangedPhone() throws Exception
	{
		System.out.println("testUpdateContractorChangedPhone");
		insert(userInstance, contractorInstance);
		contractorInstance.setPhone("+48 123123123");
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Phone hasn't changed", contractorInstance.getPhone(),  userInstance.getAllItems().iterator().next().getPhone());
	}

	@Test
	public void testUpdateContractorChangedEmail() throws Exception
	{
		System.out.println("testUpdateContractorChangedEmail");
		insert(userInstance, contractorInstance);
		contractorInstance.setEmail("myemail@email.com");
		update(userInstance, contractorInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Email hasn't changed", contractorInstance.getEmail(),  userInstance.getAllItems().iterator().next().getEmail());
	}

	@Test
	public void testGetAllUsersEmptyTable() throws Exception
	{
		System.out.println("testGetAllUsersEmptyTable");
		ConnectionManager.getConnectionInstance().getStatement(CREATE_TABLE).execute();
		ConnectionManager.getConnectionInstance().close();
		getContractors(userInstance);

		assertEquals(0, userInstance.getAllItems().size());
	}

	@Test
	public void testGetAllContractorsWhileTableDoesntExist() throws Exception
	{
		System.out.println("testGetAllContractorsWhileTableDoesntExist");
		getContractors(userInstance);
	}

	@Test
	public void testGetAllContractors() throws Exception
	{
		System.out.println("testGetAllContractors");
		insert(userInstance, contractorInstance);
		getContractors(userInstance);

		assertTrue("Returned list size is not 1", userInstance.getAllItems().size() == 1);
	}

	@Test
	public void testInsertLeftSlash() throws Exception
	{
		System.out.println("testInsertLeftSlash");
		String companyName = "\\";

		contractorInstance.setCompanyName(companyName);
		insert(userInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Doesn't contain slash", companyName,  userInstance.getAllItems().iterator().next().getCompanyName());
	}

	@Test
	public void testInsertRightSlash() throws Exception
	{
		System.out.println("testInsertRightSlash");
		String companyName = "////";

		contractorInstance.setCompanyName(companyName);
		insert(userInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Doesn't contain slash", companyName,  userInstance.getAllItems().iterator().next().getCompanyName());
	}

	@Test
	public void testInsertPeriod() throws Exception
	{
		System.out.println("testInsertPeriod");
		String companyName = ".";

		contractorInstance.setCompanyName(companyName);
		insert(userInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Doesn't contain period", companyName,  userInstance.getAllItems().iterator().next().getCompanyName());
	}

	@Test
	public void testInsertSemicolon() throws Exception
	{
		System.out.println("testInsertPeriod");
		String companyName = ";";

		contractorInstance.setCompanyName(companyName);
		insert(userInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Doesn't contain semicolon", companyName,  userInstance.getAllItems().iterator().next().getCompanyName());
	}

	@Test
	public void testInsertTylda() throws Exception
	{
		System.out.println("testInsertTylda");
		String companyName = "~";

		contractorInstance.setCompanyName(companyName);
		insert(userInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Doesn't contain ~", companyName,  userInstance.getAllItems().iterator().next().getCompanyName());
	}

	@Test
	public void testInsertWeirdCharacter() throws Exception
	{
		System.out.println("testInsertWeirdCharacter");
		String companyName = "¬";

		contractorInstance.setCompanyName(companyName);
		insert(userInstance, contractorInstance);
		getContractors(userInstance);

		assertEquals("Doesn't contain ¬", companyName,  userInstance.getAllItems().iterator().next().getCompanyName());
	}

	@Test
	public void testRemoveUserCheckIfContractorRemoved() throws Exception
	{
		System.out.println("testRemoveUserCheckIfContractorRemoved");
		insert(userInstance, contractorInstance);
		UserDB.remove(userInstance);

		getContractors(userInstance);

		assertEquals(0, userInstance.getAllItems().size());
	}
}