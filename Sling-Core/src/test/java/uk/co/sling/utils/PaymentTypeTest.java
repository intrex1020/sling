package uk.co.sling.utils;

import static org.junit.Assert.*;
import org.junit.Test;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.locale.Localisation;

/**
 *
 * @author Sebastian Wojtkowiak
 */
public class PaymentTypeTest
{
    /**
     * Test of toString method, of class PaymentType.
     */
    @Test
    public void bankTransfer()
    {
        System.out.println("bankTransfer");
        PaymentType instance = PaymentType.BANK_TRANSFER;
        String expResult = Localisation.getValuePL("bankTransfer");
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void cashPayment()
    {
        System.out.println("cashPayment");
        PaymentType instance = PaymentType.CASH;
        String expResult = Localisation.getValuePL("cash");
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void paid()
    {
        System.out.println("paid");
        PaymentType instance = PaymentType.PAID;
        String expResult = Localisation.getValuePL("paid");
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void fromBankTransferString()
    {
        System.out.println("fromBankTransferString");

        assertEquals("Didn't return bank transfer enum", PaymentType.BANK_TRANSFER,
                PaymentType.fromString(Localisation.getValuePL("bankTransfer")));
    }

    @Test
    public void fromCashString()
    {
        System.out.println("fromCashString");

        assertEquals("Didn't return cash enum", PaymentType.CASH, PaymentType.fromString(Localisation.getValuePL("cash")));
    }

    @Test
    public void fromPaidString()
    {
        System.out.println("fromPaidString");

        assertEquals("Didn't return paid enum", PaymentType.PAID,
                PaymentType.fromString(Localisation.getValuePL("paid")));
    }

    @Test (expected = InvalidValueException.class)
    public void fromInvalidString()
    {
        System.out.println("fromInvalidString");

        PaymentType.fromString("asdasd");
    }

    @Test
    public void fromInvalidStringErrorMessage()
    {
        System.out.println("");

        try
        {
            PaymentType.fromString("asdasd");
        }
        catch (InvalidValueException ive)
        {
            assertEquals("Wrong error message", Localisation.getValuePL("invalidPaymentType"), ive.getMessage());
            return;
        }

        fail("Wrong exception");
    }
}
