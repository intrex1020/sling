package uk.co.sling.utils;

import org.junit.Test;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.locale.Localisation;

import static org.junit.Assert.*;

/**
 * @author sebastian wojtkowiak
 */
public class CalculatorTest
{
	@Test
	public void calculateVATAmountWholeNumber()
	{
		System.out.println("calculateVATAmountWholeNumber");
		assertEquals(5, Calculator.calculateVatAmount(5, 10), 0);
	}

	@Test
	public void calculateVATAmountWithDec()
	{
		System.out.println("calculateVATAmountWithDec");
		assertEquals(5, Calculator.calculateVatAmount(5.5, 10.5), 0);
	}

	@Test (expected = InvalidValueException.class)
	public void calculateVATAmountWithoutVATGreater()
	{
		System.out.println("calculateVATAmountWithoutVATGreater");
		Calculator.calculateVatAmount(10, 5);
	}

	@Test (expected = InvalidValueException.class)
	public void calculateVATAmountWithoutVATLessThan0()
	{
		System.out.println("calculateVATAmountWithoutVATLessThan0");
		Calculator.calculateVatAmount(-5, 5);
	}

	@Test (expected = InvalidValueException.class)
	public void calculateVATAmountWithVATLessThan0()
	{
		System.out.println("calculateVATAmountWithVATLessThan0");
		Calculator.calculateVatAmount(5,-5);
	}

	@Test
	public void calculateVATAmountWithoutVATGreaterExMessage()
	{
		System.out.println("calculateVATAmountWithoutVATGreaterExMessage");
		try
		{
			Calculator.calculateVatAmount(10, 5);
		}
		catch (InvalidValueException ive)
		{
			assertEquals(Localisation.getValuePL("invalidWithoutVATGreater"), ive.getMessage());
			return;
		}

		fail("No exception was thrown that day");
	}

	@Test
	public void calculateVATAmountWithoutVATLessThan0ExMessage()
	{
		System.out.println("calculateVATAmountWithoutVATLessThan0ExMessage");
		try
		{
			Calculator.calculateVatAmount(-5, 0);
		}
		catch (InvalidValueException ive)
		{
			assertEquals(Localisation.getValuePL("invalidNegValue"), ive.getMessage());
			return;
		}

		fail("No exception was thrown that day");
	}

	@Test
	public void calculateVATAmountWithVATLessThan0ExMessage()
	{
		System.out.println("calculateVATAmountWithVATLessThan0ExMessage");
		try
		{
			Calculator.calculateVatAmount(5, -5);
		}
		catch (InvalidValueException ive)
		{
			assertEquals(Localisation.getValuePL("invalidNegValue"), ive.getMessage());
			return;
		}

		fail("No exception was thrown that day");
	}

	@Test
	public void calculateValueWithoutVAT1Item()
	{
		System.out.println("calculateValueWithoutVAT1Item");
		assertEquals(20 ,Calculator.calculateTotalPrice(1, 20), 0);
	}

	@Test
	public void calculateValueWithoutVAT5Items()
	{
		System.out.println("calculateValueWithoutVAT5Items");
		assertEquals(100, Calculator.calculateTotalPrice(5, 20), 0);
	}

	@Test
	public void calculateValueWithoutVAT5ItemsDec()
	{
		System.out.println("calculateValueWithoutVAT5ItemsDec");
		assertEquals(90, Calculator.calculateTotalPrice(4.5, 20), 0);
	}

	@Test
	public void calculateValueWithoutVAT5Items2Dec()
	{
		System.out.println("calculateValueWithoutVAT5Items2Dec");
		assertEquals(86.26, Calculator.calculateTotalPrice(4.54, 19), 0);
	}

	@Test (expected = InvalidValueException.class)
	public void calculateValueWithoutVATItemLessThan0()
	{
		System.out.println("calculateValueWithoutVATItemLessThan0");
		Calculator.calculateTotalPrice(-5, 1);
	}

	@Test (expected = InvalidValueException.class)
	public void calculateValueWithoutVATPriceLessThan0()
	{
		System.out.println("calculateValueWithoutVATPriceLessThan0");
		Calculator.calculateTotalPrice(5, -1);
	}

	@Test
	public void calculateValueWithoutVATItemLessThan0ExMessage()
	{
		System.out.println("calculateValueWithoutVATItemLessThan0ExMessage");
		try
		{
			Calculator.calculateTotalPrice(-5, 1);
		}
		catch (InvalidValueException ive)
		{
			assertEquals(Localisation.getValuePL("invalidNegValue"), ive.getMessage());
			return;
		}

		fail("Exception was not thrown that day");
	}

	@Test
	public void calculateValueWithoutVATPriceLessThan0ExMessage()
	{
		System.out.println("calculateValueWithoutVATPriceLessThan0ExMessage");
		try
		{
			Calculator.calculateTotalPrice(1, -1);
		}
		catch (InvalidValueException ive)
		{
			assertEquals(Localisation.getValuePL("invalidNegValue"), ive.getMessage());
			return;
		}

		fail("Exception was not thrown that day");
	}

	@Test
	public void calculateValueWithVAT1Item()
	{
		System.out.println("calculateValueWithVAT1Item");
		double valueWithoutVAT = Calculator.calculateTotalPrice(1, 20);
		assertEquals(24.6, Calculator.calculateValueWithVat(23, valueWithoutVAT), 0);
	}

	@Test
	public void calculateValueWithVAT5Items()
	{
		System.out.println("calculateValueWithVAT5Items");
		double valueWithoutVAT = Calculator.calculateTotalPrice(5, 20);
		assertEquals(123, Calculator.calculateValueWithVat(23, valueWithoutVAT), 0);
	}

	@Test
	public void calculateValueWithVAT5ItemsDec()
	{
		System.out.println("calculateValueWithVAT5ItemsDec");
		double valueWithVAT = Calculator.calculateTotalPrice(4.4, 20);
		assertEquals(108.24, Calculator.calculateValueWithVat(23, valueWithVAT) , 0);
	}

	@Test
	public void calculateValueWithVAT5Items2Dec()
	{
		System.out.println("calculateValueWithVAT5Items2Dec");
		double valueWithoutVat = Calculator.calculateTotalPrice(4.54, 19);
		assertEquals(106.10, Calculator.calculateValueWithVat(23, valueWithoutVat), 0);
	}

	@Test (expected = InvalidValueException.class)
	public void calculateValueWithVATLessThan0()
	{
		System.out.println("calculateValueWithVATLessThan0");
		Calculator.calculateValueWithVat(5, -5);
	}

	@Test
	public void calculateValueWithVATLessThan0ExMessage()
	{
		System.out.println("calculateValueWithVATLessThan0ExMessage");
		try
		{
			Calculator.calculateValueWithVat(5, -5);
		}
		catch (InvalidValueException ive)
		{
			assertEquals(Localisation.getValuePL("invalidNegValue"), ive.getMessage());
			return;
		}

		fail("Exception was not thrown that day");
	}

	@Test
	public void roundDown()
	{
		System.out.println("roundDown");
		assertEquals(2.54, Calculator.roundToMaximum2Dec(2.544), 0);
	}

	@Test
	public void roundUp()
	{
		System.out.println("roundUp");
		assertEquals(2.55, Calculator.roundToMaximum2Dec(2.545), 0);
	}

	@Test
	public void noRound()
	{
		System.out.println("noRound");
		assertEquals(2.54, Calculator.roundToMaximum2Dec(2.54), 0);
	}

	@Test
	public void testConstructor()
	{
		new Calculator();
	}
}