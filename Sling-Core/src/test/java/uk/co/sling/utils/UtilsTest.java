package uk.co.sling.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class UtilsTest
{
	@Test
	public void testValidNames()
	{
		System.out.println("testValidNames");

		assertFalse(Utils.isFieldNotValid("aa"));
		assertFalse(Utils.isFieldNotValid("a a"));
		assertFalse(Utils.isFieldNotValid("12 aa"));
		assertFalse(Utils.isFieldNotValid("aa 12"));
		assertFalse(Utils.isFieldNotValid("\\ aa //"));
		assertFalse(Utils.isFieldNotValid("// aa //"));
		assertFalse(Utils.isFieldNotValid(". aa ."));
		assertFalse(Utils.isFieldNotValid("\" aa \""));
		assertFalse(Utils.isFieldNotValid("@ aa @"));
	}

	@Test
	public void testSpaceFirst()
	{
		System.out.println("testSpaceFirst");

		assertTrue(Utils.isFieldNotValid(" a"));
	}

	@Test
	public void testTabFirst()
	{
		System.out.println("testTabFirst");

		assertTrue(Utils.isFieldNotValid("	a"));
	}

	@Test
	public void testSpaceLast()
	{
		System.out.println("testSpaceLast");

		assertTrue(Utils.isFieldNotValid("a "));
	}

	@Test
	public void testTabLast()
	{
		System.out.println("testSpaceLast");

		assertTrue(Utils.isFieldNotValid("a	"));
	}

	@Test
	public void testOnlySpace()
	{
		System.out.println("testOnlySpaces");

		assertTrue(Utils.isFieldNotValid(" "));
	}

	@Test
	public void testOnlyTab()
	{
		System.out.println("testOnlyTab");

		assertTrue(Utils.isFieldNotValid("	"));
	}

	@Test
	public void testMultipleSpace()
	{
		System.out.println("testOnlySpaces");

		assertTrue(Utils.isFieldNotValid("  "));
	}

	@Test
	public void testMultipleTab()
	{
		System.out.println("testOnlyTab");

		assertTrue(Utils.isFieldNotValid("		"));
	}

	@Test
	public void testConstructor()
	{
		new Utils();
	}
}