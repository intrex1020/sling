package uk.co.sling.local_date;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>Wrapps {@link LocalDateTime} class
 * and provides date using the European
 * format</p>
 *
 *
 * @author Sebastian Wojtkowiak
 */
public class LocalDateTimeEUFormat
{
    private final LocalDateTime date;

    private LocalDateTimeEUFormat(int day, Month month, int year)
    {
        date = LocalDateTime.of(year, month, day, 0, 0);
    }

    private LocalDateTimeEUFormat(int day, int month, int year)
    {
        date = LocalDateTime.of(year, month, day, 0, 0);
    }

    private LocalDateTimeEUFormat(int hour, int minute, int day, Month month, int year)
    {
        date = LocalDateTime.of(year, month, day, hour, minute);
    }

    private LocalDateTimeEUFormat(int hour, int minute, int seconds, int day, Month month, int year)
    {
        date = LocalDateTime.of(year, month, day, hour, minute, seconds);
    }

    private LocalDateTimeEUFormat(int hour, int minute, int day, int month, int year)
    {
        date = LocalDateTime.of(year, month, day, hour, minute);
    }

    private LocalDateTimeEUFormat(int hour, int minute, int seconds, int day, int month, int year)
    {
        date = LocalDateTime.of(year, month, day, hour, minute, seconds);
    }

    private LocalDateTimeEUFormat(LocalDateTime date)
    {
        this.date = date;
    }

    public static LocalDateTimeEUFormat now()
    {
        return new LocalDateTimeEUFormat(LocalDateTime.now());
    }

    public static LocalDateTimeEUFormat of(int day, int month, int year)
    {
        return new LocalDateTimeEUFormat(day, month, year);
    }

    public static LocalDateTimeEUFormat of(int day, Month month, int year)
    {
        return new LocalDateTimeEUFormat(day, month, year);
    }

    public static LocalDateTimeEUFormat of(int hour, int minute, int day, Month month, int year)
    {
        return new LocalDateTimeEUFormat(hour, minute, day, month, year);
    }

    public static LocalDateTimeEUFormat of(int hour, int minute, int seconds, int day, Month month, int year)
    {
        return new LocalDateTimeEUFormat(hour, minute, seconds, day, month, year);
    }

    public static LocalDateTimeEUFormat of(int hour, int minute, int day, int month, int year)
    {
        return new LocalDateTimeEUFormat(hour, minute, day, month, year);
    }

    public static LocalDateTimeEUFormat of(int hour, int minute, int seconds, int day, int month, int year)
    {
        return new LocalDateTimeEUFormat(hour, minute, seconds, day, month, year);
    }

    public static LocalDateTimeEUFormat from(LocalDate date)
    {
        return new LocalDateTimeEUFormat(date.getDayOfMonth(), date.getMonthValue(), date.getYear());
    }

    public int getSecond()
    {
        return date.getSecond();
    }

    public int getMinute()
    {
        return date.getMinute();
    }

    public int getHour()
    {
        return date.getHour();
    }

    public int getDayOfMonth()
    {
        return date.getDayOfMonth();
    }

    public DayOfWeek getDayOfWeak()
    {
        return date.getDayOfWeek();
    }

    public int getDayOfYear()
    {
        return date.getDayOfYear();
    }

    public Month getMonth()
    {
        return date.getMonth();
    }

    public int getYear()
    {
        return date.getYear();
    }

    /**
     * Compares the given date against the current
     *
     * @param date to compare against
     * @return true if given date is after
     */
    public boolean isAfter(LocalDateTimeEUFormat date)
    {
        return this.date.isAfter(date.getAsLocalDateTime());
    }

    /**
     * Compares the given date against the current
     *
     * @param date to compare against
     * @return true if given date is before
     */
    public boolean isBefore(LocalDateTimeEUFormat date)
    {
        return this.date.isBefore(date.getAsLocalDateTime());
    }

    /**
     * Compares the given date against the current
     *
     * @param date to compare against
     * @return true if date is equals
     */
    public boolean isEqual(LocalDateTimeEUFormat date)
    {
        return this.date.isEqual(date.getAsLocalDateTime());
    }

    public LocalDateTimeEUFormat minusSeconds(long seconds)
    {
        return new LocalDateTimeEUFormat(date.minusSeconds(seconds));
    }

    public LocalDateTimeEUFormat minusMinutes(long minutes)
    {
        return new LocalDateTimeEUFormat(date.minusMinutes(minutes));
    }

    public LocalDateTimeEUFormat minusHours(long hours)
    {
        return new LocalDateTimeEUFormat(date.minusHours(hours));
    }

    public LocalDateTimeEUFormat minusDays(long days)
    {
        return new LocalDateTimeEUFormat(date.minusDays(days));
    }

    public LocalDateTimeEUFormat minusMonths(long months)
    {
        return new LocalDateTimeEUFormat(date.minusMonths(months));
    }

    public LocalDateTimeEUFormat minusWeeks(long weeks)
    {
        return new LocalDateTimeEUFormat(date.minusWeeks(weeks));
    }

    public LocalDateTimeEUFormat minusYears(long years)
    {
        return new LocalDateTimeEUFormat(date.minusYears(years));
    }

    public LocalDateTimeEUFormat plusSeconds(long seconds)
    {
        return new LocalDateTimeEUFormat(date.plusSeconds(seconds));
    }

    public LocalDateTimeEUFormat plusMinutes(long minutes)
    {
        return new LocalDateTimeEUFormat(date.plusMinutes(minutes));
    }

    public LocalDateTimeEUFormat plusHours(long hours)
    {
        return new LocalDateTimeEUFormat(date.plusHours(hours));
    }

    public LocalDateTimeEUFormat plusDays(long days)
    {
        return new LocalDateTimeEUFormat(date.plusDays(days));
    }

    public LocalDateTimeEUFormat plusMonths(long months)
    {
        return new LocalDateTimeEUFormat(date.plusMonths(months));
    }

    public LocalDateTimeEUFormat plusWeeks(long weeks)
    {
        return new LocalDateTimeEUFormat(date.plusWeeks(weeks));
    }

    public LocalDateTimeEUFormat plusYears(long years)
    {
        return new LocalDateTimeEUFormat(date.plusYears(years));
    }

    /**
     * Compares current date against the provided date
     *
     * @param toCompare to compare against
     * @return the comparator value, negative if less, positive if greater
     */
    public int compareTo(LocalDateTime toCompare)
    {
        return date.compareTo(toCompare);
    }

    /**
     * Compares current date against the provided object
     *
     * @param o to compare against
     * @return true if equal
     */
    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof LocalDateTimeEUFormat))
            return false;

        LocalDateTimeEUFormat ldteuf = (LocalDateTimeEUFormat)o;

        return date.equals(ldteuf.date);
    }

    @Override
    public int hashCode()
    {
        return date.hashCode();
    }

    public LocalDateTime getAsLocalDateTime()
    {
        return date;
    }

    public String toStringWithTime()
    {
        //Append 0 in front of if needed
        return ((getHour() < 10 ? "0" + getHour() : getHour())
                + "-"
                + (getMinute() < 10 ? "0" + getMinute() : getMinute())
                + "-"
                + (getSecond() < 10 ? "0" + getSecond() : getSecond())
                + "-"
                + (getDayOfMonth() < 10 ? "0" + getDayOfMonth() : getDayOfMonth())
                + "-"
                + (date.getMonthValue() < 10 ? "0" + date.getMonthValue() : date.getMonthValue())
                + "-"
                + getYear());
    }

    public String toStringUSFormat()
    {
        return getYear()
                + "-"
                + (date.getMonthValue() < 10 ? "0" + date.getMonthValue() : date.getMonthValue())
                + "-"
                + (getDayOfMonth() < 10 ? "0" + getDayOfMonth() : getDayOfMonth());
    }

    @Override
    public String toString()
    {
        //Append 0 in front of if needed
        return (getDayOfMonth() < 10 ? "0" + getDayOfMonth() : getDayOfMonth())
                + "-"
                + (date.getMonthValue() < 10 ? "0" + date.getMonthValue() : date.getMonthValue())
                + "-"
                + getYear();
    }
}
