package uk.co.sling.exceptions;

public class SQLFetchException extends Exception
{
	public SQLFetchException(String message)
	{
		super(message);
	}
}
