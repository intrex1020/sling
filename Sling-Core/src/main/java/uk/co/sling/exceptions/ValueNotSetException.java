package uk.co.sling.exceptions;

/**
 *
 * @author sebastian wojtkowiak
 */
public final class ValueNotSetException extends RuntimeException
{
    public ValueNotSetException(String message)
    {
        super(message);
    }
}
