package uk.co.sling.exceptions;

/**
 *
 * @author sebastian wojtkowiak
 */
public final class InvalidValueException extends RuntimeException
{
    public InvalidValueException(String message)
    {
        super(message);
    }
}
