package uk.co.sling.exceptions;

/**
 * @author sebastian wojtkowiak
 */
public final class NotConnectedException extends Exception
{
	public NotConnectedException (String message)
	{
		super(message);
	}
}
