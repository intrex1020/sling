package uk.co.sling.containers;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;
import uk.co.sling.utils.Utils;

import javax.imageio.ImageIO;

/**
 * <p>Software settings<br>
 *
 *  Contains user settings that are used on hte
 *  {@link Invoice}</p>
 *
 * @author sebastian wojtkowiak
 */
public final class Setting implements IContainer
{
    private String accountNumber;
    private String bankName;
    private Image waterMark;
    private final static int MB_65 = 68157440 - 1;

    private Setting(String accountNumber, String bankName, Image waterMark)
    {
        setAccountNumber(accountNumber);
        setBankName(bankName);
        setWaterMark(waterMark);
    }

    /**
     * <p>Gets current {@link Setting#accountNumber}</p>
     *
     * @return account number
     */
    public String getAccountNumber()
    {
        return accountNumber;
    }

    /**
     * <p>Sets {@link Setting#accountNumber}<br>
     *
     * The passed value can contain any characters, however
     * if it starts or ends with a white character it will
     * be handled as an error case.<br>
     * All characters are allowed to support international account
     * numbers that have to be written as IBAN or any other format.<br>
     *
     *  If there is an attempt of setting the
     * {@link Setting#accountNumber} as null or an empty string the {@link InvalidValueException} will be
     * thrown to indicate that an attempt to set {@link Setting#accountNumber}
     * as null has been made.<br>
     *
     * If there is an attempt to save the {@link Setting#accountNumber} as white characters
     * or the last character is a white character the {@link InvalidValueException}
     * will be thrown to indicate invalid attempt.</p>
     *
     * @param accountNumber to set
     * @throws InvalidValueException if incorrect format or null
     */
    public void setAccountNumber(String accountNumber)
    {
        if (accountNumber == null || accountNumber.isEmpty() || Utils.isFieldNotValid(accountNumber))
            throw new InvalidValueException(Localisation.getValuePL("invalidAccountNumber"));
        else
            this.accountNumber = accountNumber;
    }

    /**
     * <p>Gets current {@link Setting#bankName}</p>
     *
     * @return bank name
     */
    public String getBankName()
    {
        return bankName;
    }

    /**
     * <p>Sets the {@link Setting#bankName}.<br>
     *
     * The passed value can contain any characters, however
     * if it starts or ends with a white character it will
     * be handled as an error case.<br>
     *
     * If there is an attempt of setting the
     * {@link Setting#bankName} as null or an empty string the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link Setting#bankName}
     * as null has been made.<br>
     *
     * If there is an attempt to save the {@link Setting#bankName} as white characters
     * or the last character is a white character the {@link InvalidValueException}
     * will be thrown to indicate invalid attempt.</p>
     *
     * @param bankName to set
     * @throws InvalidValueException if empty or null
     */
    public void setBankName(String bankName)
    {
        if (bankName == null || bankName.isEmpty())
            throw new ValueNotSetException(Localisation.getValuePL("notSetBankName"));
        else if (Utils.isFieldNotValid(bankName))
            throw new InvalidValueException(Localisation.getValuePL("invalidBankName"));
        else
            this.bankName = bankName;
    }

    /**
     * <p>Gets current {@link Setting#waterMark}<br>
     *
     * The value of {@link Setting#waterMark} is nullable
     * so the returned value may be {@code null}</p>
     *
     * @return Image or null if there is no watermark
     */
    public Image getWaterMark()
    {
        return waterMark;
    }

    /**
     * <p>Sets {@link Setting#waterMark}<br>
     *
     * The value passed is nullable and the {@link Setting#waterMark}
     * can be set as null to indicate that the {@link Setting#waterMark}
     * has not been set.<br>
     *
     * The {@link Setting#waterMark} can not exceed the maximum size of
     * {@value #MB_65}, if it is the case then {@link InvalidValueException} will
     * be thrown.<br>
     *
     * If there is an IOException or file could not be read an {@link IOException} will
     * be thrown</p>
     *
     * @param waterMark to set
     * @throws InvalidValueException if the image exceeds max size {@value #MB_65}
     * @throws IOException if couldn't read the image
     */
    public void setWaterMark(Image waterMark)
    {
        if (waterMark != null)
        {
            BufferedImage bi = new BufferedImage(waterMark.getWidth(null), waterMark.getHeight(null), BufferedImage.TYPE_INT_ARGB);

            Graphics2D g2 = bi.createGraphics();
            g2.drawImage(waterMark, 0, 0, null);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try
            {
                ImageIO.write(bi, "png", baos);

                if (baos.size() > MB_65)
                    throw new InvalidValueException(Localisation.getValuePL("imageTooBig"));

            }
            catch (IOException ioe)
            {
                throw new InvalidValueException(Localisation.getValuePL("imageReadError"));
            }
            finally
            {
                g2.dispose();
            }
        }

        this.waterMark = waterMark;
    }

    @Override
    public Setting copy()
    {
        return new Setting(getAccountNumber(), getBankName(), getWaterMark());
    }

    public static final class SettingBuilder
    {
        private String accountNumber;
        private String bankName;
        private Image waterMark;

        public SettingBuilder setAccountNumber(String accountNumber)
        {
            this.accountNumber = accountNumber;
            return this;
        }

        public SettingBuilder setBankName(String bankName)
        {
            this.bankName = bankName;
            return this;
        }

        public SettingBuilder setWaterMark(Image waterMark)
        {
            this.waterMark = waterMark;
            return this;
        }

        public Setting buildSetting()
        {
            return new Setting(accountNumber, bankName, waterMark);
        }
    }
}
