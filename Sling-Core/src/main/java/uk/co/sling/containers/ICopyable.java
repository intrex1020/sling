package uk.co.sling.containers;

/**
 * <p>Allows object to be copyable</p>
 *
 * @author Sebastian Wojtkowiak
 */
public interface ICopyable
{
    /**
     * <p>Creates exact copy of the {@link IContainer} object.
     * Where o1 == o2 will be {@code false} as the
     * returned copy is a hard copy. Which means
     * for both objects o1.equals(o2) should return
     * {@code true} but both of them are not the same.</p>
     *
     * @return copy of the object
     */
    IContainer copy();
}
