package uk.co.sling.containers;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;

/**
 * <p>Implements contractor profile that
 * stores information about the contractor.</p>
 *
 * @author sebastian wojtkowiak
 */
public final class Contractor extends AbstractProfile<Invoice>
{
    private Contractor(String companyName, String vatCode, String town, String postCode, String street, String buildingNumber, String phone, String email)
    {
        super(companyName, vatCode, town, postCode, street, buildingNumber, phone, email);
    }

    @Override
    public Invoice getItem(String uniqueCode)
    {
        if (uniqueCode == null)
            throw new InvalidValueException(Localisation.getValuePL("notExistingContractor"));

        for (Invoice item : getAllItems())
            if (item.getInvoiceUniqueCode().equals(uniqueCode))
                return item.copy();

        //value not found
        throw new InvalidValueException(Localisation.getValuePL("notExistingContractor"));
    }

    @Override
    public boolean equals(Object item)
    {
        if (this == item)
            return true;

        if (!(item instanceof Contractor))
            return false;

        Contractor c = (Contractor)item;

        return this.getVatCode().equals(c.getVatCode());
    }

    @Override
    public int hashCode()
    {
        int prime = 97;

        return this.getVatCode().hashCode() * prime;
    }

    @Override
    public Contractor copy()
    {
        return new Contractor(getCompanyName(), getVatCode(), getTown(), getPostCode(),
                getStreet(), getBuildingNumber(), getPhone(), getEmail());
    }

    public static final class ContractorBuilder
    {
        private String companyName;
        private String vatCode;
        private String town;
        private String postCode;
        private String street;
        private String buildingNumber;
        private String phone;
        private String email;

        public ContractorBuilder setCompanyName(String companyName)
        {
            this.companyName = companyName;
            return this;
        }

        public ContractorBuilder setVatCode(String nip)
        {
            this.vatCode = nip;
            return this;
        }

        public ContractorBuilder setTown(String town)
        {
            this.town = town;
            return this;
        }

        public ContractorBuilder setPostCode(String postCode)
        {
            this.postCode = postCode;
            return this;
        }

        public ContractorBuilder setStreet(String street)
        {
            this.street = street;
            return this;
        }

        public ContractorBuilder setBuildingNumber(String buildingNumber)
        {
            this.buildingNumber = buildingNumber;
            return this;
        }

        public ContractorBuilder setPhoneNumber(String phoneNumber)
        {
            this.phone = phoneNumber;
            return this;
        }

        public ContractorBuilder setEmail(String email)
        {
            this.email = email;
            return this;
        }

        public Contractor buildContractor()
        {
            return new Contractor(companyName, vatCode, town, postCode, street, buildingNumber, phone, email);
        }
    }
}
