package uk.co.sling.containers;

/**
 *
 * <p>Container interface used to group
 * all containers under common hood.<br></p>
 *
 * @author Sebastian Wojtkowiak
 */
public interface IContainer extends ICopyable
{

}
