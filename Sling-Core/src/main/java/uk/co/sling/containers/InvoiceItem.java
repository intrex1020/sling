package uk.co.sling.containers;

import uk.co.sling.collection_holder.AbstractItem;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;
import uk.co.sling.utils.Calculator;
import uk.co.sling.utils.Utils;

/**
 * <p>Implements {@link AbstractItem} and
 * stores information about the {@link InvoiceItem}.</p>
 *
 * @author sebastian wojtkowiak
 */
public final class InvoiceItem extends AbstractItem implements IContainer
{
	private int itemIndex;
	private String itemDescription;
	private double itemAmount;
	private String measurementUnit;
	private double vatPercentage;
	private double priceNetto;

	private InvoiceItem(int itemIndex, String itemDescription, double itemAmount, String measurementUnit, double vatPercentage, double priceNetto)
	{
		setItemIndex(itemIndex);
		setItemDescription(itemDescription);
		setItemAmount(itemAmount);
		setMeasurementUnit(measurementUnit);
		setVatPercentage(vatPercentage);
		setPriceNetto(priceNetto);
	}

	/**
	 * <p>Returns {@link InvoiceItem#itemIndex}</p>
	 *
	 * @return item index
	 */
	public int getItemIndex()
	{
		return itemIndex;
	}

	/**
	 * <p>Sets {@link InvoiceItem#itemIndex}<br>
	 *
	 * The number indicates the index of the {@link InvoiceItem} on the
	 * {@link Invoice}.<br>
	 *
	 * If there is an attempt of setting the
	 * {@link InvoiceItem#itemIndex} value less than 1 the {@link InvalidValueException} will be
	 * thrown to indicate that an attempt to set {@link InvoiceItem#itemIndex}
	 * with value less than 1 has been made.<br>
	 *
	 * @param itemIndex index
	 * @throws InvalidValueException if index is less than 1
	 */
	public void setItemIndex(int itemIndex)
	{
		if (itemIndex < 1)
			throw new InvalidValueException(Localisation.getValuePL("invalidItemIndex"));

		else
			this.itemIndex = itemIndex;
	}

	/**
	 * <p>Returns {@link InvoiceItem#itemDescription}</p>
	 *
	 * @return item description
	 */
	public String getItemDescription()
	{
		return itemDescription;
	}

	/**
	 * <p>Sets {@link InvoiceItem#itemDescription}
	 *
	 * The passed value can contain any characters, however
	 * if it starts or ends with a white character it will
	 * be handled as an error case.<br>
	 *
	 * If there is an attempt of setting the
	 * {@link InvoiceItem#itemDescription} as null or
	 * an empty string the {@link ValueNotSetException} will be
	 * thrown to indicate that an attempt to set {@link InvoiceItem#itemDescription}
	 * as null or empty string has been made.<br>
	 *
	 * If there is an attempt to save the {@link InvoiceItem#itemDescription} as white characters
	 * or the last character is a white character the {@link InvalidValueException}
	 * will be thrown to indicate invalid attempt.</p>
	 *
	 * @param itemDescription item description
	 * @throws InvalidValueException if the description starts or ends with white character
	 * @throws ValueNotSetException if the description is null or empty
	 */
	public void setItemDescription(String itemDescription)
	{
		if (itemDescription == null || itemDescription.isEmpty())
			throw new ValueNotSetException(Localisation.getValuePL("notSetItemDescription"));
		else if (Utils.isFieldNotValid(itemDescription))
			throw new InvalidValueException(Localisation.getValuePL("invalidItemDescription"));
		else
			this.itemDescription = itemDescription;
	}

	/**
	 * <p>Returns {@link InvoiceItem#itemAmount}</p>
	 *
	 * @return item amount
	 */
	public double getItemAmount()
	{
		return itemAmount;
	}

	/**
	 * <p>Sets {@link InvoiceItem#itemAmount}<br>
	 *
	 * The number indicates the amount of of the {@link InvoiceItem} on the
	 * {@link Invoice}.<br>
	 *
	 * If there is an attempt of setting the
	 * {@link InvoiceItem#itemAmount} value less than 0.001 the {@link InvalidValueException} will be
	 * thrown to indicate that an attempt to set {@link InvoiceItem#itemAmount}
	 * with value less than 0.001 has been made.<br>
	 *
	 * @param itemAmount item amount
	 * @throws InvalidValueException if amount is less than 0.001
	 */
	public void setItemAmount(double itemAmount) throws InvalidValueException
	{
		if (itemAmount < 0.001)
			throw new InvalidValueException(Localisation.getValuePL("invalidItemAmount"));

		else
			this.itemAmount = itemAmount;
	}

	/**
	 * <p>Returns {@link InvoiceItem#measurementUnit}</p>
	 *
	 * @return measurement unit
	 */
	public String getMeasurementUnit()
	{
		return measurementUnit;
	}

	/**
	 * <p>Sets {@link InvoiceItem#measurementUnit}<br>
	 *
	 * The passed value can contain any characters, however
	 * if it starts or ends with a white character it will
	 * be handled as an error case.<br>
	 *
	 * If there is an attempt of setting the
	 * {@link InvoiceItem#measurementUnit} as null or
	 * an empty string the {@link ValueNotSetException} will be
	 * thrown to indicate that an attempt to set {@link InvoiceItem#measurementUnit}
	 * as null or empty string has been made.<br>
	 *
	 * If there is an attempt to save the {@link InvoiceItem#measurementUnit} as white characters
	 * or the last character is a white character the {@link InvalidValueException}
	 * will be thrown to indicate invalid attempt.</p>
	 *
	 * @param measurementUnit measurement unit
	 * @throws InvalidValueException if the description starts or ends with white character
	 * @throws ValueNotSetException if the description is null or empty
	 */
	public void setMeasurementUnit(String measurementUnit)
	{
		if (measurementUnit == null || measurementUnit.isEmpty())
			throw new ValueNotSetException(Localisation.getValuePL("notSetMeasurementUnit"));
		else if (Utils.isFieldNotValid(measurementUnit))
			throw new InvalidValueException(Localisation.getValuePL("invalidMeasurementUnit"));
		else
			this.measurementUnit = measurementUnit;
	}

	/**
	 * <p>Returns {@link InvoiceItem#vatPercentage}</p>
	 *
	 * @return vat percentage
	 */
	public double getVatPercentage()
	{
		return vatPercentage;
	}

	/**
	 * <p>Sets {@link InvoiceItem#vatPercentage}<br>
	 *
	 * The number indicates the vat percentage in {@link InvoiceItem} on the
	 * {@link Invoice}.<br>
	 *
	 * If there is an attempt of setting the
	 * {@link InvoiceItem#vatPercentage} value less than 0 or greater than 100
	 * the {@link InvalidValueException} will be thrown to indicate that
	 * an attempt to set {@link InvoiceItem#vatPercentage}
	 * with value less than 0 or greater than 100 has been made.<br>
	 *
	 * @param vatPercentage vat percentage
	 * @throws InvalidValueException if vat percentage is less than 0 or greater than 100
	 */
	public void setVatPercentage(double vatPercentage)
	{
		if (vatPercentage < 0 || vatPercentage > 100)
			throw new InvalidValueException(Localisation.getValuePL("invalidVatPercentage"));

		else
			this.vatPercentage = vatPercentage;
	}

	/**
	 * <p>Returns {@link InvoiceItem#priceNetto}</p>
	 *
	 * @return price netto
	 */
	public double getPriceNetto()
	{
		return priceNetto;
	}

	/**
	 * <p>Sets {@link InvoiceItem#priceNetto}<br>
	 *
	 * 	The number indicates the price netto of a single {@link InvoiceItem}<br>
	 *
	 * If there is an attempt of setting the
	 * {@link InvoiceItem#priceNetto} value less than 0 the {@link InvalidValueException} will be
	 * thrown to indicate that an attempt to set {@link InvoiceItem#priceNetto}
	 * with value less than 0 has been made.<br>
	 *
	 * @param priceNetto price netto
	 * @throws InvalidValueException if price is less than 0
	 */
	public void setPriceNetto(double priceNetto)
	{
		if (priceNetto < 0)
			throw new InvalidValueException(Localisation.getValuePL("invalidPriceNetto"));
		else
			this.priceNetto = priceNetto;
	}

	@Override
	public InvoiceItem copy()
	{
		return new InvoiceItem(getItemIndex(), getItemDescription(), getItemAmount(), getMeasurementUnit(), getVatPercentage(), getPriceNetto());
	}

	@Override
	public boolean equals(Object item)
	{
		if (this == item)
			return true;

		if (!(item instanceof InvoiceItem))
			return false;

		InvoiceItem ii = (InvoiceItem)item;

		return getItemIndex() == ii.getItemIndex() &&
				getItemDescription().equals(ii.getItemDescription()) &&
				getItemAmount() == ii.getItemAmount() &&
				getMeasurementUnit().equals(ii.getMeasurementUnit()) &&
				getVatPercentage() == ii.getVatPercentage() &&
				getPriceNetto() == ii.getPriceNetto();
	}

	@Override
	public int hashCode()
	{
		int prime1 = 31;
		int prime2 = 97;

		return prime1 * prime2 + getItemIndex() + getItemDescription().hashCode();
	}

	public static final class InvoiceItemBuilder
	{
		private int itemIndex;
		private String itemDescription;
		private double itemAmount;
		private String measurementUnit;
		private double vatPercentage;
		private double priceNetto;

		public InvoiceItemBuilder setItemIndex(int itemIndex)
		{
			this.itemIndex = itemIndex;
			return this;
		}

		public InvoiceItemBuilder setItemDescription(String itemDescription)
		{
			this.itemDescription = itemDescription;
			return this;
		}

		public InvoiceItemBuilder setItemAmount(double itemAmount)
		{
			this.itemAmount = itemAmount;
			return this;
		}

		public InvoiceItemBuilder setMeasurementUnit(String measurementUnit)
		{
			this.measurementUnit = measurementUnit;
			return this;
		}

		public InvoiceItemBuilder setVatPercentage(double vatPercentage)
		{
			this.vatPercentage = vatPercentage;
			return this;
		}

		public InvoiceItemBuilder setPriceNetto(double priceNetto)
		{
			this.priceNetto = priceNetto;
			return this;
		}

		public InvoiceItem buildInvoiceItem() throws InvalidValueException, ValueNotSetException
		{
			return new InvoiceItem(itemIndex, itemDescription, itemAmount, measurementUnit, vatPercentage, priceNetto);
		}
	}
}