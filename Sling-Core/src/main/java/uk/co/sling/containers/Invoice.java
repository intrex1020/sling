package uk.co.sling.containers;

import static java.lang.Integer.parseInt;

import uk.co.sling.collection_holder.AbstractItemWithHolder;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.local_date.LocalDateTimeEUFormat;
import uk.co.sling.locale.Localisation;
import uk.co.sling.utils.PaymentType;

import java.time.Month;

/**
 * <p>Implements {@link AbstractItemWithHolder} and
 * stores information about the {@link Invoice} such as
 * {@link InvoiceItem}s and {@link Invoice} properties.</p>
 *
 * @author sebastian wojtkowiak
 */
public final class Invoice extends AbstractItemWithHolder<InvoiceItem> implements IContainer
{
    private String invoiceUniqueCode;
    //unique for 1 year
    private int invoiceNumber;
    private User issuedBy;
    private Contractor issuedTo;
    private Setting setting;
    private LocalDateTimeEUFormat issueDate;
    private LocalDateTimeEUFormat payUntil;
    private int daysToPay;
    private boolean isPaid;
    private PaymentType paymentType;

    private Invoice(int documentNumber, User issuedBy, Contractor issuedTo, Setting setting,
                    LocalDateTimeEUFormat issueDate, int daysToPay, boolean isPaid, PaymentType paymentType)
    {
        setIssuedBy(issuedBy);
        setIssuedTo(issuedTo);
        setSetting(setting);
        setIssueDate(issueDate);
        setDaysToPay(daysToPay);
        setPayUntil(getIssueDate().plusDays(getDaysToPay()));
        setIsPaid(isPaid);
        setPaymentType(paymentType);
        setInvoiceNumber(documentNumber);
        setInvoiceUniqueCode(getInvoiceNumber()+"-"+getIssueDate());
    }

    /**
     * <p>Returns the {@link Invoice#invoiceUniqueCode}</p>
     *
     * @return unique code
     */
    public String getInvoiceUniqueCode()
    {
        return invoiceUniqueCode;
    }

    /**
     * <p>Sets the {@link Invoice#invoiceUniqueCode}.<br>
     *
     * The passed value can must meet specific format:<br>
     * \\d+-dd-mm-yyyy.<br>
     * Where first number is the {@link Invoice#invoiceNumber} followed by the {@link Invoice#issueDate}.
     * To create appropriate date of issue use {@link LocalDateTimeEUFormat}
     * class that will generate the date in the correct format, and append
     * any integer with '-' suffix.<br>
     * The number before the date should be consistent with the {@link Invoice#invoiceNumber}
     * as the unique code consists of the {@link Invoice#invoiceNumber}-{@link Invoice#issueDate}
     * format.<br>
     *
     * Modifying this value will affect the {@link Invoice#invoiceNumber} and {@link Invoice#issueDate}
     * as the unique code consists of number followed by issue date.<br>
     *
     * If there is an attempt of setting the
     * {@link Invoice#invoiceUniqueCode} as null the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link Invoice#invoiceUniqueCode}
     * as null has been made.<br>
     *
     * If the {@link Invoice#invoiceUniqueCode} code doesn't meet the specified format the
     * {@link InvalidValueException} will be thrown to indicate that.</p>
     *
     * @param invoiceUniqueCode to set
     * @throws ValueNotSetException if the string is null
     * @throws InvalidValueException if the unique code doesn't meet the formatting criteria
     */
    public void setInvoiceUniqueCode(String invoiceUniqueCode)
    {
        if (invoiceUniqueCode == null)
            throw new ValueNotSetException(Localisation.getValuePL("notSetUniqueCode"));
        else if (!invoiceUniqueCode.matches("\\d+-\\d\\d-\\d\\d-\\d\\d\\d\\d"))
            throw new InvalidValueException(Localisation.getValuePL("invalidUniqueCode"));
        else
        {
            //at this point the regex is passed so we know everything is a digit :)
            this.invoiceUniqueCode = invoiceUniqueCode;
            invoiceNumber = Integer.parseInt(invoiceUniqueCode.split("-")[0]);
            //we know the format is d+-dd-dd-ddddd where d+ (index 0) is invoice number
            String[] date = invoiceUniqueCode.split("-");
            LocalDateTimeEUFormat tmp = LocalDateTimeEUFormat.of(Integer.parseInt(date[1]), Integer.parseInt(date[2]),
                    Integer.parseInt(date[3]));

            int day = tmp.getDayOfMonth() - issueDate.getDayOfMonth();
            int month = tmp.getMonth().getValue() - issueDate.getMonth().getValue();
            int year =  tmp.getYear() - issueDate.getYear();
            //ehh... immutable :(. Using of will give an issue where milliseconds are different
            issueDate = (day < 0) ? issueDate.plusDays(day) : issueDate.minusDays(day);
            issueDate = (month < 0) ? issueDate.plusMonths(month) : issueDate.minusMonths(month);
            issueDate = (year < 0) ? issueDate.plusYears(year) : issueDate.minusYears(year);
        }
    }

    /**
     * <p>Gets the {@link Invoice#invoiceNumber}</p>
     *
     * @return invoice number
     */
    public int getInvoiceNumber()
    {
        return invoiceNumber;
    }

    /**
     * <p>Sets the {@link Invoice#invoiceNumber}.<br>
     *
     * The passed value should be unique and never used by
     * any other invoice with the same {@link User} context.<br>
     *
     * This value is also used to build {@link Invoice#invoiceUniqueCode}.
     * Modification of this value will modify the {@link Invoice#invoiceUniqueCode} to
     * be correct.<br>
     *
     * The {@link Invoice#invoiceNumber} should never be less
     * than 1. 1 is the minimum value that {@link Invoice#invoiceNumber}
     * can ever have.<br>
     *
     * If there is an attempt of setting the
     * {@link Invoice#invoiceNumber} value less than 1 the {@link InvalidValueException} will be
     * thrown to indicate that an attempt to set {@link Invoice#invoiceNumber}
     * with value less than 1 has been made.<br>
     *
     * @param invoiceNumber to set
     * @throws InvalidValueException if the value to set is less than 1
     */
    public void setInvoiceNumber(int invoiceNumber)
    {
        if (invoiceNumber < 1)
            throw new InvalidValueException(Localisation.getValuePL("invalidInvoiceNumber"));

        else
        {
            this.invoiceNumber = invoiceNumber;
            setInvoiceUniqueCode(getInvoiceNumber()+"-"+getIssueDate());
        }
    }

    /**
     * <p>Returns a copy of the {@link User} that has issued the {@link Invoice}</p>
     *
     * @return issuer
     */
    public User getIssuedBy()
    {
        return issuedBy.copy();
    }

    /**
     * <p>Sets the {@link Invoice#issuedBy}.<br>
     *
     * The passed value indicates by whom the {@link Invoice} has been created.<br>
     *
     * The {@link Invoice#issuedBy} should never be null.<br>
     *
     * If there is an attempt of setting the
     * {@link Invoice#issuedBy} value as null the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link Invoice#issuedBy}
     * with value null has been made.<br>
     *
     * @param issuedBy issuer
     * @throws InvalidValueException if the issuer is null
     */
    public void setIssuedBy(User issuedBy)
    {
        if (issuedBy == null)
            throw new ValueNotSetException(Localisation.getValuePL("notSetUser"));

        else
            this.issuedBy = issuedBy;
    }

    /**
     * <p>Gets the {@link Contractor} that the {@link Invoice} was issued to</p>
     *
     * @return contractor
     */
    public Contractor getIssuedTo()
    {
        return issuedTo.copy();
    }

    /**
     * <p>Sets the {@link Invoice#issuedTo}.<br>
     *
     * The passed value indicates to whom the {@link Invoice} has been issued.<br>
     *
     * The {@link Invoice#issuedTo} should never be null.<br>
     *
     * If there is an attempt of setting the
     * {@link Invoice#issuedTo} value as null the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link Invoice#issuedTo}
     * with value null has been made.<br>
     *
     * @param issuedTo issued to
     * @throws InvalidValueException if the issuedTo is null
     */
    public void setIssuedTo(Contractor issuedTo)
    {
        if (issuedTo == null)
            throw new ValueNotSetException(Localisation.getValuePL("notSetContractor"));

        else
            this.issuedTo = issuedTo;
    }

    /**
     * <p>Gets the {@link Setting} of the
     * {@link Invoice}</p>
     *
     * @return setting
     */
    public Setting getSetting()
    {
        return setting.copy();
    }

    /**
     * <p>Sets the {@link Invoice#setting}.<br>
     *
     * The passed value indicates the {@link Setting} with which
     * the {@link Invoice} has been issued.<br>
     *
     * The {@link Invoice#setting} should never be null.<br>
     *
     * If there is an attempt of setting the
     * {@link Invoice#setting} value as null the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link Invoice#setting}
     * with value null has been made.<br>
     *
     * @param setting setting
     * @throws InvalidValueException if the setting is null
     */
    public void setSetting(Setting setting)
    {
        if (setting == null)
            throw new ValueNotSetException(Localisation.getValuePL("notSetSettings"));

        else
            this.setting = setting;
    }

    /**
     * <p>Gets the {@link Invoice#issueDate}</p>
     *
     * @return date of issue
     */
    public LocalDateTimeEUFormat getIssueDate()
    {
        return issueDate;
    }

    /**
     * <p>Sets the {@link Invoice#issueDate}.<br>
     *
     * The passed value indicates when {@link Invoice} was issued.<br>
     *
     * Modifying this value means that {@link Invoice#payUntil} will be
     * adjusted so the {@link Invoice#daysToPay} is correct and shows
     * the difference in days between {@link Invoice#issueDate} and
     * {@link Invoice#payUntil}.<br>
     *
     * This value is also used to build {@link Invoice#invoiceUniqueCode}.
     * Modification of this value will modify the {@link Invoice#invoiceUniqueCode} to
     * be correct.<br>
     *
     * The {@link Invoice#issueDate} should never be null,
     * and should have format of {@link LocalDateTimeEUFormat}<br>
     *
     * If there is an attempt of setting the
     * {@link Invoice#issueDate} value as null the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link Invoice#issueDate}
     * with value null has been made.<br>
     *
     * @param issueDate date
     * @throws InvalidValueException if the date is null
     */
    public void setIssueDate(LocalDateTimeEUFormat issueDate)
    {
        if (issueDate == null)
            throw new ValueNotSetException(Localisation.getValuePL("notSetIssueDate"));

        else
        {
            this.issueDate = issueDate;
            payUntil = getIssueDate().plusDays(getDaysToPay());
            setInvoiceUniqueCode(getInvoiceNumber()+"-"+getIssueDate());
        }
    }

    /**
     * <p>Gets the {@link Invoice#payUntil} which indicates the deadline
     * of the payment.<br>
     *
     * The {@link Invoice#payUntil} is determined using the {@link Invoice#issueDate}
     * and {@link Invoice#daysToPay}.<br></p>
     *
     * @return last day for payment
     */
    public LocalDateTimeEUFormat getPayUntil()
    {
        return payUntil;
    }

    /**
     * <p>Sets the {@link Invoice#payUntil}.<br>
     *
     * The passed value indicates the deadline for the {@link Invoice}
     * to be paid.<br>
     *
     * Modifying this value means that {@link Invoice#issueDate} will be
     * adjusted so the {@link Invoice#daysToPay} is correct and shows
     * the difference in days between {@link Invoice#issueDate} and
     * {@link Invoice#payUntil}.<br>
     *
     * The {@link Invoice#payUntil} should never be null,
     * and should have format of {@link LocalDateTimeEUFormat}<br>
     *
     * If there is an attempt of setting the
     * {@link Invoice#payUntil} value as null the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link Invoice#payUntil}
     * with value null has been made.<br>
     *
     * @param payUntil date
     * @throws InvalidValueException if the date is null
     */
    public void setPayUntil(LocalDateTimeEUFormat payUntil)
    {
        if (payUntil == null)
            throw new ValueNotSetException(Localisation.getValuePL("notSetPayDate"));

        else
        {
            this.payUntil = payUntil;
            issueDate = getPayUntil().minusDays(getDaysToPay());
        }
    }

    /**
     * <p>Gets the {@link Invoice#daysToPay}<br>
     *
     *  The returned value should be equal to the difference
     *  of {@link Invoice#payUntil} and {@link Invoice#issueDate}</p>
     *
     * @return days to pay
     */
    public int getDaysToPay()
    {
        return daysToPay;
    }

    /**
     * <p>Sets the {@link Invoice#daysToPay}<br>
     *
     * The number indicates maximum number of days that are available
     * to pay for the {@link Invoice}. The {@link Invoice#payUntil} is
     * determined using this value.<br>
     *
     * If there is an attempt of setting the
     * {@link Invoice#daysToPay} value less than 0 the {@link InvalidValueException} will be
     * thrown to indicate that an attempt to set {@link Invoice#daysToPay}
     * with value less than 0 has been made.<br>
     *
     * @param daysToPay days to pay
     * @throws InvalidValueException if days to pay is less than 0
     */
    public void setDaysToPay(int daysToPay)
    {
        if (daysToPay < 0)
            throw new InvalidValueException(Localisation.getValuePL("invalidDaysToPay"));

        else
        {
            this.daysToPay = daysToPay;
            setPayUntil(getIssueDate().plusDays(getDaysToPay()));
        }
    }

    /**
     * <p>Gets the {@link Invoice#isPaid}<br>
     *
     * This indicates whether the {@link Invoice} has
     * been already paid or not.</p>
     *
     * @return {@code true} if the invoice is paid
     */
    public boolean getIsPaid()
    {
        return isPaid;
    }

    /**
     * <p>Sets the {@link Invoice#isPaid}<br>
     *
     * If the value to set is {@code true} this will
     * indicate that the invoice is paid</p>
     *
     * @param isPaid {@code true} if the invoice is paid
     */
    public void setIsPaid(boolean isPaid)
    {
        this.isPaid = isPaid;
    }

    /**
     * <p>Gets the {@link Invoice#paymentType}<br>
     *
     * This indicates the expected {@link PaymentType} for
     * the {@link Invoice}.<br>
     *
     * The returned value will never be {@code null}</p>
     *
     * @return payment type
     */
    public PaymentType getPaymentType()
    {
        return paymentType;
    }

    /**
     * <p>Sets the {@link Invoice#paymentType}.<br>
     *
     * Sets on of the available {@link PaymentType}s for the invoice.<br>
     *
     * If there is an attempt of setting the
     * {@link Invoice#paymentType} value is null the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link Invoice#paymentType}
     * with value null has been made.<br>
     *
     * @param paymentType type
     * @throws ValueNotSetException if payment type is null
     */
    public void setPaymentType(PaymentType paymentType)
    {
        if (paymentType == null)
            throw new ValueNotSetException(Localisation.getValuePL("notSetPaymentType"));

        this.paymentType = paymentType;
    }

    @Override
    public InvoiceItem getItem(String index)
    {
        if (index == null)
            throw new InvalidValueException(Localisation.getValuePL("notExistingInvoiceItem"));

        try
        {
            int idx = parseInt(index);

            for (InvoiceItem it : getAllItems())
                if (it.getItemIndex() == idx)
                    return it.copy();
        }
        catch(NumberFormatException nfx) {}

        throw new InvalidValueException(Localisation.getValuePL("notExistingInvoiceItem"));
    }

    @Override
    public boolean equals(Object item)
    {
        if (this == item)
            return true;

        if (!(item instanceof Invoice))
            return false;

        Invoice inv = (Invoice)item;

        return this.getInvoiceUniqueCode().equals(inv.getInvoiceUniqueCode());
    }

    @Override
    public int hashCode()
    {
        int prime = 31;

        return getInvoiceUniqueCode().hashCode() * prime;
    }

    @Override
    public Invoice copy() throws InvalidValueException, ValueNotSetException
    {
        return new Invoice(getInvoiceNumber(), getIssuedBy(),
                getIssuedTo(), getSetting(), getIssueDate(), getDaysToPay(), getIsPaid(),
                getPaymentType());
    }

    public static final class InvoiceBuilder
    {
        //unique for 1 year
        private int documentNumber;
        private User issuedBy;
        private Contractor issuedTo;
        private Setting setting;
        private LocalDateTimeEUFormat issueDate;
        private int daysToPay;
        private boolean isPaid;
        private PaymentType paymentType;

        public InvoiceBuilder setInvoiceNumber(int documentNumber)
        {
            this.documentNumber = documentNumber;
            return this;
        }

        public InvoiceBuilder setIssuedBy(User issuedBy)
        {
            this.issuedBy = issuedBy;
            return this;
        }

        public InvoiceBuilder setIssuedTo(Contractor issuedTo)
        {
            this.issuedTo = issuedTo;
            return this;
        }

        public InvoiceBuilder setSetting(Setting setting)
        {
            this.setting = setting;
            return this;
        }

        public InvoiceBuilder setIssueDate(LocalDateTimeEUFormat issueDate)
        {
            this.issueDate = issueDate;
            return this;
        }

        public InvoiceBuilder setDaysToPay(int daysToPay)
        {
            this.daysToPay = daysToPay;
            return this;
        }

        public InvoiceBuilder setIsPaid(boolean isPaid)
        {
            this.isPaid = isPaid;
            return this;
        }

        public InvoiceBuilder setPaymentType(PaymentType paymentType)
        {
            this.paymentType = paymentType;
            return this;
        }

        public Invoice buildInvoice()
        {
            return new Invoice(documentNumber, issuedBy, issuedTo, setting, issueDate, daysToPay, isPaid, paymentType);
        }
    }
}
