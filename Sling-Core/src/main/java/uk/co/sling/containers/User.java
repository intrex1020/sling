package uk.co.sling.containers;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;
import uk.co.sling.utils.Utils;

/**
 * <p>Implements user profile that
 * stores information about the user.</p>
 *
 * @author sebastian wojtkowiak
 */
public final class User extends AbstractProfile<Contractor>
{
    private String userName;

    private User(String userName, String companyName, String vatCode, String town, String postCode, String street, String buildingNumber, String phone, String email)
    {
        super(companyName, vatCode, town, postCode, street, buildingNumber, phone, email);
        setUserName(userName);
    }

    /**
     * <p>Gets the {@link User#userName}</p>
     *
     * @return current username
     */
    public String getUserName()
    {
        return userName;
    }

    /**
     * <p>Sets the {@link User#userName}<br>
     *
     *  If there is an attempt of setting the
     * {@link User#userName} as null or an empty string the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link User#userName}
     * as null has been made.<br></p>
     *
     * @param userName to set
     * @throws ValueNotSetException if the username is null or empty
     */
    public void setUserName(String userName)
    {
        if (userName == null || userName.isEmpty())
            throw new ValueNotSetException(Localisation.getValuePL("notSetUserName"));
        else if (Utils.isFieldNotValid(userName))
            throw new InvalidValueException(Localisation.getValuePL("invalidUserName"));
        else
            this.userName = userName;
    }

    @Override
    public Contractor getItem(String companyName)
    {
        if (companyName == null)
            throw new InvalidValueException(Localisation.getValuePL("notExistingContractor"));

        for (Contractor item : getAllItems())
            if (item.getCompanyName().equals(companyName))
                return item.copy();

        throw new InvalidValueException(Localisation.getValuePL("notExistingContractor"));
    }

    @Override
    public boolean equals(Object item)
    {
        if (this == item)
            return true;

        if (!(item instanceof User))
            return false;

        User u = (User)item;

        return this.getUserName().equals(u.getUserName());
    }

    @Override
    public int hashCode()
    {
        int prime = 149;

        return this.getUserName().hashCode() * prime;
    }

    @Override
    public User copy()
    {
        return new User(getUserName(), getCompanyName(), getVatCode(), getTown(), getPostCode(),
                getStreet(), getBuildingNumber(), getPhone(), getEmail());
    }

    public static final class UserBuilder
    {
        private String userName;
        private String companyName;
        private String vatCode;
        private String town;
        private String postCode;
        private String street;
        private String buildingNumber;
        private String phone;
        private String email;

        public UserBuilder setUserName(String userName)
        {
            this.userName = userName;
            return this;
        }

        public UserBuilder setCompanyName(String companyName)
        {
            this.companyName = companyName;
            return this;
        }

        public UserBuilder setVatCode(String nip)
        {
            this.vatCode = nip;
            return this;
        }

        public UserBuilder setTown(String town)
        {
            this.town = town;
            return this;
        }

        public UserBuilder setPostCode(String postCode)
        {
            this.postCode = postCode;
            return this;
        }

        public UserBuilder setStreet(String street)
        {
            this.street = street;
            return this;
        }

        public UserBuilder setBuildingNumber(String buildingNumber)
        {
            this.buildingNumber = buildingNumber;
            return this;
        }

        public UserBuilder setPhoneNumber(String phoneNumber)
        {
            this.phone = phoneNumber;
            return this;
        }

        public UserBuilder setEmail(String email)
        {
            this.email = email;
            return this;
        }

        public User buildUser()
        {
            return new User(userName, companyName, vatCode, town, postCode, street, buildingNumber, phone, email);
        }
    }
}
