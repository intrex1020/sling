package uk.co.sling.containers;

import uk.co.sling.collection_holder.AbstractItem;
import uk.co.sling.collection_holder.AbstractItemWithHolder;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;
import uk.co.sling.utils.Utils;

/**
 * <p>Provides default implementation of {@link IContainer}</p>
 *
 * @author sebastian wojtkowiak
 */
abstract class AbstractProfile<T extends AbstractItem> extends AbstractItemWithHolder<T> implements IContainer
{
    private String companyName;
    private String vatCode;
    private String town;
    private String postCode;
    private String street;
    private String buildingNumber;
    private String phone;
    private String email;

    AbstractProfile(String companyName, String vatCode, String town, String postCode, String street, String buildingNumber, String phone, String email)
    {
        setCompanyName(companyName);
        setVatCode(vatCode);
        setTown(town);
        setPostCode(postCode);
        setStreet(street);
        setBuildingNumber(buildingNumber);
        setPhone(phone);
        setEmail(email);
    }

    /**
     * <p>Gets the {@link AbstractProfile#companyName}</p>
     *
     * @return company name
     */
    public final String getCompanyName()
    {
        return companyName;
    }

    /**
     * <p>Sets the {@link AbstractProfile#companyName}.<br>
     *
     * The passed value can contain any characters, however
     * if it starts or ends with a white character it will
     * be handled as an error case.<br>
     *
     * If there is an attempt of setting the
     * {@link AbstractProfile#companyName} as null or an empty string t
     * he {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link AbstractProfile#companyName}
     * as null has been made.<br>
     *
     * If there is an attempt to save the {@link AbstractProfile#companyName} name as white characters
     * or the last character is a white character the {@link InvalidValueException}
     * will be thrown to indicate invalid attempt.</p>
     *
     * @param companyName to set
     * @throws ValueNotSetException if the string is empty or null
     * @throws InvalidValueException if starts or ends with white character
     */
    public final void setCompanyName(String companyName)
    {
        if (companyName == null || companyName.isEmpty())
            throw new ValueNotSetException(Localisation.getValuePL("notSetCompanyName"));
        else if (Utils.isFieldNotValid(companyName))
            throw new InvalidValueException(Localisation.getValuePL("invalidCompanyName"));
        else
            this.companyName = companyName;
    }

    /**
     * <p>Gets the {@link AbstractProfile#vatCode}</p>
     *
     * @return vatCode
     */
    public final String getVatCode()
    {
        return vatCode;
    }

    /**
     * <p>Sets the {@link AbstractProfile#vatCode}.<br>
     *
     * The passed value can contain any characters, however
     * if it starts or ends with a white character it will
     * be handled as an error case.<br>
     *
     * If there is an attempt of setting the
     * {@link AbstractProfile#vatCode} as null or an empty string the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link AbstractProfile#vatCode}
     * as null has been made.<br>
     *
     * If there is an attempt to save the {@link AbstractProfile#vatCode} as white characters
     * or the last character is a white character the {@link InvalidValueException}
     * will be thrown to indicate invalid attempt.</p>
     *
     * @param vatCode to set
     * @throws ValueNotSetException if the string is empty or null
     * @throws InvalidValueException if starts or ends with white character
     */
    public final void setVatCode(String vatCode)
    {
        if (vatCode == null || vatCode.isEmpty())
            throw new ValueNotSetException(Localisation.getValuePL("notSetVatCode"));
        else if (Utils.isFieldNotValid(vatCode))
            throw new InvalidValueException(Localisation.getValuePL("invalidVatCode"));
        else
            this.vatCode = vatCode;
    }

    /**
     * <p>Gets the {@link AbstractProfile#town}</p>
     *
     * @return town's name
     */
    public final String getTown()
    {
        return town;
    }

    /**
     * <p>Sets the {@link AbstractProfile#town}.<br>
     *
     * The passed value can contain any characters, however
     * if it starts or ends with a white character it will
     * be handled as an error case.<br>
     *
     * If there is an attempt of setting the
     * {@link AbstractProfile#town} as null or an empty string the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link AbstractProfile#town}
     * as null has been made.<br>
     *
     * If there is an attempt to save the {@link AbstractProfile#town} as white characters
     * or the last character is a white character the {@link InvalidValueException}
     * will be thrown to indicate invalid attempt.</p>
     *
     * @param town to set
     * @throws ValueNotSetException if the string is empty or null
     * @throws InvalidValueException if starts or ends with white character
     */
    public final void setTown(String town)
    {
        if (town == null || town.isEmpty())
            throw new ValueNotSetException(Localisation.getValuePL("notSetTown"));
        else if (Utils.isFieldNotValid(town))
            throw new InvalidValueException(Localisation.getValuePL("invalidTown"));
        else
            this.town = town;
    }

    /**
     * <p>Gets the {@link AbstractProfile#postCode}</p>
     *
     * @return post code
     */
    public final String getPostCode()
    {
        return postCode;
    }

    /**
     * <p>Sets the {@link AbstractProfile#postCode}.<br>
     *
     * The passed value can contain any characters, however
     * if it starts or ends with a white character it will
     * be handled as an error case.<br>
     *
     * If there is an attempt of setting the
     * {@link AbstractProfile#postCode} as null or an empty string the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link AbstractProfile#postCode}
     * as null has been made.<br>
     *
     * If there is an attempt to save the {@link AbstractProfile#postCode} as white characters
     * or the last character is a white character the {@link InvalidValueException}
     * will be thrown to indicate invalid attempt.</p>
     *
     * @param postCode to set
     * @throws ValueNotSetException if the string is empty or null
     * @throws InvalidValueException if starts or ends with white character
     */
    public final void setPostCode(String postCode)
    {
        if (postCode == null || postCode.isEmpty())
            throw new ValueNotSetException(Localisation.getValuePL("notSetPostCode"));
        else if (Utils.isFieldNotValid(postCode))
            throw new InvalidValueException(Localisation.getValuePL("invalidPostCode"));
        else
            this.postCode = postCode;
    }

    /**
     * <p>Gets the {@link AbstractProfile#street}</p>
     *
     * @return street name
     */
    public final String getStreet()
    {
        return street;
    }

    /**
     * <p>Sets the {@link AbstractProfile#street}.<br>
     *
     * The passed value can contain any characters, however
     * if it starts or ends with a white character it will
     * be handled as an error case.<br>
     *
     * If there is an attempt of setting the
     * {@link AbstractProfile#street} as null or an empty string the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link AbstractProfile#street}
     * as null has been made.<br>
     *
     * If there is an attempt to save the {@link AbstractProfile#street} as white characters
     * or the last character is a white character the {@link InvalidValueException}
     * will be thrown to indicate invalid attempt.</p>
     *
     * @param street to set
     * @throws ValueNotSetException if the string is empty or null
     * @throws InvalidValueException if starts or ends with white character
     */
    public final void setStreet(String street)
    {
        if (street == null || street.isEmpty())
            throw new ValueNotSetException(Localisation.getValuePL("notSetStreet"));
        else if (Utils.isFieldNotValid(street))
            throw new InvalidValueException(Localisation.getValuePL("invalidStreet"));
        else
            this.street = street;
    }

    /**
     * <p>Gets the {@link AbstractProfile#buildingNumber} (can include letters)</p>
     *
     * @return building number
     */
    public final String getBuildingNumber()
    {
        return buildingNumber;
    }

    /**
     * <p>Sets the {@link AbstractProfile#buildingNumber}.<br>
     *
     * The passed value can contain any characters, however
     * if it starts or ends with a white character it will
     * be handled as an error case.<br>
     *
     * If there is an attempt of setting the
     * {@link AbstractProfile#buildingNumber} as null or an empty string the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link AbstractProfile#buildingNumber}
     * as null has been made.<br>
     *
     * If there is an attempt to save the {@link AbstractProfile#buildingNumber} as white characters
     * or the last character is a white character the {@link InvalidValueException}
     * will be thrown to indicate invalid attempt.</p>
     *
     * @param buildingNumber to set
     * @throws ValueNotSetException if the string is empty or null
     * @throws InvalidValueException if starts or ends with white character
     */
    public final void setBuildingNumber(String buildingNumber)
    {
        if (buildingNumber == null || buildingNumber.isEmpty())
            throw new ValueNotSetException(Localisation.getValuePL("notSetBuilding"));
        else if (Utils.isFieldNotValid(buildingNumber))
            throw new InvalidValueException(Localisation.getValuePL("invalidBuilding"));
        else
            this.buildingNumber = buildingNumber;
    }

    /**
     * <p>Gets the {@link AbstractProfile#phone} number</p>
     *
     * @return phone number
     */
    public final String getPhone()
    {
        return phone;
    }

    /**
     * <p>Sets the {@link AbstractProfile#phone}.<br>
     *
     * The passed value must have correct number
     * format. The number can have '+' prefix to
     * indicate country. If the +XX or +XXX format
     * is used a white space is allowed to separate
     * the region from the number.<br>
     *
     * If there is an attempt of setting the
     * {@link AbstractProfile#phone} as null or an empty string the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link AbstractProfile#phone}
     * as null has been made.<br>
     *
     * The valid formats of the phone numbers are:<br>
     * The suffix on of: +XX or +XXX followed by an
     * optional space then followed by the phone number.
     * If the {@link AbstractProfile#phone} number doesn't match the format
     * the {@link InvalidValueException} will be thrown
     * to indicate invalid format of the phone number.</p>
     *
     * @param phone to set
     * @throws ValueNotSetException if the string is empty or null
     * @throws InvalidValueException if format doesn't match
     */
    public final void setPhone(String phone)
    {
        if (phone == null)
            throw new ValueNotSetException(Localisation.getValuePL("notSetPhone"));

        else if (!phone.matches("(\\+[0-9]{1,3} [0-9]++)|([0-9]++)|()"))
            throw new InvalidValueException(Localisation.getValuePL("incorrectPhone"));

        else
            this.phone = phone;
    }

    /**
     * <p>Gets the {@link AbstractProfile#email} address</p>
     *
     * @return e-mail address
     */
    public final String getEmail()
    {
        return email;
    }

    /**
     * <p>Sets the {@link AbstractProfile#email}.<br>
     *
     * The passed value must have correct email
     * format. The {@link AbstractProfile#email} can contain all letter
     * characters and numbers in the name as well as the
     * '.', '_', '+' and '-'.<br>
     * In the host name the {@link AbstractProfile#email} can contain all letter
     * characters and numbers plus additional characters
     * such as '.' or '-'.<br>
     * The domain must consists of letter characters
     * in length between 2 and 6. Where 2 is the minimum
     * length and 6 is the maximum.<br>
     *
     * If there is an attempt of setting the
     * {@link AbstractProfile#email} as null or an empty string the {@link ValueNotSetException} will be
     * thrown to indicate that an attempt to set {@link AbstractProfile#email}
     * as null has been made.<br>
     *
     * If the valid format of an email address is not met
     * the {@link InvalidValueException} will be thrown
     * to indicate that error.
     *
     * @param email to set
     * @throws ValueNotSetException if the string is empty or null
     * @throws InvalidValueException if format doesn't match
     */
    public final void setEmail(String email)
    {
        if (email == null)
            throw new ValueNotSetException(Localisation.getValuePL("notSetEmail"));

        else if (!email.matches("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}|()"))
            throw new InvalidValueException(Localisation.getValuePL("incorrectEmail"));

        else
            this.email = email;
    }

    @Override
    public abstract T getItem(String uniqueKey) throws InvalidValueException;

    @Override
    public abstract AbstractProfile copy();
}
