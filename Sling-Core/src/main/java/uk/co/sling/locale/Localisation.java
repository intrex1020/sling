package uk.co.sling.locale;

import static java.util.ResourceBundle.getBundle;

/**
 * Provides values from localisation file
 *
 * @author sebastian wojtkowiak
 */
public final class Localisation
{

    private Localisation()
    {
    }

    /**
     * <p>Looks for the resource mapped to the
     * given key and returns the resource value.</p>
     *
     * @param key of the value to return
     * @return resource value
     */
    public static String getValuePL(String key)
    {
        return getBundle("locale").getString(key);
    }
}
