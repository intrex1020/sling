package uk.co.sling.errorloging;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import uk.co.sling.local_date.LocalDateTimeEUFormat;

/**
 * <p>Saves a stack trace of the {@link Exception} and the
 * message to a file.<br>
 *
 * If the file doesn't exist it is created and the
 * exception's information are written.<br>
 *
 * The filename is generated using {@link LocalDateTimeEUFormat}
 * thus the file format will be hh-mm-ss-dd-mm-yyyy.</p>
 *
 * @author sebastian wojtkowiak
 */
public final class WriteErrorLog
{
    private final static String LOG_DIR_NAME = "errorLogs";
    private final static File LOG_DIR = new File(LOG_DIR_NAME);
    private final static String LOG_NAME = LocalDateTimeEUFormat.now().toStringWithTime();
    private final static File LOG_FILE = new File(LOG_DIR + "/" + LOG_NAME);

    /**
     * <p>Writes the {@link Exception}'s information into a file.</p>
     *
     * @param ex to write
     * @return {@code true} if log written successfully
     */
    public static boolean writeErrorLog(Exception ex)
    {
        WriteErrorLog writer = new WriteErrorLog();

        return writer.createLogDir() && writer.writeLog(ex);

    }

    private boolean createLogDir()
    {
        return LOG_DIR.exists() || LOG_DIR.mkdir();

    }

    private boolean writeLog(Exception e)
    {
        try
        {
            if (!LOG_FILE.exists())
            {
                LOG_FILE.getParentFile().mkdirs();
                LOG_FILE.createNewFile();
            }

            if (LOG_FILE.exists())
            {
                try (PrintWriter write = new PrintWriter(LOG_FILE))
                {
                    System.out.println(write);
                    for (StackTraceElement ste : e.getStackTrace())
                        write.println(ste);

                    write.close();

                    return true;
                }
            }
        }
        catch (IOException ex)
        {
            return false;
        }

        return false;
    }
}
