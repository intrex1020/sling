package uk.co.sling.collection_holder;

/**
 * <p>Guarantees that the item will override equals and hashcode method
 * This will allow an item to be compared properly against other items
 * that are held in a container.</p>
 *
 * @author sebastian wojtkowiak
 */
public abstract class AbstractItem
{
	/**
	 * <p>Checks if two Items are equal.
	 * Object equality is defined by a user
	 * on overriding this method.<br>
	 *
	 * Equality means that either both
	 * objects are the same or user defined
	 * condition has been met that defines
	 * equality between two objects.<br>
	 *
	 * Equality means both objects are equal,
	 * however it doesn't mean they are
	 * the same.<br>
	 *
	 * While overriding this method you should remember
	 * to override the {@link AbstractItem#hashCode()} method.<br>
	 *
	 * If both object are equal the generated hashcode
	 * should be the same.</p>
	 *
	 * @param item object to compare with
	 * @return boolean. True if objects are equal. False otherwise.
	 */
	public abstract boolean equals(Object item);

	/**
	 * <p>Generates hash code value of
	 * the object.<br>
	 *
	 * The hashcode should be a unique value
	 * of the object that makes it distinct
	 * in a group of objects of the same
	 * type.<br>
	 *
	 * While overriding this method you should
	 * remember to override the {@link AbstractItem#equals(Object)} method.<br>
	 *
	 * If you wish to reuse hashcode of another object like
	 * String. You should remember to multiply that value
	 * using a prime number so there is distinction between
	 * the pure String and the object that does equality
	 * using a String.<br>
	 *
	 * If both objects of the same type return the
	 * the same hashcode value then equality should
	 * also be preserved.<br></p>
	 *
	 * Returns generated Item hash code
	 *
	 * @return generated hash code
	 */
	public abstract int hashCode();
}
