package uk.co.sling.collection_holder;

import java.util.*;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;
import uk.co.sling.locale.Localisation;

/**
 * <p>Provides partial implementation of a holder.<br>
 *
 * Gives a set of functionality that allows the user to get all items from the
 * holder. Adds an item to the holder or removes the item from the
 * holder.<br>
 *
 * {@link AbstractItemWithHolder} is also a {@link AbstractItem}
 *
 * The item held in the holder has to extend {@link AbstractItem}, because
 * the values are stored using a hash set. Therefore equals and hashcode
 * should be implemented to ensure add and remove work properly.<br>
 *
 * It is users' responsibility to implement {@link AbstractItem} item
 * methods properly and ensure the holder can use its functionality to
 * work properly.<br>
 *
 * @author Sebastian Wojtkowiak
 */
public abstract class AbstractItemWithHolder<T extends AbstractItem> extends AbstractItem implements IItemHolder<T>
{
	private Collection<T> itemsCollection = new HashSet<>();

	@Override
	public abstract T getItem(String uniqueKey);

	@Override
	public Collection<T> getAllItems()
	{
		return Collections.unmodifiableCollection(itemsCollection);
	}

	@Override
	public boolean addItem(T item)
	{
		if (item == null)
			throw new InvalidValueException(Localisation.getValuePL("errorAddingItem"));
		else
		{
			removeItem(item);
			return itemsCollection.add(item);
		}
	}

	@Override
	public boolean removeItem(T item)
	{
		if (item == null)
			throw new InvalidValueException(Localisation.getValuePL("nonExistingItem"));

		return itemsCollection.remove(item);
	}

	@Override
	public boolean removeAll()
	{
		if (itemsCollection.isEmpty())
			return false;

		itemsCollection.clear();

		return true;
	}

	@Override
	public abstract boolean equals(Object item);

	@Override
	public abstract int hashCode();
}
