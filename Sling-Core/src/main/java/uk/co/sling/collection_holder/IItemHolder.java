package uk.co.sling.collection_holder;

import java.util.Collection;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.ValueNotSetException;

/**
 * <p>Interface of ItemHolder that guarantees functionality of
 * an {@link IItemHolder}</p>
 *
 * @author Sebastian Wojtkowiak
 * @param <T> - Item type.
 */
public interface IItemHolder<T>
{
    /**
     * <p>Returns a single T from the holder that has specified
     * uniqueKey.<br>
     *
     * The method should never return null value instead should
     * throw an {@link InvalidValueException} if the uniqueKey has
     * not been found in the holder or is null.<br>
     *
     * The unique key should be specified by the user.</p>
     *
     * @param uniqueKey of an item, to identify the item
     * @return copy T object
     * @throws InvalidValueException item not found or key is null
     */
    T getItem(String uniqueKey);

    /**
     * <p>Returns all items that are held in the holder.<br>
     *
     * The returned {@link Collection} is set as unmodifiable using
     * {@link java.util.Collections#unmodifiableCollection(Collection)}
     * and thus if any operation is made directly on the {@link Collection}
     * an {@link UnsupportedOperationException} will be thrown. All operations
     * should be made using the {@link IItemHolder} class or any class
     * that implements {@link IItemHolder} class.<br>
     *
     * If the {@link IItemHolder} is empty then and empty
     * {@link Collection} should be returned.<br>
     *
     * This function should guarantee that null will never
     * be returned.
     *
     * @return collection of T
     */
    Collection<T> getAllItems();

    /**
     * <p>Adds an item to the holder.<br>
     *
     * If the item that is being add already exists
     * in the holder all of it's data will be replaced
     * with the new one.<br>
     *
     * If the item to add is a null then {@link InvalidValueException}
     * will be thrown.</p>
     *
     * @param item to add
     * @return {@code true} if success, {@code false} if fail
     * @throws InvalidValueException if item is null
     */
    boolean addItem(T item);

    /**
     * <p>Removes the item from the holder.<br>
     *
     * If the item passed is a null value {@link InvalidValueException}
     * will be thrown. In other cases a boolean value
     * will be returned to indicate whether the value
     * was removed successfully or not.<br>
     *
     * If the value doesn't exist in the holder a {@code false}
     * boolean will be returned</p>
     *
     * @param item to remove
     * @return true if success, false if fail
     * @throws InvalidValueException if item doesn't exist
     */
    boolean removeItem(T item);

    /**
     * <p>Removes all items from the holder.<br>
     *
     * If the holder is already empty a {@code false}
     * boolean will be returned</p>
     *
     * @return true if success, false if fail
     */
    boolean removeAll();
}
