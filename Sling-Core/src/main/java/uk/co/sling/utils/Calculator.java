package uk.co.sling.utils;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.locale.Localisation;

/**
 * Contains basic calculations for VAT amount and prices with and without VAT
 *
 * @author sebastian wojtkowiak
 */
public class Calculator
{
	/**
	 * <p>Calculates amount of VAT in the price.</p>
	 *
	 * @param valueWithoutVAT value without vat
	 * @param valueWithVAT value with vat
	 * @return amount of VAT in the price
	 * @throws InvalidValueException if the value is negative, or value without VAT is greater than value with VAT
	 */
	public static double calculateVatAmount(double valueWithoutVAT, double valueWithVAT)
	{
		if (valueWithoutVAT < 0 || valueWithVAT < 0)
			throw new InvalidValueException(Localisation.getValuePL("invalidNegValue"));

		if (valueWithoutVAT > valueWithVAT)
			throw new InvalidValueException(Localisation.getValuePL("invalidWithoutVATGreater"));

		return roundToMaximum2Dec(valueWithVAT - valueWithoutVAT);
	}

	/**
	 * <p>Calculates total value of all items together.<br>
	 *  The returned value doesn't contain VAT in it.
	 *  To get the VAT amount of the total value call
	 *  {@link Calculator#calculateVatAmount(double, double)}</p>
	 *
	 * @param itemAmount amount of items
	 * @param priceOfItem price per item
	 * @return total price of all items
	 * @throws InvalidValueException if item or price is negative
	 */
	public static double calculateTotalPrice(double itemAmount, double priceOfItem)
	{
		if (itemAmount < 0 || priceOfItem < 0)
			throw new InvalidValueException(Localisation.getValuePL("invalidNegValue"));

		return roundToMaximum2Dec(itemAmount * priceOfItem);
	}

	/**
	 * <p>Calculates price with VAT.<br>
	 * The function assumes that 1 is equivalent of 1 percent
	 * thus 0.1 will be read as 0.1%.<br></p>
	 *
	 * @param vatPercentage vat in percentage.
	 * @param valueWithoutVAT price without VAT
	 * @return price including VAT
	 * @throws InvalidValueException if value with VAT is negative
	 */
	public static double calculateValueWithVat(double vatPercentage, double valueWithoutVAT)
	{
		if (valueWithoutVAT < 0)
			throw new InvalidValueException(Localisation.getValuePL("invalidNegValue"));

		return roundToMaximum2Dec(((vatPercentage / 100.0) * valueWithoutVAT) + valueWithoutVAT);
	}

	/**
	 * <p>Rounds the double to 2 decimal places</p>
	 *
	 * @param toRound number to round
	 * @return value with 2 decimal places
	 */
	public static double roundToMaximum2Dec(double toRound)
	{
		return Math.round(toRound * 100.0) / 100.0;
	}
}