package uk.co.sling.utils;

/**
 * <p>Contains a set of helper functions</p>
 *
 * @author Sebastian Wojtkowiak
 */
public class Utils
{
	/**
	 * <p>Checks if the field starts or ends
	 * with a white space character.</p>
	 *
	 * @param field to check
	 * @return @{code true} if field starts or ends with white character
	 */
	public static boolean isFieldNotValid(String field)
	{
		return field.matches("(\\s+)|(\\s.+)|(.+\\s)");
	}
}
