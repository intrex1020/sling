package uk.co.sling.utils;

import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.locale.Localisation;

/**
 * <p> Contains all payments types.</p>
 *
 * @author sebastian wojtkowiak
 */
public enum PaymentType
{
    BANK_TRANSFER(Localisation.getValuePL("bankTransfer")),
    CASH(Localisation.getValuePL("cash")),
    PAID(Localisation.getValuePL("paid"));

    private final String paymentType;

    PaymentType(String type)
    {
        paymentType = type;
    }

    /**
     * <p>Generates an enum from the given string.<br>
     * If the equivalent enum is not found and
     * {@link InvalidValueException} is thrown.</p>
     *
     * @param paymentType to convert into enum
     * @return equivalent PaymentType enum
     * @throws InvalidValueException if the enum is not found
     */
    public static PaymentType fromString(String paymentType)
    {
        for (PaymentType pt : PaymentType.values())
        {
            if (pt.paymentType.equals(paymentType))
                return pt;
        }

        throw new InvalidValueException(Localisation.getValuePL("invalidPaymentType"));
    }

    @Override
    public String toString()
    {
        return paymentType;
    }
}
