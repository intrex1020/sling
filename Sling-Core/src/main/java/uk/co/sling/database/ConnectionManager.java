package uk.co.sling.database;

import org.sqlite.SQLiteConfig;
import uk.co.sling.exceptions.NotConnectedException;

import java.io.File;
import java.io.IOException;
import java.sql.*;

/**
 * Provides functionality to connect and disconnect from/to database.
 * The connection is auto closeable so the disconnect can be omitted.
 * By default the commit is called on close.
 *
 * @author sebastian wojtkowiak
 */
final class ConnectionManager implements AutoCloseable
{
	//for manual testing
	//private static final String DB_DIR_NAME = "/home/intrex/Desktop";
	private final static String DB_DIR_NAME = "data";
	private final static String DB_FILE = "data.db";

	private static ConnectionManager conManager;
	private Connection con;

	/**
	 * Initialises the manager
	 *
	 * @param con sql connection
	 * @throws SQLException if couldn't set auto commit to false
	 */
	private ConnectionManager(Connection con) throws SQLException
	{
		this.con = con;

		con.setAutoCommit(false);
	}

	/**
	 * Opens a connection with the database and
	 * return manager to manage the database.
	 *
	 * @return ConnectionManager
	 * @throws SQLException  if a database access error occurs or the url is null, or the connection timeouts
	 * @throws ClassNotFoundException if the JDBC driver not found
	 */
	static ConnectionManager getConnectionInstance() throws SQLException, ClassNotFoundException
	{
		if (conManager == null)
		{
			File db = new File(DB_DIR_NAME+"/"+DB_FILE);

			try
			{
				if (!db.exists())
				{
					db.getParentFile().mkdirs();
					db.createNewFile();
				}

				if (db.exists())
				{
					Class.forName("org.sqlite.JDBC");
					SQLiteConfig cfg = new SQLiteConfig();
					cfg.enforceForeignKeys(true);

					conManager = new ConnectionManager(
							DriverManager.getConnection("jdbc:sqlite:" + DB_DIR_NAME + "/" + DB_FILE, cfg.toProperties()));

					return conManager;
				}
			}
			catch (IOException ignored) {}

			throw new SQLException("Error connecting to database");
		}
		else
		{
			return conManager;
		}
	}

	/**
	 * Closes the connection with the database
	 *
	 * @return true if connection closed successfully
	 * @throws SQLException if a database access error occurs
	 */
	public void disconnect() throws SQLException
	{
		con.commit();
		con.close();

		if (con.isClosed())
		{
			con = null;
			conManager = null;

		}

	}

	/**
	 * Returns prepared statement with query
	 *
	 * @param sql query
	 * @return prepared statement to execute
	 * @throws SQLException if a database access error occurs or this method is called on a closed connection
	 */
	public PreparedStatement getStatement(String sql) throws SQLException
	{
		return con.prepareStatement(sql);
	}

	/**
	 * Returns created blob
	 *
	 * @return blob
	 * @throws SQLException if a database access error occurs or this method is called on a closed connection
	 */
	public Blob getBlob() throws SQLException
	{
		return con.createBlob();
	}

	/**
	 * Provides a way to commit data if auto commit is set as false
	 *
	 * @throws SQLException if a database access error occurs, this method is called while participating
	 * in a distributed transaction, if this method is called on a closed connection or this Connection
	 * object is in auto-commit mode
	 */
	public void commit() throws SQLException
	{
		con.commit();
	}

	/**
	 * Rollbacks all transactions that are not committed
	 *
	 * @throws SQLException if a database access error occurs, this method is called while participating in a
	 * distributed transaction, this method is called on a closed connection or this Connection object
	 * is in auto-commit mode
	 */
	public void rollback() throws SQLException
	{
		con.rollback();
	}

	/**
	 * <p>Checks if the given table exists in the database</p>
	 *
	 * @param tableName to check
	 * @return true if table exists
	 * @throws SQLException if a database access error occurs, this method is called while participating in a
	 * distributed transaction, this method is called on a closed connection or this Connection object
	 * is in auto-commit mode
	 */
	public boolean isExistingTable(String tableName) throws SQLException
	{
		DatabaseMetaData dbm = con.getMetaData();
		ResultSet rs = dbm.getTables(null, null, tableName, null);

		return rs.next();
	}

	@Override
	public void close() throws SQLException
	{
		disconnect();
	}
}
