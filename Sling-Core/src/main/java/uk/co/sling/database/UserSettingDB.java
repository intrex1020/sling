package uk.co.sling.database;

import uk.co.sling.containers.Setting;
import uk.co.sling.containers.User;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.SQLFetchException;
import uk.co.sling.exceptions.ValueNotSetException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Provides functionality to interact with the database.
 * Allows to add and retrieve of a user or invoice settings.
 *
 * @author sebastian wojtkowiak
 */
public class UserSettingDB
{
	static final String TABLE_NAME = "UserSetting";
	static final String CREATE_TABLE =
			"CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " "+
					"(" +
						"userName TEXT NOT NULL," +
						"accountNumber TEXT NOT NULL," +
						"bankName TEXT NOT NULL," +
						"waterMark Blob," +
						"PRIMARY KEY(userName)," +
						"FOREIGN KEY(userName) REFERENCES User(userName) " +
						"ON UPDATE CASCADE ON DELETE CASCADE" +
					");";
	static final String INSERT_INTO_TABLE = "" +
			"INSERT INTO " + TABLE_NAME + " "+
				"(userName, accountNumber, bankName, waterMark)" +
			"VALUES " +
				"(?, ?, ?, ?)";
	static final String DELETE_SETTING = "" +
			"DELETE FROM " + TABLE_NAME + " " +
			"WHERE " +
				"userName = ? ";
	static final String SELECT_ALL_SETTING_OF_USER = "" +
			"SELECT * FROM " + TABLE_NAME + " " +
			"WHERE " +
				"userName = ?";


	/**
	 * Adds new setting for the user to the database
	 *
	 * @param user to that has the setting
	 * @param setting setting to save
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void insert(User user, Setting setting) throws SQLException, ClassNotFoundException
	{
		if (user == null || setting == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					cm.getStatement(CREATE_TABLE).execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}

			try
			{
				remove(cm, user);

				PreparedStatement stmt = cm.getStatement(INSERT_INTO_TABLE);

				stmt.setString(1, user.getUserName());
				stmt.setString(2, setting.getAccountNumber());
				stmt.setString(3, setting.getBankName());

				Blob blob;
				/* TODO: not supported by JDBC4
				if ((blob = convertImageToBlob(cm.getBlob(), setting.getWaterMark())) != null)
					stmt.setBlob(4, blob);
				else
					stmt.setBlob(4, cm.getBlob());*/

				stmt.execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	private static void remove(ConnectionManager cm, User user) throws SQLException
	{
		if (cm == null || user == null)
			return;

		try
		{
			PreparedStatement stmt = cm.getStatement(DELETE_SETTING);

			stmt.setString(1, user.getUserName());

			stmt.execute();
		}
		catch (SQLException sqle)
		{
			cm.rollback();
			throw sqle;
		}
	}

	/**
	 * Get a setting from the database.
	 *
	 * @param user to fetch setting for
	 * @return setting
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if the driver doesn't exist
	 * @throws ValueNotSetException fetched values are empty
	 * @throws InvalidValueException fetched values are invalid
	 */
	public static Setting getSetting(User user) throws SQLException, ClassNotFoundException, SQLFetchException
	{
		if (user == null)
			throw new ValueNotSetException("Fetching setting for null user");

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			if (!cm.isExistingTable(TABLE_NAME))
				throw new SQLFetchException("Fetching from table that doesn't exist");

			PreparedStatement stmt = cm.getStatement(SELECT_ALL_SETTING_OF_USER);

			stmt.setString(1, user.getUserName());

			ResultSet rs = stmt.executeQuery();

			if (rs.next())
			{
				return new Setting.SettingBuilder()
						.setAccountNumber(rs.getString("accountNumber"))
						.setBankName(rs.getString("bankName"))
						//.setWaterMark(convertBlobToImage(rs.getBlob("waterMark"))) //Not supported by JDBC4 driver
						.buildSetting();
			}
			else
				throw new SQLException("Error reading setting from the database");
		}
	}

	/**
	 * Writes image to blob.
	 *
	 * @param blob to write to
	 * @param image to write
	 * @return Blob if success. Otherwise null will be returned
	 * @throws SQLException on connection issues
	 */
	private static Blob convertImageToBlob(Blob blob, Image image) throws SQLException
	{
		if (image == null)
			return null;

		BufferedImage bi = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2 = bi.createGraphics();
		g2.drawImage(image, 0, 0, null);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try
		{
			ImageIO.write(bi, "jpg", baos);
			blob.setBytes(1, baos.toByteArray());
			return blob;
		}
		catch(IOException ioe)
		{
			return null;
		}
		finally
		{
			g2.dispose();
		}
	}

	/**
	 * Converts blob to image
	 *
	 * @param blob to convert
	 * @return Image if success. Otherwise null
	 * @throws SQLException on connection issues
	 */
	private static Image convertBlobToImage(Blob blob) throws SQLException
	{
		try
		{
			BufferedImage bi = ImageIO.read(blob.getBinaryStream());

			return bi;
		}
		catch(IOException ioe)
		{
			return null;
		}
	}
}
