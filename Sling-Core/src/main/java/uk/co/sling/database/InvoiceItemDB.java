package uk.co.sling.database;

import uk.co.sling.containers.*;
import uk.co.sling.exceptions.ValueNotSetException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Provides functionality to interact with the database.
 * Allows to add, modify and removeAll invoice item.
 *
 * @author sebastian wojtkowiak
 */
public class InvoiceItemDB
{
	static final String TABLE_NAME = "InvoiceItem";
	static final String CREATE_TABLE =
			"CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " "+
					"(" +
						"invoiceCode TEXT NOT NULL," +
						"userName TEXT NOT NULL," +
						"indx INTEGER UNSIGNED NOT NULL," +
						"description TEXT NOT NULL," +
						"amount DOUBLE UNSIGNED  NOT NULL," +
						"measurementUnit TEXT NOT NULL," +
						"vatPercentage DOUBLE UNSIGNED UNSIGNED NOT NULL," +
						"priceNetto DOUBLE UNSIGNED NOT NULL," +
						"PRIMARY KEY(userName, invoiceCode, indx)," +
						"FOREIGN KEY(userName, invoiceCode) REFERENCES Invoice(userName, invoiceCode) " +
						"ON UPDATE CASCADE ON DELETE CASCADE" +
					");";
	static final String INSERT_INTO_TABLE = "" +
			"INSERT INTO " + TABLE_NAME + " "+
			"(invoiceCode, userName, indx, description, amount, measurementUnit, vatPercentage, priceNetto)" +
			"VALUES " +
			"(?, ?, ?, ?, ?, ?, ?, ?)";
	static final String DELETE_FROM_TABLE = "" +
			"DELETE FROM " + TABLE_NAME + " "+
			"WHERE " +
				"userName = ? " +
			"AND " +
				"invoiceCode = ? ";
	static final String SELECT_ALL_ITEMS = "" +
			"SELECT * FROM " + TABLE_NAME + " " +
			"WHERE " +
				"userName = ? " +
			"AND " +
				"invoiceCode = ?";

	/**
	 * <p>Given the instance of a {@link Invoice} it saves its {@link InvoiceItem}s details into the
	 * database.<br>
	 * If the passed value is null the function will immediately terminate and no action
	 * of insert will be attempted.<br>
	 * The connection with the database is passed as first argument and won't be
	 * closed by this function. It is the caller responsibility to terminate the conncetion.<br>
	 * If the database does not contain a table an attempt will be made to create such, on
	 * creation failure {@link SQLException} will be thrown to indicate that a failure
	 * has occurred.<br>
	 * Upon the successful creation of the table an attempt of saving the state of {@link InvoiceItem}s
	 * will be made. On failure rollback will be performed to the last consistent state
	 * of the database and {@link SQLException} will be thrown<br>
	 * The {@link ValueNotSetException} will be always thrown when the invoice is a null value reference.<br></p>
	 *
	 * @param invoice of which items to save
	 * @throws SQLException on connection issues
	 */
	static void insert(ConnectionManager cm, Invoice invoice) throws SQLException
	{
		if (invoice == null || cm == null)
			return;

		try
		{
			if (!cm.isExistingTable(TABLE_NAME))
				cm.getStatement(CREATE_TABLE).execute();
		}
		catch (SQLException sqle)
		{
			cm.rollback();
			throw sqle;
		}

		try
		{
			PreparedStatement stmt = cm.getStatement(INSERT_INTO_TABLE);

			for (InvoiceItem it : invoice.getAllItems())
			{
				stmt.setString(1, invoice.getInvoiceUniqueCode());
				stmt.setString(2, invoice.getIssuedBy().getUserName());
				stmt.setInt(3, it.getItemIndex());
				stmt.setString(4, it.getItemDescription());
				stmt.setDouble(5, it.getItemAmount());
				stmt.setString(6, it.getMeasurementUnit());
				stmt.setDouble(7, it.getVatPercentage());
				stmt.setDouble(8, it.getPriceNetto());

				stmt.execute();
			}
		}
		catch(SQLException sqle)
		{
			cm.rollback();
			throw sqle;
		}
	}

	/**
	 * <p>Given the instance of a {@link Invoice} it updates its {@link InvoiceItem} details in the
	 * database.<br>
	 * If the value of cm or invoice is {@code null} or table doesn't exist the function immediately exits and
	 * performs no operation.<br>
	 * The connection with the database is automatically managed by the function, and
	 * should never be closed/opened manually to ensure correct behaviour of the
	 * function.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br>
	 *
	 * @param cm connection manager
	 * @param invoice to update
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	static void update(ConnectionManager cm, Invoice invoice) throws SQLException
	{
		//This will be much easier/quicker as number of items
		//can change anytime. Instead of checking if item is present
		//in the database just removeAll and re-insert all of them.
		//Definitely less bug prone.
		removeAll(cm, invoice);
		insert(cm, invoice);
	}

	/**
	 * <p>Performs {@link InvoiceItem} deletion.<br>
	 * A backup of the data is not performed by this function. It is user decision if the backup
	 * should be created.<br>
	 * If the cm or invoice value is {@code null} or the table is not yet created the function exits without raising
	 * any errors.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br></p>
	 *
	 * @param cm connection manager
	 * @param invoice to removeAll
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	private static void removeAll(ConnectionManager cm, Invoice invoice) throws SQLException
	{
		if (cm == null | invoice == null)
			return;

		try
		{
			if (!cm.isExistingTable(TABLE_NAME))
				return;

			PreparedStatement stmt = cm.getStatement(DELETE_FROM_TABLE);

			stmt.setString(1, invoice.getIssuedBy().getUserName());
			stmt.setString(2, invoice.getInvoiceUniqueCode());

			stmt.execute();
		}
		catch (SQLException sqle)
		{
			cm.rollback();
			throw sqle;
		}
	}

	/**
	 * <p>Given the {@link Invoice} it populates it with {@link InvoiceItem}.<br>
	 * If the {@link Invoice} has no {@link InvoiceItem} it won't be populated.<br>
	 * If the invoice value, cm value is null or the table doesn't exist the function will immediately without
	 * raising any errors.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br>
	 * </p>
	 *
	 * @param cm connection manager
	 * @param invoice invoice to populate
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if the driver doesn't exist
	 */
	static void getItems(ConnectionManager cm, Invoice invoice) throws SQLException
	{
		if (cm == null || invoice == null)
			return;

		try
		{
			PreparedStatement stmt = cm.getStatement(SELECT_ALL_ITEMS);

			stmt.setString(1, invoice.getIssuedBy().getUserName());
			stmt.setString(2, invoice.getInvoiceUniqueCode());

			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				invoice.addItem(new InvoiceItem.InvoiceItemBuilder()
					.setItemIndex(rs.getInt("indx"))
					.setItemDescription(rs.getString("description"))
					.setItemAmount(rs.getDouble("amount"))
					.setMeasurementUnit(rs.getString("measurementUnit"))
					.setVatPercentage(rs.getDouble("vatPercentage"))
					.setPriceNetto(rs.getDouble("priceNetto"))
					.buildInvoiceItem());
			}
		}
		catch (SQLException sqle)
		{
			cm.rollback();
			throw sqle;
		}
	}
}
