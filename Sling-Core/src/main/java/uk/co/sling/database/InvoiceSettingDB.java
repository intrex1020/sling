package uk.co.sling.database;

import uk.co.sling.containers.Invoice;
import uk.co.sling.containers.Setting;
import uk.co.sling.containers.User;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.SQLFetchException;
import uk.co.sling.exceptions.ValueNotSetException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Provides functionality to interact with the database.
 * Allows to add and retrieve of a user or invoice settings.
 *
 * @author sebastian wojtkowiak
 */
public class InvoiceSettingDB
{
	static final String TABLE_NAME = "InvoiceSetting";
	static final String CREATE_TABLE =
			"CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " "+
					"(" +
						"invoiceCode TEXT NOT NULL," +
						"userName TEXT NOT NULL," +
						"accountNumber TEXT NOT NULL," +
						"bankName TEXT NOT NULL," +
						"waterMark Blob," +
						"PRIMARY KEY(userName, invoiceCode)," +
						"FOREIGN KEY(userName, invoiceCode) REFERENCES Invoice(userName, invoiceCode) " +
						"ON UPDATE CASCADE ON DELETE CASCADE" +
					");";
	static final String INSERT_INTO_TABLE = "" +
			"INSERT INTO " + TABLE_NAME + " "+
				"(invoiceCode, userName, accountNumber, bankName, waterMark)" +
			"VALUES " +
				"(?, ?, ?, ?, ?)";
	static final String UPDATE_SETTING = "" +
			"UPDATE " + TABLE_NAME + " " +
			"SET " +
				"accountNumber = ?," +
				"bankName = ?," +
				"waterMark = ? " +
			"WHERE " +
				"invoiceCode = ? " +
			"AND " +
				"userName = ?";
	static final String SELECT_SETTING_OF_INVOICE = "" +
			"SELECT * FROM " + TABLE_NAME + " " +
			"WHERE " +
				"invoiceCode = ? " +
			"AND " +
				"userName = ?";


	/**
	 * Adds new setting for the user to the database
	 *
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void insert(ConnectionManager cm, Invoice invoice) throws SQLException, ClassNotFoundException
	{
		if (cm == null || invoice == null)
			return;

		try
		{
			if (!cm.isExistingTable(TABLE_NAME))
				cm.getStatement(CREATE_TABLE).execute();
		}
		catch (SQLException sqle)
		{
			cm.rollback();
			throw sqle;
		}

		try
		{
			PreparedStatement stmt = cm.getStatement(INSERT_INTO_TABLE);

			stmt.setString(1, invoice.getInvoiceUniqueCode());
			stmt.setString(2, invoice.getIssuedBy().getUserName());
			stmt.setString(3, invoice.getSetting().getAccountNumber());
			stmt.setString(4, invoice.getSetting().getBankName());

			Blob blob;
			/*TODO: blob is unsupported by JDBC4 driver
			if ((blob = convertImageToBlob(cm.getBlob(), invoice.getSetting().getWaterMark())) != null)
				stmt.setBlob(4, blob);
			else
				stmt.setBlob(4, cm.getBlob());*/

			stmt.execute();
		}
		catch (SQLException sqle)
		{
			cm.rollback();
			throw sqle;
		}
	}

	public static void update(ConnectionManager cm, Invoice invoice) throws SQLException
	{
		if (cm == null || invoice == null)
			return;

		try
		{
			if (!cm.isExistingTable(TABLE_NAME))
				return;

			PreparedStatement stmt = cm.getStatement(UPDATE_SETTING);

			stmt.setString(1, invoice.getSetting().getAccountNumber());
			stmt.setString(2, invoice.getSetting().getBankName());

			Blob blob;
			/* TODO: feature not supported by JDBC4 driver
			if ((blob = convertImageToBlob(cm.getBlob(), invoice.getSetting().getWaterMark())) != null)
				stmt.setBlob(3, blob);
			else
				stmt.setBlob(3, cm.getBlob());*/
			//where clause
			stmt.setString(4, invoice.getInvoiceUniqueCode());
			stmt.setString(5, invoice.getIssuedBy().getUserName());

			stmt.execute();
		}
		catch (SQLException sqle)
		{
			cm.rollback();
			throw sqle;
		}
	}

	/**
	 * Get a setting from the database.
	 *
	 * @return setting
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if the driver doesn't exist
	 * @throws ValueNotSetException fetched values are empty
	 * @throws InvalidValueException fetched values are invalid
	 */
	public static Setting getSetting(ConnectionManager cm, User user, String invoiceCode) throws SQLException, ClassNotFoundException, SQLFetchException
	{
		if (cm == null || invoiceCode == null || user == null)
			throw new ValueNotSetException("Connection manager or invoice code is null");

		if (!cm.isExistingTable(TABLE_NAME))
			throw new SQLFetchException("Can't fetch from non-existing table");

		PreparedStatement stmt = cm.getStatement(SELECT_SETTING_OF_INVOICE);

		stmt.setString(1, invoiceCode);
		stmt.setString(2, user.getUserName());

		ResultSet rs = stmt.executeQuery();

		if (rs.next())
		{
			return new Setting.SettingBuilder()
					.setAccountNumber(rs.getString("accountNumber"))
					.setBankName(rs.getString("bankName"))
					//.setWaterMark(convertBlobToImage(rs.getBlob("waterMark"))) //not supported by JDBC4 driver
					.buildSetting();
		}
		else
			throw new SQLException("Error reading setting from the database");
	}

	/**
	 * Writes image to blob.
	 *
	 * @param blob to write to
	 * @param image to write
	 * @return Blob if success. Otherwise null will be returned
	 * @throws SQLException on connection issues
	 */
	private static Blob convertImageToBlob(Blob blob, Image image) throws SQLException
	{
		if (image == null)
			return null;

		BufferedImage bi = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2 = bi.createGraphics();
		g2.drawImage(image, 0, 0, null);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try
		{
			ImageIO.write(bi, "jpg", baos);
			blob.setBytes(1, baos.toByteArray());
			return blob;
		}
		catch(IOException ioe)
		{
			return null;
		}
		finally
		{
			g2.dispose();
		}
	}

	/**
	 * Converts blob to image
	 *
	 * @param blob to convert
	 * @return Image if success. Otherwise null
	 * @throws SQLException on connection issues
	 */
	private static Image convertBlobToImage(Blob blob) throws SQLException
	{
		try
		{
			BufferedImage bi = ImageIO.read(blob.getBinaryStream());

			return bi;
		}
		catch(IOException ioe)
		{
			return null;
		}
	}
}
