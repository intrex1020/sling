package uk.co.sling.database;


import uk.co.sling.containers.User;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.SQLFetchException;
import uk.co.sling.exceptions.ValueNotSetException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Provides functionality to interact with the database.
 * Allows to add, modify and remove user.
 *
 * @author sebastian wojtkowiak
 */
public final class UserDB
{
	static final String TABLE_NAME = "User";
	static final String CREATE_TABLE =
			"CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " "+
					"(" +
						"userName TEXT NOT NULL," +
						"companyName TEXT NOT NULL," +
						"vatCode TEXT NOT NULL," +
						"townName TEXT NOT NULL," +
						"postCode TEXT NOT NULL," +
						"streetName TEXT NOT NULL," +
						"buildingNumber TEXT NOT NULL," +
						"phoneNumber TEXT NOT NULL," +
						"emailAddress TEXT NOT NULL," +
						"PRIMARY KEY(userName)" +
					");";
	static final String INSERT_INTO_TABLE = "" +
			"INSERT INTO " + TABLE_NAME + " "+
				"(userName, companyName, vatCode, townName, postCode, streetName, buildingNumber, phoneNumber, emailAddress)" +
			"VALUES " +
				"(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	static final String UPDATE_TABLE = "" +
			"UPDATE " + TABLE_NAME + " "+
			"SET " +
				"userName = ?," +
				"companyName = ?," +
				"vatCode = ?," +
				"townName = ?," +
				"postCode = ?," +
				"streetName = ?," +
				"buildingNumber = ?," +
				"phoneNumber = ?," +
				"emailAddress = ? " +
			"WHERE " +
				"userName = ?";
	static final String DELETE_FROM_TABLE = "" +
			"DELETE FROM " + TABLE_NAME + " "+
			"WHERE " +
				"userName = ?";
	static final String SELECT_ALL_USERS = "" +
			"SELECT * FROM " + TABLE_NAME;

	/**
	 * <p>Given the instance of a {@link User} it saves its details into the
	 * database.<br>
	 * If the passed value is null the function will immediately terminate and no action
	 * of insert will be attempted.<br>
	 * The connection with the database is automatically managed by the function, and
	 * should never be closed/opened manually to ensure correct behaviour of the
	 * function.<br>
	 * If the database do not contain a table an attempt will be made to create such, on
	 * creation failure {@link SQLException} will be thrown to indicate that a failure
	 * has occurred.<br>
	 * Upon the successful creation of the table an attempt of saving the state of {@link User}
	 * will be made. On failure rollback will be performed to the last consistent state
	 * of the database and {@link SQLException} will be thrown<br></p>
	 *
	 * @param user to add
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void insert(User user) throws SQLException, ClassNotFoundException, ValueNotSetException
	{
		if (user == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					cm.getStatement(CREATE_TABLE).execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}

			try
			{
				PreparedStatement stmt = cm.getStatement(INSERT_INTO_TABLE);

				stmt.setString(1, user.getUserName());
				stmt.setString(2, user.getCompanyName());
				stmt.setString(3, user.getVatCode());
				stmt.setString(4, user.getTown());
				stmt.setString(5, user.getPostCode());
				stmt.setString(6, user.getStreet());
				stmt.setString(7, user.getBuildingNumber());
				stmt.setString(8, user.getPhone());
				stmt.setString(9, user.getEmail());

				stmt.execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Given the instance of a {@link User} it updates its details in the
	 * database.<br>
	 * If the value of user is {@code null} or table doesn't exist the function immediately exits and
	 * performs no operation.
	 * The value of newUser is optional and should
	 * only be provided if the primaryKey {@link User#userName} is to be updated<br>
	 * Updating an non-existing entry within the database will have no effect and will
	 * always succeed, and the database state won't be changed.<br>
	 * The connection with the database is automatically managed by the function, and
	 * should never be closed/opened manually to ensure correct behaviour of the
	 * function.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br>
	 * The {@link ValueNotSetException} will be always thrown when the user is a null value reference.<br>
	 * On unsuccessful update rollback will be performed and the entry won't be updated.</p>
	 *
	 * @param user to update
	 * @param newUser optional parameter to update username
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void update(User user, User newUser) throws SQLException, ClassNotFoundException
	{
		if (user == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					return;

				PreparedStatement stmt = cm.getStatement(UPDATE_TABLE);

				if (newUser == null)
				{
					stmt.setString(1, user.getUserName());
					stmt.setString(2, user.getCompanyName());
					stmt.setString(3, user.getVatCode());
					stmt.setString(4, user.getTown());
					stmt.setString(5, user.getPostCode());
					stmt.setString(6, user.getStreet());
					stmt.setString(7, user.getBuildingNumber());
					stmt.setString(8, user.getPhone());
					stmt.setString(9, user.getEmail());
					//where clause
					stmt.setString(10, user.getUserName());
				}
				else
				{
					stmt.setString(1, newUser.getUserName());
					stmt.setString(2, newUser.getCompanyName());
					stmt.setString(3, newUser.getVatCode());
					stmt.setString(4, newUser.getTown());
					stmt.setString(5, newUser.getPostCode());
					stmt.setString(6, newUser.getStreet());
					stmt.setString(7, newUser.getBuildingNumber());
					stmt.setString(8, newUser.getPhone());
					stmt.setString(9, newUser.getEmail());
					//where clause
					stmt.setString(10, user.getUserName());
				}

				stmt.execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Performs {@link User} deletion and all associated data with that user. Which
	 * means all {@link uk.co.sling.containers.Setting}, {@link uk.co.sling.containers.Contractor},
	 * {@link uk.co.sling.containers.Invoice} and {@link uk.co.sling.containers.InvoiceItem} entries
	 * will be removed from the database.<br>
	 * If the user value is null or the table is not yet created the function exits without raising
	 * any errors.<br>
	 * A backup of the data is not performed by this function. It is user decision if the backup
	 * should be created.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br>
	 * </p>
	 *
	 * @param user to remove
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void remove(User user) throws SQLException, ClassNotFoundException
	{
		if (user == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					return;

				PreparedStatement stmt = cm.getStatement(DELETE_FROM_TABLE);

				stmt.setString(1, user.getUserName());

				stmt.execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Retrieves a {@link List} of all {@link User} from the database.<br>
	 * The returned {@link List} contains all users within the database. If the
	 * database is empty then empty list will be returned instead.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br>
	 * </p>
	 *
	 * @return list of users
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if the driver doesn't exist
	 */
	public static List<User> getUsers() throws SQLException, ClassNotFoundException
	{
		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			List<User> users = new ArrayList<>();

			if (!cm.isExistingTable(TABLE_NAME))
				return users;

			PreparedStatement stmt = cm.getStatement(SELECT_ALL_USERS);

			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				users.add(new User.UserBuilder()
					.setUserName(rs.getString("userName"))
					.setCompanyName(rs.getString("companyName"))
					.setVatCode(rs.getString("vatCode"))
					.setTown(rs.getString("townName"))
					.setPostCode(rs.getString("postCode"))
					.setStreet(rs.getString("streetName"))
					.setBuildingNumber(rs.getString("buildingNumber"))
					.setPhoneNumber(rs.getString("phoneNumber"))
					.setEmail(rs.getString("emailAddress"))
					.buildUser());
			}

			return Collections.unmodifiableList(users);
		}
	}
}
