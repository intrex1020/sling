package uk.co.sling.database;

import uk.co.sling.containers.*;
import uk.co.sling.exceptions.SQLFetchException;
import uk.co.sling.local_date.LocalDateTimeEUFormat;
import uk.co.sling.utils.PaymentType;

import java.sql.*;

/**
 * Provides functionality to interact with the database.
 * Allows to add, modify and remove invoice.
 *
 * @author sebastian wojtkowiak
 */
public final class InvoiceDB
{
	static final String TABLE_NAME = "Invoice";
	static final String CREATE_TABLE =
			"CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " "+
					"(" +
						"invoiceCode TEXT NOT NULL," +
						"userName TEXT NOT NULL," +
						"vatCode TEXT NOT NULL," +
						"invoiceNumber INTEGER UNSIGNED NOT NULL," +
						"issueDate TEXT NOT NULL," +
						"daysToPay INTEGER UNSIGNED NOT NULL," +
						"isPaid BOOL NOT NULL," +
						"paymentType TEXT NOT NULL," +
						"PRIMARY KEY(userName, invoiceCode)," +
						"FOREIGN KEY(userName, vatCode) REFERENCES Contractor(userName, vatCode) " +
						"ON UPDATE CASCADE ON DELETE CASCADE" +
					");";
	static final String INSERT_INTO_TABLE = "" +
			"INSERT INTO " + TABLE_NAME + " "+
				"(invoiceCode, userName, vatCode, invoiceNumber, issueDate, daysToPay, isPaid, paymentType)" +
			"VALUES " +
				"(?, ?, ?, ?, ?, ?, ?, ?)";
	static final String UPDATE_TABLE = "" +
			"UPDATE " + TABLE_NAME + " "+
			"SET " +
				"invoiceCode = ?," +
				"invoiceNumber = ?," +
				"issueDate = ?," +
				"daysToPay = ?," +
				"isPaid = ?," +
				"paymentType = ? " +
			"WHERE " +
				"invoiceCode = ? " +
			"AND " +
				"userName = ? " +
			"AND " +
				"vatCode = ?";
	static final String DELETE_FROM_TABLE = "" +
			"DELETE FROM " + TABLE_NAME + " "+
			"WHERE " +
				"invoiceCode = ? " +
			"AND " +
				"userName = ? " +
			"AND " +
				"vatCode = ?";
	static final String SELECT_ALL_INVOICES_OF_CONTRACTOR = "" +
			"SELECT * FROM " + TABLE_NAME + " " +
			"WHERE " +
				"userName = ? " +
			"AND " +
				"vatCode = ?";
	static final String SELECT_INVOICE_NUMBER ="" +
			"SELECT MAX(invoiceNumber) FROM " + TABLE_NAME + " " +
			"WHERE " +
				"userName = ? " +
			"AND " +
				"STRFTIME('%Y', issueDate) = ?";

	/**
	 * <p>Given the instance of a {@link Invoice} it saves its details into the
	 * database. The invoice {@link Setting} and {@link uk.co.sling.containers.InvoiceItem} is also
	 * saved as it is part of the {@link Invoice}.<br>
	 * If the passed value is null the function will immediately terminate and no action
	 * of insert will be attempted.<br>
	 * The connection with the database is automatically managed by the function, and
	 * should never be closed/opened manually to ensure correct behaviour of the
	 * function.<br>
	 * If the database does not contain a table an attempt will be made to create such, on
	 * creation failure {@link SQLException} will be thrown to indicate that a failure
	 * has occurred.<br>
	 * Upon the successful creation of the table an attempt of saving the state of {@link Invoice}
	 * will be made. On failure rollback will be performed to the last consistent state
	 * of the database and {@link SQLException} will be thrown<br></p>
	 * @param invoice to add
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void insert(Invoice invoice) throws SQLException, ClassNotFoundException
	{
		if (invoice == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					cm.getStatement(CREATE_TABLE).execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}

			try
			{
				PreparedStatement stmt = cm.getStatement(INSERT_INTO_TABLE);

				stmt.setString(1, invoice.getInvoiceUniqueCode());
				stmt.setString(2, invoice.getIssuedBy().getUserName());
				stmt.setString(3, invoice.getIssuedTo().getVatCode());
				stmt.setInt(4, invoice.getInvoiceNumber());
				stmt.setString(5, invoice.getIssueDate().toStringUSFormat());
				stmt.setInt(6, invoice.getDaysToPay());
				stmt.setBoolean(7, invoice.getIsPaid());
				stmt.setString(8, invoice.getPaymentType().toString());

				stmt.execute();

				InvoiceItemDB.insert(cm, invoice);
				InvoiceSettingDB.insert(cm, invoice);
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Given the instance of a {@link Invoice} it updates its details in the
	 * database. {@link uk.co.sling.containers.InvoiceItem} and {@link Setting} may be
	 * affected if those are different than existing one.<br>
	 * If the value of user or contractor is {@code null} or table doesn't exist the function immediately exits and
	 * performs no operation. The value of newInvoice is optional and should
	 * only be provided if the primaryKey {@link Invoice#invoiceUniqueCode} is to be updated<br>
	 * Updating an non-existing entry within the database will have no effect and will
	 * always succeed, and the database state won't be changed.<br>
	 * The connection with the database is automatically managed by the function, and
	 * should never be closed/opened manually to ensure correct behaviour of the
	 * function.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br></p>

	 * @param invoice to update
	 * @param newInvoice if changing primary key
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void update(Invoice invoice, Invoice newInvoice) throws SQLException, ClassNotFoundException
	{
		if (invoice == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					return;

				PreparedStatement stmt = cm.getStatement(UPDATE_TABLE);

				if (newInvoice == null)
				{
					stmt.setString(1, invoice.getInvoiceUniqueCode());
					stmt.setInt(2, invoice.getInvoiceNumber());
					stmt.setString(3, invoice.getIssueDate().toStringUSFormat());
					stmt.setInt(4, invoice.getDaysToPay());
					stmt.setBoolean(5, invoice.getIsPaid());
					stmt.setString(6, invoice.getPaymentType().toString());
					//where clause
					stmt.setString(7, invoice.getInvoiceUniqueCode());
					stmt.setString(8, invoice.getIssuedBy().getUserName());
					stmt.setString(9, invoice.getIssuedTo().getVatCode());

					InvoiceSettingDB.update(cm, invoice);
					InvoiceItemDB.update(cm, invoice);
				}
				else
				{
					stmt.setString(1, newInvoice.getInvoiceUniqueCode());
					stmt.setInt(2, newInvoice.getInvoiceNumber());
					stmt.setString(3, newInvoice.getIssueDate().toStringUSFormat());
					stmt.setInt(4, newInvoice.getDaysToPay());
					stmt.setBoolean(5, newInvoice.getIsPaid());
					stmt.setString(6, newInvoice.getPaymentType().toString());
					//where clause
					stmt.setString(7, invoice.getInvoiceUniqueCode());
					stmt.setString(8, invoice.getIssuedBy().getUserName());
					stmt.setString(9, invoice.getIssuedTo().getVatCode());

					InvoiceSettingDB.update(cm, newInvoice);
					InvoiceItemDB.update(cm, newInvoice);
				}

				stmt.execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Performs {@link Invoice} deletion and all associated data with that user. Which
	 * means all invoice {@link uk.co.sling.containers.Setting}, {@link uk.co.sling.containers.Invoice}
	 * and {@link uk.co.sling.containers.InvoiceItem} entries
	 * will be removed from the database.<br>
	 * A backup of the data is not performed by this function. It is user decision if the backup
	 * should be created.<br>
	 * If the user or contractor value is null or the table is not yet created the function exits without raising
	 * any errors.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br></p>
	 *
	 * @param invoice to remove
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void remove(Invoice invoice) throws SQLException, ClassNotFoundException
	{
		if (invoice == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					return;

				PreparedStatement stmt = cm.getStatement(DELETE_FROM_TABLE);

				stmt.setString(1, invoice.getInvoiceUniqueCode());
				stmt.setString(2, invoice.getIssuedBy().getUserName());
				stmt.setString(3, invoice.getIssuedTo().getVatCode());

				stmt.execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Retrieves the greatest number of the invoice for the given year.<br>
	 * If the passed user is {@code null}, the year has no invoices issued or
	 * the table doesn't exist 1 is returned as the
	 * number cannot be determined.</p>
	 *
	 * @param user for which get the number
	 * @param year year to get the number for. Any number is valid.
	 * @return the greatest invoice number
	 * @throws SQLException on connection error
	 * @throws ClassNotFoundException on driver error
	 */
	public static int getLatestInvoiceNumberForYear(User user, int year) throws SQLException, ClassNotFoundException
	{
		if (user == null)
			return 1;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					return 1;

				PreparedStatement stmt = cm.getStatement(SELECT_INVOICE_NUMBER);

				stmt.setString(1, user.getUserName());
				stmt.setString(2, String.valueOf(year));

				ResultSet rs = stmt.executeQuery();

				if (rs.next())
				{
					if (rs.getInt(1) >= 1)
						return rs.getInt(1);

				}
				return 1;
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Given the {@link User} and {@link Contractor} it populates it with {@link Invoice}.<br>
	 * If the {@link Contractor} has no {@link Invoice} it won't be populated.<br>
	 * If the user value, contractor value is null or the table doesn't exist the function will immediately without
	 * raising any errors.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br>
	 * </p>
	 *
	 * @param user user to populate
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if the driver doesn't exist
	 */
	public static void getInvoices(User user, Contractor contractor) throws SQLException, SQLFetchException, ClassNotFoundException
	{
		if (user == null || contractor == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			if (!cm.isExistingTable(TABLE_NAME))
				return;

			PreparedStatement stmt = cm.getStatement(SELECT_ALL_INVOICES_OF_CONTRACTOR);

			stmt.setString(1, user.getUserName());
			stmt.setString(2, contractor.getVatCode());

			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				//sanity check if the contractor and username actually are matching
				if (!user.getUserName().equals(rs.getString("userName")) || !contractor.getVatCode().equals(rs.getString("vatCode")))
					continue;

				Setting set = InvoiceSettingDB.getSetting(cm, user, rs.getString("invoiceCode"));

				String date[] = rs.getString("issueDate").split("-");

				Invoice iv =
						new Invoice.InvoiceBuilder()
								.setInvoiceNumber(rs.getInt("invoiceNumber"))
								.setIssuedBy(user)
								.setIssuedTo(contractor)
								.setIssueDate(LocalDateTimeEUFormat.of(Integer.parseInt(date[2]), Integer.parseInt(date[1]),
										Integer.parseInt(date[0])))
								.setSetting(set)
								.setDaysToPay(rs.getInt("daysToPay"))
								.setIsPaid(rs.getBoolean("isPaid"))
								.setPaymentType(PaymentType.fromString(rs.getString("paymentType")))
								.buildInvoice();

				InvoiceItemDB.getItems(cm, iv);

				contractor.addItem(iv);
			}
		}
	}
}