package uk.co.sling.database;

import uk.co.sling.containers.Contractor;
import uk.co.sling.containers.User;
import uk.co.sling.exceptions.InvalidValueException;
import uk.co.sling.exceptions.SQLFetchException;
import uk.co.sling.exceptions.ValueNotSetException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Provides functionality to interact with the database.
 * Allows to add, modify and remove contractors.
 *
 * @author sebastian wojtkowiak
 */
public final class ContractorDB
{
	static final String TABLE_NAME = "Contractor";
	static final String CREATE_TABLE =
			"CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " "+
					"(" +
						"userName TEXT NOT NULL," +
						"companyName TEXT NOT NULL," +
						"vatCode TEXT NOT NULL," +
						"townName TEXT NOT NULL," +
						"postCode TEXT NOT NULL," +
						"streetName TEXT NOT NULL," +
						"buildingNumber TEXT NOT NULL," +
						"phoneNumber TEXT NOT NULL," +
						"emailAddress TEXT NOT NULL," +
						"PRIMARY KEY(userName, vatCode)," +
						"FOREIGN KEY(userName) REFERENCES User(userName) " +
						"ON UPDATE CASCADE ON DELETE CASCADE" +
					");";
	static final String INSERT_INTO_TABLE = "" +
			"INSERT INTO " + TABLE_NAME + " "+
				"(userName, companyName, vatCode, townName, postCode, streetName, buildingNumber, phoneNumber, emailAddress)" +
			"VALUES " +
				"(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	static final String UPDATE_TABLE = "" +
			"UPDATE " + TABLE_NAME + " "+
			"SET " +
				"companyName = ?," +
				"vatCode = ?," +
				"townName = ?," +
				"postCode = ?," +
				"streetName = ?," +
				"buildingNumber = ?," +
				"phoneNumber = ?," +
				"emailAddress = ? " +
			"WHERE " +
				"userName LIKE ? " +
			"AND " +
				"vatCode LIKE ?";
	static final String DELETE_FROM_TABLE = "" +
			"DELETE FROM " + TABLE_NAME + " "+
			"WHERE " +
				"userName LIKE ? " +
			"AND " +
				"vatCode LIKE ?";
	static final String SELECT_ALL_CONTRACTORS_OF_USER = "" +
			"SELECT * FROM " + TABLE_NAME + " " +
			"WHERE " +
				"userName LIKE ?";

	/**
	 * <p>Given the instance of a {@link Contractor} it saves its details into the
	 * database.<br>
	 * If the passed value is null the function will immediately terminate and no action
	 * of insert will be attempted.<br>
	 * The connection with the database is automatically managed by the function, and
	 * should never be closed/opened manually to ensure correct behaviour of the
	 * function.<br>
	 * If the database do not contain a table an attempt will be made to create such, on
	 * creation failure {@link SQLException} will be thrown to indicate that a failure
	 * has occurred.<br>
	 * Upon the successful creation of the table an attempt of saving the state of {@link User}
	 * will be made. On failure rollback will be performed to the last consistent state
	 * of the database and {@link SQLException} will be thrown<br>
	 * The {@link ValueNotSetException} will be always thrown when the user is a null value reference.<br></p>
	 *
	 * @param user user to which the contractor belongs
	 * @param contractor contractor to add
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void insert(User user, Contractor contractor) throws SQLException, ClassNotFoundException
	{
		if (user == null || contractor == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					cm.getStatement(CREATE_TABLE).execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}

			try
			{
				PreparedStatement stmt = cm.getStatement(INSERT_INTO_TABLE);

				stmt.setString(1, user.getUserName());
				stmt.setString(2, contractor.getCompanyName());
				stmt.setString(3, contractor.getVatCode());
				stmt.setString(4, contractor.getTown());
				stmt.setString(5, contractor.getPostCode());
				stmt.setString(6, contractor.getStreet());
				stmt.setString(7, contractor.getBuildingNumber());
				stmt.setString(8, contractor.getPhone());
				stmt.setString(9, contractor.getEmail());

				stmt.execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Given the instance of a {@link Contractor} it updates its details in the
	 * database.<br>
	 * If the value of user or contractor is {@code null} or table doesn't exist the function immediately exits and
	 * performs no operation. The value of newContractor is optional and should
	 * only be provided if the primaryKey {@link Contractor#vatCode} is to be updated<br>
	 * Updating an non-existing entry within the database will have no effect and will
	 * always succeed, and the database state won't be changed.<br>
	 * The connection with the database is automatically managed by the function, and
	 * should never be closed/opened manually to ensure correct behaviour of the
	 * function.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br>
	 * The {@link ValueNotSetException} will be always thrown when the user is a null value reference.<br>
	 * On unsuccessful update rollback will be performed and the entry won't be updated.</p>
	 *
	 *
	 * @param user user to which the contractor belongs
	 * @param contractor to update
	 * @param newContractor optional parameter to update primary key
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void update(User user, Contractor contractor, Contractor newContractor) throws SQLException, ClassNotFoundException
	{
		if (contractor == null || user == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					return;

				PreparedStatement stmt = cm.getStatement(UPDATE_TABLE);

				if (newContractor == null)
				{
					stmt.setString(1, contractor.getCompanyName());
					stmt.setString(2, contractor.getVatCode());
					stmt.setString(3, contractor.getTown());
					stmt.setString(4, contractor.getPostCode());
					stmt.setString(5, contractor.getStreet());
					stmt.setString(6, contractor.getBuildingNumber());
					stmt.setString(7, contractor.getPhone());
					stmt.setString(8, contractor.getEmail());
					//where clause
					stmt.setString(9, user.getUserName());
					stmt.setString(10, contractor.getVatCode());
				}
				else
				{
					stmt.setString(1, newContractor.getCompanyName());
					stmt.setString(2, newContractor.getVatCode());
					stmt.setString(3, newContractor.getTown());
					stmt.setString(4, newContractor.getPostCode());
					stmt.setString(5, newContractor.getStreet());
					stmt.setString(6, newContractor.getBuildingNumber());
					stmt.setString(7, newContractor.getPhone());
					stmt.setString(8, newContractor.getEmail());
					//where clause
					stmt.setString(9, user.getUserName());
					stmt.setString(10, contractor.getVatCode());
				}

				stmt.execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Performs {@link Contractor} deletion and all associated data with that contractor. Which
	 * means all {@link uk.co.sling.containers.Invoice} and {@link uk.co.sling.containers.InvoiceItem} entries
	 * will be removed from the database.<br>
	 * A backup of the data is not performed by this function. It is user decision if the backup
	 * should be created.<br>
	 * If the user or contractor value is null or the table is not yet created the function exits without raising
	 * any errors.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br>
	 * The {@link ValueNotSetException} will be always thrown when the user is a null value reference.<br>
	 * On unsuccessful deletion rollback will be performed and the entry won't be removed.
	 * </p>
	 *
	 * @param user user to which the contractor belongs
	 * @param contractor to remove
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if driver not found
	 */
	public static void remove(User user, Contractor contractor) throws SQLException, ClassNotFoundException
	{
		if (user == null || contractor == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			try
			{
				if (!cm.isExistingTable(TABLE_NAME))
					return;

				PreparedStatement stmt = cm.getStatement(DELETE_FROM_TABLE);

				stmt.setString(1, user.getUserName());
				stmt.setString(2, contractor.getVatCode());

				stmt.execute();
			}
			catch (SQLException sqle)
			{
				cm.rollback();
				throw sqle;
			}
		}
	}

	/**
	 * <p>Given the {@link User} it populates it with {@link Contractor}.<br>
	 * If the {@link User} has no {@link Contractor} it won't be populated.<br>
	 * If the user value is null or the table doesn't exist the function will immediately without
	 * raising any errors.<br>
	 * Encountering any connection issues the {@link SQLException} will always be thrown,
	 * upon encountering of a driver issue the {@link ClassNotFoundException} will be thrown instead.<br>
	 * </p>
	 *
	 * @param user user to populate
	 * @throws SQLException on connection issues
	 * @throws ClassNotFoundException if the driver doesn't exist
	 */
	public static void getContractors(User user) throws SQLException, SQLFetchException, ClassNotFoundException
	{
		if (user == null)
			return;

		try (ConnectionManager cm = ConnectionManager.getConnectionInstance())
		{
			if (!cm.isExistingTable(TABLE_NAME))
				return;

			PreparedStatement stmt = cm.getStatement(SELECT_ALL_CONTRACTORS_OF_USER);
			stmt.setString(1, user.getUserName());

			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				user.addItem(new Contractor.ContractorBuilder()
						.setCompanyName(rs.getString("companyName"))
						.setVatCode(rs.getString("vatCode"))
						.setTown(rs.getString("townName"))
						.setPostCode(rs.getString("postCode"))
						.setStreet(rs.getString("streetName"))
						.setBuildingNumber(rs.getString("buildingNumber"))
						.setPhoneNumber(rs.getString("phoneNumber"))
						.setEmail(rs.getString("emailAddress"))
						.buildContractor());
			}
		}
	}
}
